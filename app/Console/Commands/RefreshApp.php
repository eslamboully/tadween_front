<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Artisan;

class RefreshApp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ra';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh App';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      $this->info('config:cache');
      Artisan::call('config:cache');

      $this->info('cache:clear');
      Artisan::call('cache:clear');

      $this->info('route:clear');
      Artisan::call('route:clear');

    }
}
