<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Invoice;

class OverdueInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'overdue:invoices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Overdue Invoices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $invoices = Invoice::where('approval' , 'approved')->payment(['unpaid' , 'partially_paid'])->get();

      $invoices->filter(function($invoice) {

        if($invoice->order and  $invoice->order->method == 'Pay later'){

          $overdue_payamounts = $invoice->order->payments()->whereDate('payment_date' , '<' , now() )->get();

          $invoice->payment_status = 'overdue';
          $invoice->save();

        }

      });
    }
}
