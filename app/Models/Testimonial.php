<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    protected $fillable = ['image','name_ar','name_en','body_ar','body_en' , 'job_title_ar' , 'job_title_en'];
}
