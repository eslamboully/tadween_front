<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin;

class News extends Model
{
    protected $fillable = [
        'title_ar','title_en', 'description_ar','description_en','photo','admin_id'
    ];

    public function admin () {
        return $this->belongsTo(Admin::class);
    }

}
