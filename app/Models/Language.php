<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
  protected $fillable = ['name' , 'code' , 'icon' , 'file'];

  public $timestamps = false;

  public static function boot(){

    parent::boot();

    static::deleting(function($language){
      $language->translations()->delete();
    });

  }

  public function translations(){
    return $this->hasMany(Translation::class , 'locale' , 'code');
  }

}
