<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use File as LaravelFile;

class File extends Model
{
  protected $fillable = [
    'file','file_type','file_size','type','type_id'
  ];

  public static function boot(){

    parent::boot();

    static::deleting(function($file){

      $uploaded_file = url('assets/images/' . $file->type . '/' . $file->file);

      LaravelFile::delete( $uploaded_file );

    });

  }

}
