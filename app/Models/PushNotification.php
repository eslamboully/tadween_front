<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PushNotification extends Model
{
  protected $fillable = [
    'title','body' , 'type' , 'type_id' , 'token','user_id','seen'
  ];
}
