<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = [
        'title_ar','title_en','description_ar','description_en','salesman','number_of_admins','number_of_customers','number_of_product','price'
    ];

}
