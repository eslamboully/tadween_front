<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TestimonialResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name_ar' => $this->name_ar,
            'name_en' => $this->name_en,
            'body_ar' => $this->body_ar,
            'body_en' => $this->body_en,
            'job_title_ar' => $this->job_title_ar,
            'job_title_en' => $this->job_title_en,
            'image' => '/assets/images/testimonial/'.$this->image
        ];
    }
}
