<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AdminPermissionsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

      $modules = ['users' ,'admins' ,'categories' ,'sotres' ,'products' ,'settings'];

      $array = [];

      foreach($modules as $module){

        $operations = ['create','read','update','delete'];

        $value = [];

        foreach($operations as $operation){
          $value[$operation] = $this->can($operation . '-' .  $module);
        }
        $array[$module] = $value;
      }

      return $array;
    }
}
