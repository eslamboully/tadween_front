<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'id' => $this->id,
        'name'=> $this->name,
        'photo' => '/assets/images/users/' . $this->photo,
        'email' => $this->email,
        'phone'=> $this->phone,
        'joined_at' => $this->created_at->format('Y/m/d'),
      ];
    }
}
