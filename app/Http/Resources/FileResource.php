<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class FileResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'file' => '/assets/images/' . $this->type . '/' .  $this->file ,
            'file_type' => $this->file_type,
            'file_size' => $this->file_size,
            'type' => $this->type,
            'type_id' => $this->type_id
        ];
    }
}
