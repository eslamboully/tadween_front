<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GeneralSettingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'footer_logo' => '/assets/images/' . $this->footer_logo,
          'logo' => '/assets/images/' . $this->logo,
          'title' => $this->title,
          'header_email' => $this->header_email,
          'header_phone' => $this->header_phone,
          'footer' => $this->footer,
          'about_us' => $this->about_us,
        ];
    }
}
