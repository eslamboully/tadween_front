<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Language;

class Lang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->header('lang');

        $language = Language::where('code' , $locale)->first();

        if($language)
          app()->setLocale($locale);
        else
          app()->setLocale('en');


        return $next($request);
    }
}
