<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\Api\ApiResponse;

class ApiMidlleware
{

  use ApiResponse;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){

      if($this->api_user() || $this->api_admin() ){
        return $next($request);
      }
      return $this->unauthenticated();
    }
}
