<?php

namespace App\Http\Middleware;

use Closure;

class RemoveNullAndUndefinedFromRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

      $data = $request->all();

      foreach($data as $key => $value){
        if($value == null || $value == 'null' || $value == 'undefined' ){
          unset($data[$key]);
        }
      }

      $request->replace($data);

       return $next($request);

    }
}
