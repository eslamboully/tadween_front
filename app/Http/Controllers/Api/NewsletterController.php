<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Newsletter;
use App\Models\User;
use Validator;

class NewsletterController extends Controller {

  use ApiResponse;

	public function subscribe() {

    $validator = Validator::make(request()->all() , [
      'email' => 'required|email',
    ]);

    if($validator->fails()){
      return $this->apiResponse(null , $validator->errors() , 422);
    }

		Newsletter::subscribe(request('email'));

        return $this->successResponse();

	}

}
