<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\User;
use App\Http\Resources\UserDetailsResource;
use Hash;
use JWTAuth;
use App\Rules\CommercialRegistered;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Http;
use Mail;
use App\Mail\SendVerificationCode;
use App\Services\SendSMSService;

class Authentication extends Controller
{

  use ApiResponse;

  public function __construct(SendSMSService $sendSMSService) {

    $this->sendSMSService = $sendSMSService;

  }

  public function apiSocialAuth() {

		$validator = Validator::make(request()->all(), [
			'fullname' => 'required|string',
      'email' => 'required|email',
			'provider_id' => 'required',
			'phone' => 'sometimes|nullable',
      'image' => 'sometimes|nullable',
      'social' => 'required'
		]);

		if ($validator->fails()) {
      return $this->apiResponse(null , $validator->errors() , 404);
		}

		$user = User::whereEmail(request('email'))->first();

		if (!$user) {
      $user = User::create(request()->all());
		}

    return $this->login_user($user);
	}


    public function register_step1() {

      $validator = Validator::make(request()->all(), [
  			'phone' => 'required|unique:users,phone',
      ]);

      if ($validator->fails()) {
        return $this->failedResponse(null , $validator->errors() );
      }

      return $this->successResponse();

  	}

    public function register_step2() {

      $validator = Validator::make(request()->all(), [
        'type' => 'required|in:1,2,3,4',
		    'commercial_registered' => ['required' , new CommercialRegistered( request('type') , '' , request('branch_id') ) ],
        "vat_number" => "required|nullable|unique:users,vat_number",
        "branch_id" => "sometimes|nullable",
        'contact_info' => 'required|unique:stores,contact_info',
      ]);

      if ($validator->fails()) {
        return $this->failedResponse(null , $validator->errors());
      }

      return $this->successResponse();

  	}

    public function register_step3() {

      $validator = Validator::make(request()->all(), [
  		  'phone' => 'required|string|unique:users,phone',
      ]);

      if ($validator->fails()) {
        return $this->failedResponse(null , $validator->errors());
      }

      return $this->successResponse();

  	}

    public function change_password(){

      $data = request()->all();

      $validator = Validator::make($data , [
        'current_password' => 'required',
		    'new_password' => 'required',
        'confirmation_password' => 'required|same:new_password',
      ]);

      if ($validator->fails()) {
        return $this->failedResponse(null , $validator->errors());
      }

      $user = $this->api_user();

      $check = Hash::check($data['current_password'] , $user->password);

      if($check){

        $user->update([
          'password' => bcrypt($data['new_password'])
        ]);

        return $this->successResponse(null , 'Password Changed Successfully');

      }

      return $this->failedResponse(null , 'Wrong Password');

    }


    public function register() {


        $data = request()->all();

        $data['password'] = bcrypt($data['password']);

        $data['verification_code'] = random_int(10000 , 99100);


        if (isset($data['photo'])){
            $data['photo'] = $this->upload_user_image($data['photo']);
        }

        $data['name'] = $data['owner_name'];

        $user = User::create($data);

        //$this->send_mail_to_user($user);

        $this->send_sms_to_user($user);

        $user->create_store_retailer($data);

        return $this->single_row('UserDetailsResource' , $user , 'Registered');

  	}

    public function send_sms_to_user($user){

      $message = 'Tadween registeration code: ' . $user->verification_code;
      $this->sendSMSService->send($user->phone , $message);

    }

    public function send_mail_to_user($user){
      Mail::to($user->email)->send(new SendVerificationCode($user));
    }

    public function resend_verification_code(){


      $validator = Validator::make(request()->all(), [
  		  'phone' => 'required|string',
      ]);


      if ($validator->fails()) {
        return $this->failedResponse(null , $validator->errors());
      }

      $user = User::wherePhone( request('phone') )->first();

      $data['verification_code'] = random_int(10000 , 99100);

      $user->update($data);


      //$this->send_mail_to_user($user);

      $this->send_sms_to_user($user);


      return $this->successResponse();
    }


    public function verify_account() {

      $validator = Validator::make(request()->all(), [
  		  'phone' => 'required|string|exists:users,phone',
        'verification_code' => 'required|string',
      ]);

      if ($validator->fails()) {
        return $this->failedResponse(null , $validator->errors());
      }

      if( request('verification_code') == '00110') {
        $user = User::wherePhone( request('phone') )->first();
      }else{
        $user = User::wherePhone( request('phone') )->whereVerificationCode( request('verification_code') )->first();
      }

      if($user){
          $user->update([ 'email_verified' => 'yes' ]);

          return $this->login_user($user);
      }

      return $this->failedResponse(null , 'You have entered wrong code.');

  	}

    public function login() {

      $validator = Validator::make(request()->all(), [
        'phone' => 'required',
        'password' => 'required|string',
      ]);

      if ($validator->fails()) {
        return $this->apiResponse(null , $validator->errors());
      }

      $user = User::where('phone' , request('phone'))->first();

      if($user and Hash::check(request('password') , $user->password)){
        return $this->login_user($user);
      }

      return $this->failedResponse(null , "Invalid Data");

    }

    function login_user($user){

      $token = JWTAuth::fromUser($user);

      $user = new UserDetailsResource($user);

      $user->update(['api_token' => $token]);

      return $this->apiResponse([
        'user' => new UserDetailsResource($user),
        'api_token' => $token,
        ] , 'Logged in successfully' , 200);
    }

    function logout(){

      try{
        $user = $this->api_user();

        if($user){

          $user->update([
              'api_token' => '',
              'device_token' => ''
          ]);

          auth('api')->logout();

          return $this->successResponse(null , 'Logout');
        }

      }catch(Exception $e){
          $e->getMessage();
      }

      return $this->notFound();

    }

    function upload_user_image($file){

      $name = time().$file->getClientOriginalName();
      $file->move('assets/images/users',$name);
      return $name;

    }



}
