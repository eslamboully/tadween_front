<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Validator;
use App\Models\File;
use DB;
use File as LaravelFile;
use App\Services\SendPushNotificationService;
use App\Models\Contact;
use Mail;
use App\Models\Order;
use App\Models\Product;
use App\Models\Invoice;
use App\Models\Store;
use App\Models\Admin;

class HomeController extends Controller
{

    use ApiResponse;

    protected $sendPushNotificationService;

    function __construct(SendPushNotificationService $sendPushNotificationService)
    {
        $this->sendPushNotificationService = $sendPushNotificationService;
    }

    public function banks()
    {

        $data = [
            'Bank Alahly',
            'Bank Masr',
            'Bank Alexandria',
        ];

        return $this->successResponse($data);

    }

    public function upload_file()
    {

        $data = request()->all();

        $validator = Validator::make($data, [
            'type_id' => 'required_if:flag,==,create|numeric',
            'file' => 'required_if:flag,==,create|file',
            'type' => 'required_if:flag,==,create|nullable',
            'flag' => 'required|in:create,delete,update_main',
            'id' => 'required_if:flag,==,delete|exists:files,id'
        ]);


        if ($validator->fails()) {
            return $this->failedResponse(null, $validator->errors(), 422);
        }

        $data['type'] = str_plural($data['type']);


        if ($data['flag'] == 'create') {

            $data = $this->upload_new_file($data['file'], $data['type']);

            $file = File::create($data);

            return $this->single_row('FileResource', $file);

        } elseif ($data['flag'] == 'delete') {

            $file = File::find($data['id']);

            if ($file) {
                $file->delete();
                return $this->successResponse(null, "File Uploaded Successfully");
            }

        } elseif ($data['flag'] == 'update_main') {

            $type = DB::table($data['type'])->where('id', $data['type_id']);

            if ($type->exists()) {

                $data = $this->upload_new_file($data['file'], $data['type']);

                $type->update([
                    'photo' => $data['file']
                ]);

                $type = $type->first();

                if ($type->photo) {
                    $uploaded_file = url('assets/images/' . $data['type'] . '/' . $type->photo);
                    LaravelFile::delete($uploaded_file);
                }

                return $this->successResponse(null, "File Uploaded Successfully");
            }

        }

        return $this->failedResponse();


    }


    public function upload_new_file($file, $type)
    {

        $data['file_type'] = $file->getClientMimeType();
        $data['file_size'] = $file->getClientSize();
        $data['file'] = time() . $file->getClientOriginalName();
        $data['type'] = $type;

        $file->move('assets/images/' . $type, $data['file']);

        return $data;
    }


    public function statistics()
    {


        return $this->successResponse();
    }
}
