<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\PushNotification;

class NotificationController extends Controller {

  use ApiResponse;

	public function read($id) {

		$notification = PushNotification::find($id);

    if($notification){

      $notification->update([
        'seen' => 1
      ]);

      return $this->successResponse();
    }

    return $this->notFound();
	}

  public function notifications_badge_count()
  {

    $my_notifications = $this->api_user()->my_notifications();
    $my_notifications = $my_notifications->where('seen' , 0)->count();
    return $this->successResponse($my_notifications);

  }

  public function my_notifications()
  {

    $my_notifications = $this->api_user()->my_notifications()->orderBy('created_at' , 'desc')->get();
    return $this->collection('NotificationResource' , $my_notifications);

  }

}
