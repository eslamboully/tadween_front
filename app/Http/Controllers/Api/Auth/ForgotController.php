<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\ApiResponse;
use App\Models\Generalsetting;
use App\Models\User;
use Mail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Classes\GeniusMailer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Validator;
use Config;

class ForgotController extends Controller
{
    use ApiResponse;
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showForgotForm()
    {
      return view('user.forgot');
    }

    public function forgot(Request $request)
    {
        $user=User::where('email',request('email'))->first();
        if(!empty($user)){
            
            $code = random_int(100000 , 999999); 
            
            DB::table('password_resets')->insert(['email'=>$user->email,
                'token'=> $code ,
                'created_at'=>Carbon::now()]);
                        
            Mail::to($user->email)->send(new ResetPassword([ 'name' => $user->fullname , 'code'=> $code ]));

            return $this->successResponse(null,'Your Password Reseted Successfully. Please Check your email for new Password.');


        }
        return $this->failedResponse(null ,'User Email Not Found' );


    }


}
