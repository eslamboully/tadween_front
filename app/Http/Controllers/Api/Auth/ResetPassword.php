<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace  App\Http\Controllers\Api\Auth;


use App\Http\Controllers\Api\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Input;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Translation\Formatter\MessageFormatter;
use Validator;
use DB;
use Carbon\Carbon;
use App\Models\User;
class ResetPassword extends Mailable
{
    use ApiResponse;
    /**
     * @dataProvider getTransMessages
     */
    use Queueable ,SerializesModels;
    protected $data;
    public function __construct($data=[])
    {
        $this->data=$data;
    }
        public  function  build(){
            return $this->markdown('auth.emails.reset_code')
                ->subject('Reset Account')
                ->with($this->data);
        }

    public function verify_code(Request $request){

        $rules = [
            'email' => 'required|email|exists:users,email',
            'code'=>'required'
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return $this->apiResponse(null , $validator->errors() , 422);
        }


        $check_token =DB::table('password_resets')->where('email',$request->email)->where('token',$request->code)->where('created_at','>',Carbon::now()->subHours(2))->first();

        if($check_token)
            return $this->successResponse();

        return $this->failedResponse();

    }

    public function reset_password_post(Request $request){
        $rules = [
            'email' => 'required|email|exists:users,email',
            'password'=>'required|confirmed',
            'password_confirmation'=>'same:password',
            'code'=>'required'
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return $this->apiResponse(null , $validator->errors() , 422);
        }
        $check_token =DB::table('password_resets')->where('email',$request->email)->where('token',$request->code)->where('created_at','>',Carbon::now()->subHours(2))->first();
        if(!empty($check_token)){
            $user=User::where('email',$check_token->email)->update([
                'email'=>$check_token->email,
                'password'=>bcrypt(request('password')),
            ]);

            DB::table('password_resets')->where('email',request("email"))->delete();
            return $this->apiResponse($user ,'Your Password Updated Successfully.');

        }
        else{
            return $this->failedResponse();
        }
    }



}
