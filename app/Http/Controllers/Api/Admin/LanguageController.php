<?php

namespace App\Http\Controllers\Api\Admin;

use App\Models\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Http\Controllers\Api\ApiResponse;
use App\Helpers\UploadHelper;

class LanguageController extends Controller
{
  use ApiResponse , UploadHelper;

    public function index()
    {
      $languages = Language::all();

      return $this->collection('LanguageResource' , $languages);

    }

    public function show($id)
    {
      $language = Language::find($id);
      return $this->single_row('LanguageResource' , $language);

    }

    //*** POST Request
    public function store(Request $request)
    {
        //--- Validation Section
        $data = $request->all();

        $validator = Validator::make($data, [
          'name' => 'required|unique:languages,name|max:255',
          'code' => 'required|unique:languages,code|max:255',
          'icon' => 'required|image'
        ]);

        if ($validator->fails()) {
          return $this->failedResponse( null , $validator->errors());
        }

        $data['icon'] = $this->upload($data['icon'] , 'languages');

        Language::create($data);

        return $this->successResponse();

    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $language = Language::find($id);

        if(!$language){
          return $this->notFound();
        }

        $validator = Validator::make($data, [
          'name' => 'required|max:255|unique:languages,name,' . $id,
          'code' => 'required|max:255|unique:languages,code,' . $id,
          'icon' => 'sometimes|nullable|image'
        ]);

        if ($validator->fails()) {
          return $this->failedResponse( null , $validator->errors());
        }
        //--- Validation Section Ends

        if(request()->hasFile('icon')){
          $data['icon'] = $this->upload($data['icon'] , 'languages');
        }else{
          unset($data['icon']);
        }

        $language->update($data);

        return $this->successResponse();
    }

    //*** GET Request Delete
    public function destroy($id)
    {
        $language = Language::find($id);
        if($language){
          $language->delete();
          return $this->successResponse(null , 'Deleted Successfully');
        }

        return $this->notFound();
    }
}
