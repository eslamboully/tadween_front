<?php
namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Partner;
use App\Http\Controllers\Api\ApiResponse;
use Validator;
use Hash;

class PartnerController extends Controller
{
    use ApiResponse;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $partner = Partner::all();

        return response()->json(['data' => $partner,'message' => '', 'status' => 1]);
    }


    function upload_partners_image($file){

        $name = time().$file->getClientOriginalName();
        $file->move('assets/images/partners',$name);
        return $name;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = request()->all();

        $validator = Validator::make($data, [
            'photo' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->failedResponse( null , $validator->errors());
        }

        if(request()->hasFile('photo')){
            $data['photo'] = '/assets/images/partners/'.$this->upload_partners_image( request('photo') );
        }else{
            $data['photo'] = 'default.png';
        }
        $partner = Partner::create($data);

        return response()->json(['data' => $partner,'message' => '','status' => 1]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $partner = Partner::find($id);

        return response()->json(['data' => $partner,'message' => '','status' => 1]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $partner = Partner::find($id);

        if($partner){

            $data = request()->all();

            $validator = Validator::make($data, [
                'photo' => 'sometimes|nullable',
            ]);

            if ($validator->fails()) {
                return $this->failedResponse( null , $validator->errors());
            }

            if(request()->hasFile('photo')){
                $data['photo'] = $this->upload_partners_image( request('photo') );
            }else{
                unset($data['photo']);
            }

            $partner->update($data);

            return response()->json(['data' => $partner,'message' => '','status' => 1]);
        }

        return $this->notFound();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $partner =  Partner::find($id);

        if($partner){
            $partner->delete();
            return $this->successResponse(null , 'Deleted Successfully');

        }

        return $this->notFound();

    }
}
