<?php
namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\News;
use App\Http\Controllers\Api\ApiResponse;
use Validator;
use Hash;

class NewsController extends Controller
{
    use ApiResponse;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $news = News::all();

        return response()->json(['data' => $news,'message' => '', 'status' => 1]);
    }


    function upload_news_image($file){

        $name = time().$file->getClientOriginalName();
        $file->move('assets/images/news',$name);
        return $name;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = request()->all();

        $validator = Validator::make($data, [
            'title_ar' => 'required',
            'title_en' => 'required',
            'description_ar' => 'required',
            'description_en' => 'required',
            'photo' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->failedResponse( null , $validator->errors());
        }

        if(request()->hasFile('photo')){
            $data['photo'] = $this->upload_news_image( request('photo') );
        }else{
            $data['photo'] = 'default.png';
        }

        $data['admin_id'] = auth('admin')->user()->id;
        $news = News::create($data);

        return response()->json(['data' => $news,'message' => '','status' => 1]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = News::find($id);
        $news['photo'] = url('/assets/images/news/'.$news->photo);

        return response()->json(['data' => $news,'message' => '','status' => 1]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $news = News::find($id);

        if($news){

            $data = request()->all();

            $validator = Validator::make($data, [
                'title_ar' => 'required',
                'title_en' => 'required',
                'description_ar' => 'required|string',
                'description_en' => 'required|string',
                'photo' => 'sometimes|nullable'
            ]);

            if ($validator->fails()) {
                return $this->failedResponse( null , $validator->errors());
            }

            if(request()->hasFile('photo')){
                $data['photo'] = $this->upload_news_image( request('photo') );
            }else{
                unset($data['photo']);
            }

            $news->update($data);

            return response()->json(['data' => $news,'message' => '','status' => 1]);
        }

        return $this->notFound();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news =  News::find($id);

        if($news){
            $news->delete();
            return $this->successResponse(null , 'Deleted Successfully');

        }

        return $this->notFound();

    }


    public function newsSingle($id)
    {
        $news = News::find($id);

        return response()->json(['data' => $news,'message' => '','status' => 1]);
    }
}
