<?php
namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Testimonial;
use App\Http\Controllers\Api\ApiResponse;
use Validator;
use Hash;

class TestimonialController extends Controller
{
    use ApiResponse;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $testimonials = Testimonial::all();
        return $this->collection('TestimonialResource' , $testimonials);
    }


    function upload_testimonial_image($file){

        $name = time().$file->getClientOriginalName();
        $file->move('assets/images/testimonial',$name);
        return $name;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = request()->all();

        $validator = Validator::make($data, [
            'name_ar' => 'required',
            'name_en' => 'required',
            'body_ar' => 'required',
            'body_en' => 'required',
            'job_title_ar' => 'required',
            'job_title_en' => 'required',
            'image' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->failedResponse( null , $validator->errors());
        }

        $data['image'] = $this->upload_testimonial_image( request('image') );

        $testimonial = Testimonial::create($data);

        return $this->successResponse();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $testimonial = Testimonial::find($id);

        return $this->single_row('TestimonialResource' , $testimonial);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $testimonial = Testimonial::find($id);

        if($testimonial){

            $data = request()->all();

            $validator = Validator::make($data, [
                'name_ar' => 'required',
                'name_en' => 'required',
                'body_ar' => 'required',
                'body_en' => 'required',
                'job_title_ar' => 'required',
                'job_title_en' => 'required',
                'image' => 'sometimes|nullable'
            ]);

            if ($validator->fails()) {
                return $this->failedResponse( null , $validator->errors());
            }

            if(request()->hasFile('image')){
                $data['image'] = $this->upload_testimonial_image( request('image') );
            }else{
                unset($data['image']);
            }

            $testimonial->update($data);

            return response()->json(['data' => $testimonial,'message' => '','status' => 1]);
        }

        return $this->notFound();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testimonial =  Testimonial::find($id);

        if($testimonial){
            $testimonial->delete();
            return $this->successResponse(null , 'Deleted Successfully');

        }

        return $this->notFound();

    }
}
