<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Http\Controllers\Api\ApiResponse;
use Validator;
use Hash;
use App\Models\Permission;

class AdminController extends Controller
{
  use ApiResponse;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
      $admins = Admin::all();

      return $this->collection('AdminResource' , $admins);

    }

    public function update_password(Request $request){
      $data = $request->all();

      $validator = Validator::make($data, [
  			'current_password' => 'required|string|min:3|max:20',
  			'new_password' => 'required|string|min:3|max:20',
  			'confirm_new_password' => 'same:new_password',
		  ]);

      $admin = $this->api_admin();

  		if (Hash::check($data['current_password'], $admin->password)) {

  			$admin->update(['password' => bcrypt(request('new_password'))]);

        return $this->successResponse();

  		}

      return $this->failedResponse([
        'error' => 'Current Password Is Invalid'
      ]);

    }

    function upload_admin_image($file){

      $name = time().$file->getClientOriginalName();
      $file->move('assets/images/users',$name);
      return $name;

    }

    public function get_permissions($id){
      $admin = Admin::with('permissions')->find($id);

      return $this->single_row('AdminPermissionsResource' , $admin);
    }


    public function update_permissions($id){
      $admin = Admin::find($id);

      $modules = request()->all();


      foreach($modules as $module => $permissions){

        foreach($permissions as $key => $value){

          $permissionName = $key . '-' . $module;

          $permission = Permission::where('name' , $permissionName)->first();

          if($permission){

            if($value and ! $admin->hasPermission($permissionName) ){
              $admin->attachPermission($permission->id);
            }elseif(!$value  and $admin->hasPermission($permissionName) ){
              $admin->detachPermission($permission->id);
            }

          }

        }

      }

      return $this->successResponse();

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $data = request()->all();

      $validator = Validator::make($data, [
        'email' => 'required|unique:admins,email',
        'phone' => 'required|unique:admins,phone',
        'name' => 'required|string|max:20',
        'password' => 'required|string',
        'photo' => 'sometimes|nullable',
      ]);

      if ($validator->fails()) {
        return $this->failedResponse( null , $validator->errors());
      }

      if(request()->hasFile('photo')){
        $data['photo'] = $this->upload_admin_image( request('photo') );
      }else{
        $data['photo'] = 'default.png';
      }

      $data['password'] = bcrypt($data['password']);

      $admin = Admin::create($data);

      return $this->single_row('AdminResource' , $admin);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $admin = Admin::find($id);

      return $this->single_row('AdminResource' , $admin);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $admin = Admin::find($id);

      if($admin){

        $data = request()->all();

        $validator = Validator::make($data, [
          'email' => 'required|unique:admins,email,'.$id,
          'phone' => 'required|unique:admins,phone,'.$id,
          'name' => 'required|string|max:20',
          'password' => 'sometimes|nullable|string',
          'photo' => 'sometimes|nullable'
        ]);

        if ($validator->fails()) {
          return $this->failedResponse( null , $validator->errors());
        }

        if(request()->hasFile('photo')){
          $data['photo'] = $this->upload_admin_image( request('photo') );
        }else{
          unset($data['photo']);
        }

        if(isset($data['password'])){
          $data['password'] = bcrypt($data['password']);
        }else{
          unset($data['password']);
        }

        $admin->update($data);

        return $this->single_row('AdminResource' , $admin);
      }

      return $this->notFound();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $admin =  Admin::find($id);

      if($admin){
        $admin->delete();
        return $this->successResponse(null , 'Deleted Successfully');

      }

      return $this->notFound();

    }
}
