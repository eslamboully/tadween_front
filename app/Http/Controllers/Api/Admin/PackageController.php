<?php
namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Package;
use App\Http\Controllers\Api\ApiResponse;
use Validator;
use Hash;

class PackageController extends Controller
{
    use ApiResponse;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $packages = Package::all()->map(function ($package){
            return[
                'description_ar' => $package->description_ar,
                'description_en' => $package->description_en,
                'id' => $package->id,
                'title_ar' => $package->title_ar,
                'title_en' => $package->title_en,
                'number_of_admins' => $package->number_of_admins == -1 ? null : $package->number_of_admins,
                'number_of_customers' => $package->number_of_customers == -1 ? null : $package->number_of_customers,
                'number_of_product' => $package->number_of_product == -1 ? null : $package->number_of_product,
                'price' => $package->price,
                'salesman' => $package->salesman == -1 ? null : $package->salesman,
            ];
        });

        return response()->json(['data' => $packages ,'message' => '', 'status' => 1]);
    }


    function upload_news_image($file){

        $name = time().$file->getClientOriginalName();
        $file->move('assets/images/news',$name);
        return $name;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = request()->all();

        $validator = Validator::make($data, [
            'title_ar' => 'sometimes',
            'title_en' => 'sometimes',
            'description_ar' => 'sometimes',
            'description_en' => 'sometimes',
            'salesman' => 'sometimes|nullable',
            'number_of_admins' => 'sometimes|nullable',
            'number_of_product' => 'sometimes|nullable',
            'price' => 'sometimes|nullable',
        ]);

        if ($validator->fails()) {
            return $this->failedResponse( null , $validator->errors());
        }

        if (request('salesman') == null){
            $data['salesman'] = -1;
        }

        if (request('number_of_admins') == null){
            $data['number_of_admins'] = -1;
        }

        if (request('number_of_product') == null){
            $data['number_of_product'] = -1;
        }

        if (request('price') == null){
            $data['price'] = 0;
        }

        $package = Package::create($data);

        return response()->json(['data' => $package,'message' => '','status' => 1]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $package = Package::find($id);

        $package->number_of_admins = $package->number_of_admins == -1 ? null : $package->number_of_admins;
        $package->number_of_customers = $package->number_of_customers == -1 ? null : $package->number_of_customers;
        $package->number_of_product = $package->number_of_product == -1 ? null : $package->number_of_product;
        $package->salesman = $package->salesman == -1 ? null : $package->salesman;

        return response()->json(['data' => $package,'message' => '','status' => 1]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $package = Package::find($id);

        if($package){

            $data = request()->all();

            $validator = Validator::make($data, [
                'title_ar' => 'required',
                'title_en' => 'sometimes|nullable',
                'description_ar' => 'sometimes|nullable',
                'description_en' => 'sometimes|nullable',
                'salesman' => 'sometimes|nullable',
                'number_of_admins' => 'sometimes|nullable',
                'number_of_product' => 'sometimes|nullable',
                'price' => 'sometimes|nullable',
            ]);

            if ($validator->fails()) {
                return $this->failedResponse( null , $validator->errors());
            }

            if ($validator->fails()) {
                return $this->failedResponse( null , $validator->errors());
            }

            if (request('salesman') == null){
                $data['salesman'] = -1;
            }

            if (request('number_of_admins') == null){
                $data['number_of_admins'] = -1;
            }

            if (request('number_of_product') == null){
                $data['number_of_product'] = -1;
            }

            if (request('price') == null){
                $data['price'] = 0;
            }


            $package->update($data);

            return response()->json(['data' => $package,'message' => '','status' => 1]);
        }

        return $this->notFound();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package =  Package::find($id);

        if($package){
            $package->delete();
            return $this->successResponse(null , 'Deleted Successfully');

        }

        return $this->notFound();

    }
}
