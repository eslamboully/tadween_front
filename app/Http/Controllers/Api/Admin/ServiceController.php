<?php
namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Service;
use App\Http\Controllers\Api\ApiResponse;
use Validator;
use Hash;

class ServiceController extends Controller
{
    use ApiResponse;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $service = Service::all();

        return response()->json(['data' => $service,'message' => '', 'status' => 1]);
    }


    function upload_service_image($file){

        $name = time().$file->getClientOriginalName();
        $file->move('assets/images/service',$name);
        return $name;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = request()->all();

        $validator = Validator::make($data, [
            'title_ar' => 'required',
            'title_en' => 'required',
            'description_ar' => 'required',
            'description_en' => 'required',
            'icon' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->failedResponse( null , $validator->errors());
        }

        if(request()->hasFile('icon')){
            $data['icon'] = $this->upload_service_image( request('icon') );
        }else{
            $data['icon'] = 'default.png';
        }

        $service = Service::create($data);

        return response()->json(['data' => $service,'message' => '','status' => 1]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service = Service::find($id);
        $service['icon'] = url('/assets/images/service/'.$service->icon);

        return response()->json(['data' => $service,'message' => '','status' => 1]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $service = Service::find($id);

        if($service){

            $data = request()->all();

            $validator = Validator::make($data, [
                'title_ar' => 'required',
                'title_en' => 'required',
                'description_ar' => 'required',
                'description_en' => 'required',
                'icon' => 'sometimes|nullable'
            ]);

            if ($validator->fails()) {
                return $this->failedResponse( null , $validator->errors());
            }

            if(request()->hasFile('icon')){
                $data['icon'] = $this->upload_service_image( request('icon') );
            }else{
                unset($data['icon']);
            }

            $service->update($data);

            return response()->json(['data' => $service,'message' => '','status' => 1]);
        }

        return $this->notFound();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service =  Service::find($id);

        if($service){
            $service->delete();
            return $this->successResponse(null , 'Deleted Successfully');

        }

        return $this->notFound();

    }
}
