<?php
namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\Package;
use App\Models\Partner;
use App\Models\Service;
use Illuminate\Http\Request;
use App\Models\Config;
use App\Http\Controllers\Api\ApiResponse;
use Validator;
use Hash;
use App\Models\Testimonial;

class ConfigController extends Controller
{
    use ApiResponse;

    public function index($id)
    {

        $config = Config::where('category',$id)->where('type','!=',2)->get();

        return response()->json(['data' => $config,'message' => '', 'status' => 1]);
    }


    function upload_config_image($file){

        $name = time().$file->getClientOriginalName();
        $file->move('assets/images/config',$name);
        return $name;

    }


    public function update(Request $request)
    {
        $configs = $request->except(['_method']);
//        dd($request->all());
        foreach($configs as $index => $value) {
            if (substr($index,-2) == 'ar'){
                $setting = Config::where('var',substr($index, 0, -3))->first();
                $setting->value_ar = $value;
                $setting->save();

            }elseif (substr($index,-2) == 'en'){
                $setting = Config::where('var',substr($index, 0, -3))->first();
                $setting->value_en = $value;
                $setting->save();
            }else{
                $setting = Config::where('var',$index)->first();
                $setting->static_value = $value;
                $setting->save();
            }
        }


        return response()->json(['data' => 'successfull','message' => '','status' => 1]);
    }


    public function frontIndex()
    {
        $configs = Config::all();
        $settings = [];
        foreach ($configs as $config) {
            if ($config->static == 0) {
                $settings[$config->var.'-ar'] = $config->value_ar;
                $settings[$config->var.'-en'] = $config->value_en;
            }else{
                $settings[$config->var] = $config->static_value;
            }
        }

        $news = News::with(['admin'])->get();

        $testimonials =  $this->collection_resource('TestimonialResource' , Testimonial::all());


        $packages = Package::all()->map(function ($package){
            return [
                'description_ar' => $package->description_ar,
                'description_en' => $package->description_en,
                'id' => $package->id,
                'title_ar' => $package->title_ar,
                'title_en' => $package->title_en,
                'number_of_admins' => $package->number_of_admins == -1 ? null : $package->number_of_admins,
                'number_of_customers' => $package->number_of_customers == -1 ? null : $package->number_of_customers,
                'number_of_product' => $package->number_of_product == -1 ? null : $package->number_of_product,
                'price' => $package->price,
                'salesman' => $package->salesman == -1 ? null : $package->salesman,
            ];
        });

        $services = Service::all();
        $partners = Partner::all();

        return response()->json(['data' => ['configs' => $settings,'news' => $news,'testimonials' => $testimonials,'packages' => $packages, 'services' => $services,'partners' => $partners],'message' => '','status' => 1]);
    }
}
