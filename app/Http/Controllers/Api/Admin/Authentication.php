<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\ApiResponse;
use App\Http\Controllers\Controller;
use App\Http\Resources\AdminResource;
use App\Models\Admin;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Validator;

class Authentication extends Controller {
	use ApiResponse;

	public function login() {
        $validator = Validator::make(request()->all(), [
			'email' => 'required',
			'password' => 'required|string',
		]);

		if ($validator->fails()) {
			return $this->apiResponse(null, $validator->errors());
		}

		$admin = Admin::where('email', request('email'))->first();
		if ($admin and Hash::check(request('password'), $admin->password)) {
			return $this->login_admin($admin, str_random(32));
		}
		return $this->failedResponse(null, 'Invalid Data');
	}

	public function login_admin($admin, $token = null) {

		$token = JWTAuth::fromUser($admin);

		$admin = new AdminResource($admin);

		$admin->update(['api_token' => $token]);

		return $this->apiResponse([
			'admin' => $admin,
			'api_token' => $token,

		], 'Logged in successfully', 200);
	}

	public function logout() {

		try {
			$admin = $this->api_admin();

			if ($admin) {

				$admin->update(['api_token' => '']);

				auth('admin')->logout();

				return $this->successResponse(null, 'Logout');
			}

		} catch (Exception $e) {
		}

		return $this->failedResponse();

	}

}
