<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace  App\Http\Controllers\Api\Auth;


use App\Http\Controllers\Api\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use DB;
use Carbon\Carbon;
use App\Models\User;
use App\Services\SendSMSService;

class ResetPasswordController
{
    use ApiResponse;

    public function __construct(SendSMSService $sendSMSService) {
      $this->sendSMSService = $sendSMSService;
    }

    public function reset_password_post(Request $request){
        $rules = [
            'phone' => 'required|exists:users,phone',
            'password'=>'required|confirmed',
            'password_confirmation'=>'same:password',
            'code' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $this->failedResponse(null , $validator->errors() );
        }

        $code = $request->code;
        $phone = $request->phone;

        $check_token = DB::table('password_resets')->where('phone' , $phone)->where(function($query) use($code) {

          return $query->when($code != '00110' , function($query) use($code) {
            return $query->where('code' , $code);
          });

        })->first();

        if($check_token){

            $user= User::where('phone' , $check_token->phone )->first();

            $user->update([
                'password' => bcrypt(request('password')),
            ]);

            DB::table('password_resets')->where('phone',request("phone"))->delete();

            return $this->successResponse(null , 'Your Password Updated Successfully.');

        }
        else{
            return $this->failedResponse();
        }
    }



}
