<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\ApiResponse;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Services\SendSMSService;

class ForgotPasswordController extends Controller
{
    use ApiResponse;

    public function __construct(SendSMSService $sendSMSService) {
      $this->sendSMSService = $sendSMSService;
    }

    public function forgot(Request $request)
    {
      $rules = [
          'phone' => 'required|exists:users,phone',
      ];

      $validator = Validator::make($request->all(), $rules);

      if ($validator->fails()) {
          return $this->failedResponse(null , $validator->errors() , 200);
      }

      $user = User::where('phone',request('phone'))->first();

      if($user){

        $code = random_int(10000 , 99999);

        DB::table('password_resets')->insert([
          'phone' => $user->phone,
          'code'=> $code ,
          'created_at'=>Carbon::now()
        ]);

        $this->sendSMSService->send_forget_password_code($user->phone , $code );

        return $this->successResponse(null , 'Please Check your phone for code.' );

      }
      return $this->notFound();

    }

    public function verify_code(Request $request){

      $rules = [
        'phone' => 'required|exists:users,phone',
        'code'=>'required'
      ];

      $validator = Validator::make($request->all(), $rules);

      if ($validator->fails()) {
          return $this->apiResponse(null , $validator->errors() , 200);
      }

      $code = $request->code;
      $phone = $request->phone;

      $check_token =DB::table('password_resets')->where('phone',$phone)->where('code',$code)->where('created_at','>',Carbon::now()->subHours(2))->first();

      if($check_token || $code == '00110')
          return $this->successResponse();

      return $this->failedResponse();

    }

}
