<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;

class InvoicesExport implements FromCollection
{
  use Exportable;

    public function __construct($invoice)
    {
        $this->invoice = $invoice;
    }

    public function collection()
    {
       $invoice = $this->invoice;

       $product_data = $invoice->products;

       $data[] = ['Id' , 'Invoice ID' , 'Name' , 'Price' , 'Qty' , 'Total' , 'Sub Total'] ;

       foreach($product_data as $product){

         $data[] = [$product->id , $invoice->id , $product->product->name , $product->price , $product->qty , $product->total , $product->subtotal] ;

       }

       return collect($data);


    }
}
