<?php

namespace App\Operations;

use App\Models\Store;
use App\Http\Controllers\Api\ApiResponse;

trait StoreOperation {

	use ApiResponse;

  public function create_store($user){


   if($user->stores()->count() == 1){
     return $this->failedResponse(null , 'You Have Reached The Limit Number Of Stores');
   }

	 if(in_array($user->type , [1,2]) ){

      $data = request()->all();

      if (isset($data['photo'])){
          $data['photo'] = $this->upload_store_image($data['photo']);
      }else{
				$data['photo'] = 'default.png';
			}


    $store = $user->stores()->create($data);

    return $this->single_row('StoreDetailsResource' , $store , 'Created Successfully' , 201);
  }

  return $this->failedResponse(null , 'Not Autherized To Create Stores');

  }

	public function update_store($id , $user){

		$store = Store::find($id);

		if(!$store){
			return $this->notFound();
		}


		$data = request()->all();

			if ( request()->hasFile('photo') ){
					$data['photo'] = $this->upload_store_image($data['photo']);
			}else{
				unset($data['photo']);
			}

			$store->update($data);

		return $this->single_row('StoreDetailsResource' , $store , 'Updated Successfully' , 200);



	}


 function upload_store_image($file){

   $name = time().$file->getClientOriginalName();
   $file->move('assets/images/stores',$name);
   return $name;

 }


}
