<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Schema;
use URL;
use App;
use Config;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

      Config::set('filesystems.disks.tadween_public_export.url' , url('assets/exports'));

      Schema::defaultStringLength(191);
      Schema::enableForeignKeyConstraints();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        if (! request()->secure() && App::environment() === 'production') {
          URL::forceScheme('https');
        }

    }
}
