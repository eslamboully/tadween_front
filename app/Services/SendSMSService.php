<?php

namespace App\Services;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use App\Http\Controllers\Api\ApiResponse;
use Illuminate\Database\Eloquent\Model;
use App\Models\PushNotification;
use App\Models\User;
use App\Jobs\NewOrderCreated;

class SendSMSService {

	use ApiResponse;

	private $client , $data;

	public function __construct(){

		$this->client = new \GuzzleHttp\Client();

    $this->data = [
        'AppSid' => 'OBFKsburOj7C2Mb5D_9xm7zQACO55',
        'Priority' => 'High',
        'SenderID' => 'HIBAT',
    ];

	}

	public function send_sms(){

		// $this->client->post('https://api.unifonic.com/rest/Messages/Send', [
    //   'form_params' => $this->data
    // ]);

	}

  public function send($phone , $message){

		$this->data['Recipient'] = $phone;
		$this->data['Body'] = $message;

		$this->send_sms();

  }

	public function send_complete_account_sms($user){

		$code = random_int(10000 , 999999);

		$this->data['Recipient'] = $user->phone;
		$this->data['Body'] = 'Your tadween.app Code Is : ' . $code;

		$user->update([
			'profile_code' => $code
		]);

		$this->send_sms();

	}

	public function send_invoice_status_changed($user , $body){

		$this->data['Recipient'] = $user->phone;
		$this->data['Body'] = $body;

		$this->send_sms();

	}

	public function send_forget_password_code($phone , $code){

		$this->data['Recipient'] = $phone;
		$this->data['Body'] = 'Your tadween.app Forget Password Code Is : ' . $code;

		$this->send_sms();

	}

}
