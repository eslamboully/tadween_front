<?php

namespace App\Services;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use App\Http\Controllers\Api\ApiResponse;
use Illuminate\Database\Eloquent\Model;
use App\Models\PushNotification;
use App\Models\User;
use App\Jobs\NewOrderCreated;

class SendEmailService {

	use ApiResponse;


  public static function SendNewOrderCreated($sender , $users, $title, $body, $click_action){

    $data = [
      'title' => $title ,
      'body' => $body,
      'click_action' => $click_action,
      'sender' => $sender
    ];

    NewOrderCreated::dispatch($users , $data)->delay( now()->addMinutes(5) );
  }

}
