<?php

namespace App\Services;

use App\Models\Cart;
use App\Models\CartProduct;
use App\Models\Product;
use App\Models\Cart as CartDB;
use App\Http\Controllers\Api\ApiResponse;

class CartService {

	use ApiResponse;

	public $cart;

	public function __construct() {
		$this->cart = $this->api_user() ? $this->api_user()->carts()->whereDoesntHave('order')->latest()->first() : collect([]);
	}

	public function find($id) {
		return CartProduct::find($id);
	}

	public function findByProductId($id) {

		return CartProduct::where('product_id', $id)->where('cart_id' , $this->cart->id)->first();

	}

	public function add($id, $price, $qty) {

		$user = $this->api_user();


		$cart = $this->cart;

		if (!$cart) {

			$cart = $this->cart = CartDB::create([
				'total' => 0,
				'subtotal' => 0,
				'user_id' => $user->id,
			]);

		}

		$product = $this->findByProductId($id);

		if (!$product) {

			$subtotal = $qty * $price;
			$total = $qty * $price;

			$product = CartProduct::create([
				'product_id' => $id,
				'qty' => $qty,
				'subtotal' => $subtotal,
				'total' => $total,
				'price' => $price,
				'cart_id' => $cart->id,
			]);

			$cart->subtotal += $subtotal;
			$cart->total += $total;

			$cart->save();


		}

		return $product;

	}

	public function empty() {

		$user = $this->api_user();
		$cart = $this->cart;

		if ($cart) {

			foreach ($cart->products as $product) {
				$product->delete();
			}

			$cart->total = 0;
			$cart->subtotal = 0;
			$cart->save();
		}

		return $this->successResponse(null , 'Cart Is Empty' );

	}

	public function remove($id) {

		$product = $this->findByProductId($id);

		$user = $this->api_user();

		$cart = $this->cart;

		if ( $cart and ($user->id and $cart->user_id) and $product) {

			$cart->subtotal = $cart->subtotal - $product->total;
			$cart->total = $cart->total - $product->total;

			$cart->save();

			$product->delete();

			return $this->successResponse(null , 'Removed Successfully' );

		}

		return $this->failedResponse();

	}

	# Edit
	public function changeQty($id , $qty) {

		$product = $this->findByProductId($id);

		$user = $this->api_user();

		$cart = $this->cart;

		if ( $cart and ($user->id and $cart->user_id) and $product) {

            $this->update_cart($product , $qty , null);


//			$cart = $product->cart;
//
//			$cart->subtotal -= $product->qty * $product->product->price;
//			$cart->total -= $product->qty * $product->product->price;
//
//			$cart->save();
//
//			$product->qty = $qty;
//
//			$product->subtotal = $qty * $product->product->price;
//			$product->total = $qty * $product->product->price;
//
//			$product->save();
//
//			$cart->subtotal += $product->qty * $product->product->price;
//			$cart->total += $product->qty * $product->product->price;
//
//			$cart->save();

			return $this->single_row('ProductCartResource' , $product , 'Updated Successfully' );

		}

		return $this->failedResponse();

	}

	public function UpdatePrice(CartProduct $product , $price){

        $this->update_cart($product , $product->qty , $price);

    }

    function update_cart($product , $qty , $price = ''){

        $cart = $product->cart;

				//dd($product , $cart);

        $cart->subtotal -= $product->subtotal;
        $cart->total -= $product->total;
        $cart->save();

        if (!$price) {
            $price = $product->product->price;
        }

        $product->qty = $qty;
        $product->subtotal = $qty * $price;
        $product->total = $qty * $price;
        $product->price = $price;
        $product->save();

        $cart->subtotal += $product->subtotal;
        $cart->total += $product->total;
        $cart->save();

    }

}
