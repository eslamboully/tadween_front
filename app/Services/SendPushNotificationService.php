<?php

namespace App\Services;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use App\Http\Controllers\Api\ApiResponse;
use Illuminate\Database\Eloquent\Model;
use App\Models\PushNotification;
use App\Models\User;
use Carbon\Carbon;

class SendPushNotificationService {

	use ApiResponse;


  public static function Send($users, $title, $body, $type , $type_id){

    $optionBuilder = new OptionsBuilder();

    $optionBuilder->setTimeToLive(60*20);

    $notificationBuilder = new PayloadNotificationBuilder($title);
    $notificationBuilder->setBody($body)
        ->setSound('default')
        ->setBadge(1)
        ->setIcon(null)
        ->setClickAction(null);

    $dataBuilder = new PayloadDataBuilder();

    $option = $optionBuilder->build();
    $notification = $notificationBuilder->build();
    $data = $dataBuilder->build();

    // You must change it to get your tokens

    $users = collect($users);
    $tokens = $users->pluck('device_token')->toArray();
    foreach ($users as $user) {

      static::save(new PushNotification , [
        'title' => $title,
        'body' => $body,
        'type' => $type,
        'type_id' => $type_id,
        'token' => $user->device_token,
        'user_id' => $user->id
      ]);

    }

		$start = Carbon::createFromTimeString('8:00');

		$end = Carbon::createFromTimeString('18:00');

		$is_allowed_time = now()->between($start , $end , true);

		if (count($tokens) and $is_allowed_time) {
      $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

	    $downstreamResponse->numberSuccess();
	    $downstreamResponse->numberFailure();
	    $downstreamResponse->numberModification();

	    // return Array - you must remove all this tokens in your database
	    $downstreamResponse->tokensToDelete();

	    // return Array (key : oldToken, value : new token - you must change the token in your database)
	    $downstreamResponse->tokensToModify();

	    // return Array - you should try to resend the message to the tokens in the array
	    $downstreamResponse->tokensToRetry();

	    // return Array (key:token, value:error) - in production you should remove from your database the tokens present in this array
	    $downstreamResponse->tokensWithError();
		}

  }


  public function index(Model $pushNotification){

    return $pushNotification->all();

  }


  public static function save(Model $pushNotification , $data){

    return $pushNotification->create($data);

  }


}
