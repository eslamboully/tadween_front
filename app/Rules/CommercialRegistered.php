<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\User;

class CommercialRegistered implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($type , $id = '' , $branch_id = '')
    {
      $this->type = $type;
      $this->id = $id;
      $this->branch_id = $branch_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        $user = User::where($attribute , $value);
        $type = $this->type;
        $branch_id = $this->branch_id;


        if( $type == 3 || $type ==  4 || $type ==  5 ){
            return $user->count() > 0;
        }elseif(($type ==  1 || $type ==  2) and !$this->id ){

          $branch_exists = User::where('branch_id' , $branch_id)->count() == 0;

          if($branch_exists){
            return $user->count() >= 0;
          }else{
            return $user->count() == 0;
          }

        }elseif(($type ==  1 || $type ==  2) and $this->id ){

          $branch_exists = User::where('branch_id' , $branch_id)->where('id' , '!=' ,  $this->id)->count() == 0;

          if($branch_exists){
            return $user->where('id' , '!=' ,  $this->id)->count() >= 0;
          }else{
            return $user->where('id' , '!=' ,  $this->id)->count() == 0;
          }

        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
      $type = $this->type;

        if( $type ==  3 || $type ==  4 || $type ==  5 ){
            return 'The Commercial Registered Must Be Exist';
        }elseif($type ==  1 || $type ==  2){
            return 'The Commercial Registered Already Taken';
        }
    }
}
