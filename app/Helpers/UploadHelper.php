<?php

namespace App\Helpers;

trait UploadHelper{

  public function upload($file , $folder){

    $type = $file->getClientMimeType();
    $file_size  = $file->getClientSize();
    $name = time().$file->getClientOriginalName();

    $file->move('assets/images/' . $folder, $name );

    return $name;
  }

}
