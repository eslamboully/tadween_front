<?php

use App\Models\Language;

if(! function_exists('get_msg')){

  function get_msg($key){

    $locale = app()->getLocale();

    $language = Language::where('code' , $locale)->first();

    if($language and $language->file){

      $file_path = public_path() . '/assets/languages/' . $language->file;

      $file_data = file_get_contents($file_path);

      $file_data = json_decode($file_data , true);

      foreach($file_data as $main_keys_array){

        $exists = false;


        foreach($main_keys_array as $main_keys_key => $main_keys_value){

          if( $main_keys_key == $key ){

            $key = $main_keys_value;

            $exists = true;

          }

        }

        if($exists){
          break;
        }

      }

    }

    return $key;

  }

}

if(! function_exists('languages')){

    function languages(){
        return Language::all();
    }

}
