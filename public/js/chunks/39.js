(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[39],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/FrontView.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/front/FrontView.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../axios */ "./resources/js/src/axios.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      configs: [],
      news: [],
      packages: [],
      services: [],
      partners: [],
      lang: this.$i18n.locale
    };
  },
  mounted: function mounted() {
    this.fetch_configs_data();
  },
  created: function created() {
    var _this = this;

    window.VueInstant.$on('change_locale', function (lang) {
      _this.lang = lang;
    });
  },
  methods: {
    fetch_configs_data: function fetch_configs_data() {
      var _this2 = this;

      _axios__WEBPACK_IMPORTED_MODULE_0__["default"].get("/front/configs").then(function (response) {
        _this2.configs = response.data.configs;
        _this2.news = response.data.news;
        _this2.packages = response.data.packages;
        _this2.services = response.data.services;
        _this2.partners = response.data.partners;
        var vm = _this2;
        window.Fire.nextTick(function () {
          $('.packages .owl-carousel').owlCarousel({
            loop: true,
            margin: 30,
            nav: true,
            navText: ["<img src='/front/img/previos.png'>", "<img src='/front/img/next.png'>"],
            responsive: {
              0: {
                items: 1,
                stagePadding: 100,
                margin: 10
              },
              580: {
                items: 1,
                stagePadding: 200
              },
              1000: {
                items: 3
              }
            }
          });
          $('.partner .owl-carousel').owlCarousel({
            loop: true,
            margin: 30,
            responsive: {
              0: {
                items: 3
              },
              600: {
                items: 3
              },
              1000: {
                items: 5
              }
            }
          });
        }).catch(function (error) {
          reject(error);
        });
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/FrontView.vue?vue&type=template&id=91995c64&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/front/FrontView.vue?vue&type=template&id=91995c64& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("header", [
      _c("div", { staticClass: "container" }, [
        _c(
          "div",
          { class: _vm.lang == "ar" ? "row flex-row-reverse" : "row" },
          [
            _c(
              "div",
              { staticClass: "col-lg-6 col-12 text-lg-left text-center" },
              [
                _c(
                  "div",
                  {
                    class:
                      _vm.lang == "ar"
                        ? "main-text float-right"
                        : "main-text float-left",
                    style:
                      _vm.lang == "ar"
                        ? "text-align :right"
                        : "text-align :left"
                  },
                  [
                    _c(
                      "h1",
                      {
                        staticStyle: {
                          "font-family": "AlmaraiRegular",
                          "font-style": "normal",
                          "font-weight": "800",
                          "font-size": "42px",
                          "line-height": "118.1%"
                        }
                      },
                      [_vm._v(_vm._s(_vm.configs["slider-title-" + _vm.lang]))]
                    ),
                    _vm._v(" "),
                    _c(
                      "p",
                      {
                        staticStyle: {
                          "font-family": "AlmaraiRegular",
                          "font-style": "normal",
                          "font-weight": "normal",
                          "font-size": "16px",
                          "line-height": "24px",
                          "letter-spacing": "-0.41px",
                          "text-indent": "1px",
                          "text-transform": "capitalize"
                        }
                      },
                      [
                        _c("span", { staticClass: "bold" }, [
                          _vm._v(_vm._s(_vm.$t("tadween")))
                        ]),
                        _vm._v(
                          "\n                                " +
                            _vm._s(
                              _vm.configs["slider-description-" + _vm.lang]
                            ) +
                            "\n                            "
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticStyle: { "text-align": "center" } }, [
                      _c(
                        "a",
                        {
                          staticClass: "trial mt-5 mt-lg-0",
                          staticStyle: {
                            "font-family": "AlmaraiRegular",
                            "font-style": "normal",
                            "font-weight": "800",
                            "font-size": "16px",
                            "line-height": "22px"
                          },
                          attrs: { href: "#" }
                        },
                        [
                          _vm._v(
                            _vm._s(
                              _vm.configs["slider-first-button-" + _vm.lang]
                            )
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "a",
                        {
                          staticClass: "price mt-5 mt-lg-0",
                          staticStyle: {
                            padding: "17px",
                            display: "inline",
                            "font-family": "AlmaraiRegular",
                            "font-style": "normal",
                            "font-weight": "800",
                            "font-size": "16px",
                            "line-height": "22px"
                          },
                          attrs: { href: "#" }
                        },
                        [
                          _vm._v(
                            _vm._s(
                              _vm.configs["slider-second-button-" + _vm.lang]
                            )
                          )
                        ]
                      )
                    ])
                  ]
                )
              ]
            ),
            _vm._v(" "),
            _vm._m(0)
          ]
        )
      ]),
      _vm._v(" "),
      _c("img", {
        staticClass: "shape1",
        attrs: { src: "front/img/shapes/shape3-b.png" }
      }),
      _vm._v(" "),
      _c("img", {
        staticClass: "shape2",
        attrs: { src: "front/img/shapes/shape2-b.png" }
      })
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "work" }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12 text-center" }, [
            _c("div", { staticClass: "head" }, [
              _c(
                "h2",
                {
                  staticStyle: {
                    "font-family": "AlmaraiBold",
                    "font-style": "normal",
                    "font-weight": "800",
                    "font-size": "32px",
                    "line-height": "118.1%"
                  }
                },
                [_vm._v(_vm._s(_vm.$t("howitwork")))]
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-12" }, [
            _c("div", { staticClass: "body" }, [
              _vm._m(1),
              _vm._v(" "),
              _c("div", { staticClass: "download-app text-center" }, [
                _c(
                  "h2",
                  {
                    staticStyle: {
                      "font-family": "AlmaraiRegular",
                      "font-style": "normal",
                      "font-weight": "900",
                      "font-size": "26px",
                      "line-height": "118.1%"
                    }
                  },
                  [_vm._v(_vm._s(_vm.$t("download_app")))]
                ),
                _vm._v(" "),
                _c("div", { staticClass: "stores" }, [
                  _c("a", { attrs: { href: _vm.configs["app-store-link"] } }, [
                    _c("img", {
                      attrs: {
                        src: "front/img/app-store.png",
                        alt: "app-store"
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c(
                    "a",
                    {
                      attrs: {
                        target: "_blank",
                        href: _vm.configs["google-play-link"]
                      }
                    },
                    [
                      _c("img", {
                        attrs: {
                          src: "front/img/google-play.png",
                          alt: "google-play"
                        }
                      })
                    ]
                  )
                ])
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "colord" })
    ]),
    _vm._v(" "),
    _c(
      "section",
      { staticClass: "packages", attrs: { id: "packages-section" } },
      [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12 text-center" }, [
              _c("div", { staticClass: "head" }, [
                _c("h1", [
                  _c(
                    "span",
                    {
                      staticStyle: {
                        "font-family": "AlmaraiBold",
                        "font-style": "normal",
                        "font-weight": "800",
                        "font-size": "42px",
                        "line-height": "118.1%"
                      }
                    },
                    [_vm._v(" " + _vm._s(_vm.$t("packages")))]
                  ),
                  _vm._v(" "),
                  _c(
                    "span",
                    {
                      staticStyle: {
                        "font-family": "AlmaraiBold",
                        "font-style": "normal",
                        "font-weight": "800",
                        "font-size": "42px",
                        "line-height": "118.1%",
                        color: "#00928A"
                      }
                    },
                    [_vm._v(" " + _vm._s(_vm.$t("tadween")))]
                  )
                ]),
                _vm._v(" "),
                _c(
                  "p",
                  {
                    staticStyle: {
                      "font-family": "AlmaraiBold",
                      "font-style": "normal",
                      "font-weight": "normal",
                      "font-size": "16px",
                      "line-height": "22px",
                      "text-align": "center"
                    }
                  },
                  [
                    _vm._v(
                      _vm._s(_vm.configs["packages-description-" + _vm.lang])
                    )
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "owl-carousel owl-theme" },
              _vm._l(_vm.packages, function(packago) {
                return _c("div", { class: "package " + packago }, [
                  _c("div", { staticClass: "head" }, [
                    _c(
                      "h2",
                      {
                        staticStyle: {
                          "font-family": "AlmaraiBold",
                          "font-style": "normal",
                          "font-weight": "900",
                          "font-size": "35px",
                          "line-height": "132%",
                          "align-items": "center",
                          "text-align": "center",
                          "letter-spacing": "0.25px"
                        }
                      },
                      [_vm._v(_vm._s(packago["title_" + _vm.lang]))]
                    ),
                    _vm._v(" "),
                    _c(
                      "p",
                      {
                        staticStyle: {
                          "font-family": "AlmaraiRegular",
                          "font-style": "normal",
                          "font-weight": "normal",
                          "font-size": "12px",
                          "line-height": "15px",
                          "align-items": "center",
                          "text-align": "center",
                          "letter-spacing": "0.25px"
                        }
                      },
                      [_vm._v(_vm._s(packago["description_" + _vm.lang]))]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "details" }, [
                    _c("ul", [
                      packago.salesman
                        ? _c(
                            "li",
                            {
                              staticStyle: { "font-family": "AlmaraiRegular" }
                            },
                            [
                              _vm._v(_vm._s(_vm.$t("Salesmans"))),
                              _c("span", [_vm._v(_vm._s(packago.salesman))])
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      packago.number_of_admins
                        ? _c(
                            "li",
                            {
                              staticStyle: { "font-family": "AlmaraiRegular" }
                            },
                            [
                              _vm._v(_vm._s(_vm.$t("num_of_admins"))),
                              _c("span", [
                                _vm._v(_vm._s(packago.number_of_admins))
                              ])
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      packago.number_of_customers
                        ? _c(
                            "li",
                            {
                              staticStyle: { "font-family": "AlmaraiRegular" }
                            },
                            [
                              _vm._v(_vm._s(_vm.$t("num_customers"))),
                              _c("span", [
                                _vm._v(_vm._s(packago.number_of_customers))
                              ])
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      packago.number_of_product
                        ? _c(
                            "li",
                            {
                              staticStyle: { "font-family": "AlmaraiRegular" }
                            },
                            [
                              _vm._v(_vm._s(_vm.$t("num_products"))),
                              _c("span", [
                                _vm._v(_vm._s(packago.number_of_product))
                              ])
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      packago.price
                        ? _c(
                            "li",
                            {
                              staticStyle: { "font-family": "AlmaraiRegular" }
                            },
                            [
                              _vm._v(_vm._s(_vm.$t("price"))),
                              _c("span", [
                                _vm._v(
                                  _vm._s(packago.price + " " + _vm.$t("rs")) +
                                    " "
                                )
                              ])
                            ]
                          )
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("br"),
                    _vm._v(" "),
                    _c(
                      "a",
                      {
                        staticClass: "subscribe",
                        staticStyle: {
                          width: "130px",
                          "font-family": "AlmaraiRegular",
                          opacity: "1"
                        },
                        attrs: { href: "#" }
                      },
                      [
                        _vm._v(
                          "\n                                    " +
                            _vm._s(_vm.$t("sub_now")) +
                            "\n                                "
                        )
                      ]
                    )
                  ])
                ])
              }),
              0
            )
          ])
        ]),
        _vm._v(" "),
        _c("img", {
          staticClass: "shape1",
          attrs: { src: "front/img/shapes/shape3.png" }
        }),
        _vm._v(" "),
        _c("img", {
          staticClass: "shape2",
          attrs: { src: "front/img/shapes/shape2.png" }
        })
      ]
    ),
    _vm._v(" "),
    _c("section", { staticClass: "turn-up" }, [
      _c("div", { staticClass: "container" }, [
        _c(
          "div",
          { staticClass: "row" },
          [
            _c("div", { staticClass: "col-12 text-center" }, [
              _c("div", { staticClass: "head" }, [
                _c(
                  "h2",
                  {
                    staticStyle: {
                      "font-family": "AlmaraiLight",
                      "font-style": "normal",
                      "font-weight": "500",
                      "font-size": "42px",
                      "line-height": "113.1%",
                      "text-align": "center"
                    }
                  },
                  [
                    _vm._v(_vm._s(_vm.$t("why_you_must")) + " "),
                    _c("span", [
                      _vm._v(_vm._s(_vm.$t("turn_up")) + " "),
                      _c("br"),
                      _vm._v(_vm._s(_vm.$t("to")))
                    ]),
                    _vm._v(" " + _vm._s(_vm.$t("tadween")) + " ")
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _vm._l(_vm.services, function(service) {
              return _c(
                "div",
                { staticClass: "col-6 col-lg-4 text-center mt-5 hvr-float" },
                [
                  _c("div", { staticClass: "body" }, [
                    _c("div", { staticClass: "icon" }, [
                      _c("img", {
                        attrs: { src: "/assets/images/service/" + service.icon }
                      })
                    ]),
                    _vm._v(" "),
                    _c(
                      "h2",
                      {
                        staticStyle: {
                          "font-family": "AlmaraiBold",
                          "font-style": "normal",
                          "font-weight": "800",
                          "font-size": "20px",
                          "line-height": "113.6%",
                          "text-align": "center"
                        }
                      },
                      [_vm._v(_vm._s(service["title_" + _vm.lang]))]
                    ),
                    _vm._v(" "),
                    _c(
                      "p",
                      {
                        staticStyle: {
                          "font-family": "AlmaraiRegular",
                          "font-style": "normal",
                          "font-weight": "normal",
                          "font-size": "18px",
                          "line-height": "150.69%",
                          "text-align": "center"
                        }
                      },
                      [_vm._v(_vm._s(service["description_" + _vm.lang]))]
                    )
                  ])
                ]
              )
            })
          ],
          2
        )
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "news", attrs: { id: "blogs-section" } }, [
      _c("div", { staticClass: "container" }, [
        _c(
          "div",
          { staticClass: "row" },
          [
            _c("div", { staticClass: "col-12 text-center" }, [
              _c("div", { staticClass: "head" }, [
                _c(
                  "h2",
                  {
                    staticStyle: {
                      "font-family": "AlmaraiBold",
                      "font-style": "normal",
                      "font-weight": "800",
                      "font-size": "35px",
                      "line-height": "118.1%"
                    }
                  },
                  [_vm._v(_vm._s(_vm.$t("last_news")))]
                ),
                _vm._v(" "),
                _c(
                  "p",
                  {
                    staticStyle: {
                      "font-family": "AlmaraiRegular",
                      "font-style": "normal",
                      "font-weight": "normal",
                      "font-size": "16px",
                      "line-height": "118.1%",
                      "text-align": "center"
                    }
                  },
                  [_vm._v(_vm._s(_vm.configs["news-description-" + _vm.lang]))]
                )
              ])
            ]),
            _vm._v(" "),
            _vm._l(_vm.news, function(newsSingle) {
              return _c(
                "div",
                {
                  staticClass:
                    "col-12 col-lg-4 col-md-6 text-md-left text-center mt-5"
                },
                [
                  _c("div", { staticClass: "body" }, [
                    _c("img", {
                      attrs: { src: "/assets/images/news/" + newsSingle.photo }
                    }),
                    _vm._v(" "),
                    _c(
                      "h3",
                      {
                        staticStyle: {
                          "font-family": "AlmaraiRegular",
                          "font-style": "normal",
                          "font-weight": "900",
                          "font-size": "16px",
                          "line-height": "28px"
                        }
                      },
                      [
                        _c(
                          "router-link",
                          {
                            attrs: { tag: "a", to: "/blogs/" + newsSingle.id }
                          },
                          [_vm._v(_vm._s(newsSingle["title_" + _vm.lang]))]
                        )
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "p",
                      {
                        staticStyle: {
                          margin: "auto",
                          "font-family": "AlmaraiRegular",
                          "font-style": "normal",
                          "font-weight": "normal",
                          "font-size": "14px",
                          "line-height": "24px"
                        }
                      },
                      [_vm._v(_vm._s(newsSingle["description_" + _vm.lang]))]
                    )
                  ])
                ]
              )
            })
          ],
          2
        )
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "download" }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12 text-center" }, [
            _c(
              "h2",
              {
                staticStyle: {
                  "font-family": "AlmaraiBold",
                  "font-style": "normal",
                  "font-weight": "900",
                  "font-size": "26px",
                  "line-height": "118.1%"
                }
              },
              [_vm._v(_vm._s(_vm.$t("download_app")))]
            ),
            _vm._v(" "),
            _c("div", { staticClass: "stores" }, [
              _c("a", { attrs: { href: _vm.configs["app-store-link"] } }, [
                _c("img", {
                  attrs: { src: "front/img/app-store.png", alt: "app-store" }
                })
              ]),
              _vm._v(" "),
              _c(
                "a",
                {
                  attrs: {
                    target: "_blank",
                    href: _vm.configs["google-play-link"]
                  }
                },
                [
                  _c("img", {
                    attrs: {
                      src: "front/img/google-play.png",
                      alt: "google-play"
                    }
                  })
                ]
              )
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-lg-6 col-12" }, [
      _c("div", { staticClass: "slides" }, [
        _c("img", {
          attrs: { src: "front/img/slides/iPhone%2011%20Pro%20Max%20-%202.png" }
        }),
        _vm._v(" "),
        _c("img", {
          attrs: { src: "front/img/slides/iPhone%2011%20Pro%20Max%20-%201.png" }
        }),
        _vm._v(" "),
        _c("img", {
          attrs: { src: "front/img/slides/iPhone%2011%20Pro%20Max%20-%203.png" }
        }),
        _vm._v(" "),
        _c("img", { attrs: { src: "front/img/shapes/shape1.png" } })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "video text-center" }, [
      _c("div", [
        _c("iframe", {
          staticStyle: { "border-radius": "20px" },
          attrs: {
            width: "550",
            height: "390",
            src: "https://www.youtube.com/embed/PuAbqwTDeh0",
            frameborder: "0",
            allow:
              "accelerometer; autoplay; encrypted-media;\n                                             gyroscope; picture-in-picture",
            allowfullscreen: ""
          }
        })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/front/FrontView.vue":
/*!****************************************************!*\
  !*** ./resources/js/src/views/front/FrontView.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _FrontView_vue_vue_type_template_id_91995c64___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./FrontView.vue?vue&type=template&id=91995c64& */ "./resources/js/src/views/front/FrontView.vue?vue&type=template&id=91995c64&");
/* harmony import */ var _FrontView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./FrontView.vue?vue&type=script&lang=js& */ "./resources/js/src/views/front/FrontView.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _FrontView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _FrontView_vue_vue_type_template_id_91995c64___WEBPACK_IMPORTED_MODULE_0__["render"],
  _FrontView_vue_vue_type_template_id_91995c64___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/front/FrontView.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/front/FrontView.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/src/views/front/FrontView.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FrontView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./FrontView.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/FrontView.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FrontView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/front/FrontView.vue?vue&type=template&id=91995c64&":
/*!***********************************************************************************!*\
  !*** ./resources/js/src/views/front/FrontView.vue?vue&type=template&id=91995c64& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FrontView_vue_vue_type_template_id_91995c64___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./FrontView.vue?vue&type=template&id=91995c64& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/FrontView.vue?vue&type=template&id=91995c64&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FrontView_vue_vue_type_template_id_91995c64___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FrontView_vue_vue_type_template_id_91995c64___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);