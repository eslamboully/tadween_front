(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[36],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/AboutView.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/front/AboutView.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../axios */ "./resources/js/src/axios.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      configs: [],
      news: [],
      packages: [],
      services: [],
      partners: [],
      testimonials: [],
      lang: this.$i18n.locale
    };
  },
  mounted: function mounted() {
    this.fetch_configs_data();
  },
  created: function created() {
    var _this = this;

    window.VueInstant.$on('change_locale', function (lang) {
      _this.lang = lang;
    });
  },
  methods: {
    fetch_configs_data: function fetch_configs_data() {
      var _this2 = this;

      _axios__WEBPACK_IMPORTED_MODULE_0__["default"].get("/front/configs").then(function (response) {
        _this2.configs = response.data.configs;
        _this2.news = response.data.news;
        _this2.packages = response.data.packages;
        _this2.services = response.data.services;
        _this2.partners = response.data.partners;
        _this2.testimonials = response.data.testimonials;
        var vm = _this2;
        window.Fire.nextTick(function () {
          $('.partner .owl-carousel').owlCarousel({
            loop: true,
            margin: 30,
            responsive: {
              0: {
                items: 3
              },
              600: {
                items: 3
              },
              1000: {
                items: 5
              }
            }
          });
        }).catch(function (error) {
          reject(error);
        });
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/AboutView.vue?vue&type=template&id=418bf332&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/front/AboutView.vue?vue&type=template&id=418bf332& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("section", { staticClass: "main" }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12 col-lg-6" }, [
            _c(
              "div",
              {
                class: "content text-" + (_vm.lang == "en" ? "left" : "right")
              },
              [
                _c(
                  "h1",
                  {
                    staticStyle: {
                      "font-family": "AlmaraiBold",
                      "font-style": "normal",
                      "font-weight": "800",
                      "font-size": "42px",
                      "line-height": "118.1%"
                    }
                  },
                  [_vm._v(_vm._s(_vm.configs["about-title-" + _vm.lang]))]
                ),
                _vm._v(" "),
                _c(
                  "p",
                  {
                    class: _vm.lang == "ar" ? "text-right" : "",
                    staticStyle: {
                      "font-family": "AlmaraiRegular",
                      "font-style": "normal",
                      "font-weight": "normal",
                      "font-size": "16px",
                      "line-height": "22px",
                      "text-align": "center"
                    }
                  },
                  [_vm._v(_vm._s(_vm.configs["about-description-" + _vm.lang]))]
                ),
                _vm._v(" "),
                _c(
                  "h2",
                  {
                    staticStyle: {
                      "font-family": "AlmaraiBold",
                      "font-style": "normal",
                      "font-weight": "900",
                      "font-size": "26px",
                      "line-height": "118.1%"
                    }
                  },
                  [_vm._v(_vm._s(_vm.$t("download_app")))]
                ),
                _vm._v(" "),
                _c("div", { staticClass: "download-images" }, [
                  _c("a", { attrs: { href: _vm.configs["app-store-link"] } }, [
                    _c("img", {
                      attrs: {
                        src: "front/img/app-store.png",
                        alt: "app-store"
                      }
                    })
                  ]),
                  _vm._v(" "),
                  _c(
                    "a",
                    {
                      attrs: {
                        target: "_blank",
                        href: _vm.configs["google-play-link"]
                      }
                    },
                    [
                      _c("img", {
                        attrs: {
                          src: "front/img/google-play.png",
                          alt: "google-play"
                        }
                      })
                    ]
                  )
                ])
              ]
            )
          ]),
          _vm._v(" "),
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "col-12 col-md-8 offset-md-2" }, [
            _c(
              "div",
              { staticClass: "review" },
              _vm._l(_vm.testimonials, function(testimonial) {
                return _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-12 col-lg-8" }, [
                    _c("div", { staticClass: "name" }, [
                      _c("div", { staticClass: "image" }, [
                        _c("img", {
                          attrs: {
                            src: testimonial.image,
                            alt: "preview",
                            width: "100%"
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "content" }, [
                        _c("h2", [
                          _vm._v(_vm._s(testimonial["name_" + _vm.lang]))
                        ]),
                        _vm._v(" "),
                        _c("h5", [
                          _vm._v(_vm._s(testimonial["job_title_" + _vm.lang]))
                        ])
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "desc" }, [
                      _c("p", [_vm._v(_vm._s(testimonial["body_" + _vm.lang]))])
                    ])
                  ])
                ])
              }),
              0
            )
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "turn-up" }, [
      _c("div", { staticClass: "container" }, [
        _c(
          "div",
          { staticClass: "row" },
          [
            _c("div", { staticClass: "col-12 text-center" }, [
              _c("div", { staticClass: "head" }, [
                _c(
                  "h2",
                  {
                    staticStyle: {
                      "font-family": "AlmaraiBold",
                      "font-style": "normal",
                      "font-weight": "500",
                      "font-size": "42px",
                      "line-height": "113.1%",
                      "text-align": "center"
                    }
                  },
                  [
                    _vm._v(_vm._s(_vm.$t("why_you_must")) + " "),
                    _c("span", [
                      _vm._v(_vm._s(_vm.$t("turn_up")) + " "),
                      _c("br"),
                      _vm._v(_vm._s(_vm.$t("to")))
                    ]),
                    _vm._v(" " + _vm._s(_vm.$t("tadween")) + " ")
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _vm._l(_vm.services, function(service) {
              return _c(
                "div",
                { staticClass: "col-6 col-lg-4 text-center mt-5 hvr-float" },
                [
                  _c("div", { staticClass: "body" }, [
                    _c("div", { staticClass: "icon" }, [
                      _c("img", {
                        attrs: { src: "/assets/images/service/" + service.icon }
                      })
                    ]),
                    _vm._v(" "),
                    _c(
                      "h2",
                      {
                        staticStyle: {
                          "font-family": "AlmaraiBold",
                          "font-style": "normal",
                          "font-weight": "800",
                          "font-size": "20px",
                          "line-height": "113.6%",
                          "text-align": "center"
                        }
                      },
                      [_vm._v(_vm._s(service["title_" + _vm.lang]))]
                    ),
                    _vm._v(" "),
                    _c(
                      "p",
                      {
                        staticStyle: {
                          "font-family": "AlmaraiRegular",
                          "font-style": "normal",
                          "font-weight": "normal",
                          "font-size": "18px",
                          "line-height": "150.69%",
                          "text-align": "center"
                        }
                      },
                      [_vm._v(_vm._s(service["description_" + _vm.lang]))]
                    )
                  ])
                ]
              )
            })
          ],
          2
        )
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "download" }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12 text-center" }, [
            _c("h2", [_vm._v(_vm._s(_vm.$t("download_app")) + " :")]),
            _vm._v(" "),
            _c("div", { staticClass: "stores" }, [
              _c("a", { attrs: { href: _vm.configs["app-store-link"] } }, [
                _c("img", {
                  attrs: { src: "front/img/app-store.png", alt: "app-store" }
                })
              ]),
              _vm._v(" "),
              _c(
                "a",
                {
                  attrs: {
                    target: "_blank",
                    href: _vm.configs["google-play-link"]
                  }
                },
                [
                  _c("img", {
                    attrs: {
                      src: "front/img/google-play.png",
                      alt: "google-play"
                    }
                  })
                ]
              )
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-12 col-lg-6" }, [
      _c("div", { staticClass: "image" }, [
        _c("img", { attrs: { src: "front/img/about.png", width: "100%;" } })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/front/AboutView.vue":
/*!****************************************************!*\
  !*** ./resources/js/src/views/front/AboutView.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AboutView_vue_vue_type_template_id_418bf332___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AboutView.vue?vue&type=template&id=418bf332& */ "./resources/js/src/views/front/AboutView.vue?vue&type=template&id=418bf332&");
/* harmony import */ var _AboutView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AboutView.vue?vue&type=script&lang=js& */ "./resources/js/src/views/front/AboutView.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _AboutView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AboutView_vue_vue_type_template_id_418bf332___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AboutView_vue_vue_type_template_id_418bf332___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/front/AboutView.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/front/AboutView.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/src/views/front/AboutView.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./AboutView.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/AboutView.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/front/AboutView.vue?vue&type=template&id=418bf332&":
/*!***********************************************************************************!*\
  !*** ./resources/js/src/views/front/AboutView.vue?vue&type=template&id=418bf332& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutView_vue_vue_type_template_id_418bf332___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./AboutView.vue?vue&type=template&id=418bf332& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/AboutView.vue?vue&type=template&id=418bf332&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutView_vue_vue_type_template_id_418bf332___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AboutView_vue_vue_type_template_id_418bf332___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);