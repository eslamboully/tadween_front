(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[26],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateOthers.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateOthers.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _store_eCommerce_moduleECommerce_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/store/eCommerce/moduleECommerce.js */ "./resources/js/src/store/eCommerce/moduleECommerce.js");
/* harmony import */ var _SettingsUpdateTabAccountOthers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SettingsUpdateTabAccountOthers */ "./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateTabAccountOthers.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// Store Module


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    SettingsUpdateTabAccountOthers: _SettingsUpdateTabAccountOthers__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  data: function data() {
    return {
      configs_data: null,
      config_not_found: false,

      /*
        This property is created for fetching latest data from API when tab is changed
         Please check it's watcher
      */
      activeTab: 0
    };
  },
  watch: {
    activeTab: function activeTab() {
      this.fetch_configs_data(this.$route.params.configsId);
    }
  },
  methods: {
    showUpdatedSuccess: function showUpdatedSuccess() {
      this.$vs.notify({
        color: 'success',
        title: 'Setting Updated',
        text: 'Setting was successfully updated'
      });
    },
    fetch_configs_data: function fetch_configs_data(configsId) {
      var _this = this;

      this.$store.dispatch('moduleECommerce/fetchConfigs', configsId).then(function (res) {
        _this.configs_data = res;
      }).catch(function (err) {
        if (err.response.status === 404) {
          _this.config_not_found = true;
          return;
        }

        console.error(err);
      });
    },
    chnage_tab: function chnage_tab(tab) {
      this.activeTab = tab;
      this.showUpdatedSuccess();
    }
  },
  created: function created() {
    // Register Module SettingManagement Module
    if (!_store_eCommerce_moduleECommerce_js__WEBPACK_IMPORTED_MODULE_0__["default"].isRegistered) {
      this.$store.registerModule('moduleECommerce', _store_eCommerce_moduleECommerce_js__WEBPACK_IMPORTED_MODULE_0__["default"]);
      _store_eCommerce_moduleECommerce_js__WEBPACK_IMPORTED_MODULE_0__["default"].isRegistered = true;
    }

    console.log(this.$route.params);
    this.fetch_configs_data(this.$route.params.configsId);
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateTabAccountOthers.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateTabAccountOthers.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-select */ "./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_select__WEBPACK_IMPORTED_MODULE_1__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    vSelect: vue_select__WEBPACK_IMPORTED_MODULE_1___default.a
  },
  props: {
    data: ''
  },
  created: function () {
    var _created = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    function created() {
      return _created.apply(this, arguments);
    }

    return created;
  }(),
  data: function data() {
    return {
      configs: this.data,
      current_photo: '',
      id: ''
    };
  },
  computed: {
    validateForm: function validateForm() {
      return !this.errors.any();
    }
  },
  methods: {
    capitalize: function capitalize(str) {
      return str.slice(0, 1).toUpperCase() + str.slice(1, str.length);
    },
    showError: function showError(message) {
      this.$vs.notify({
        color: 'danger',
        text: message
      });
    },
    showCreatedSuccess: function showCreatedSuccess() {
      this.$vs.notify({
        color: 'success',
        title: 'Services Updated',
        text: 'Services was successfully updated'
      });
    },
    save_changes: function save_changes() {
      var _this = this;

      this.$store.dispatch('moduleECommerce/updateConfigs', this.configs).then(function (res) {
        _this.$router.push({
          name: 'dashboard-settings-general'
        });

        _this.showCreatedSuccess();
      }).catch(function (err) {
        _this.showError(err);
      });
    },
    reset_configs: function reset_configs() {
      this.configs = JSON.parse(JSON.stringify(this.configs));
    },
    update_photo: function update_photo(event) {
      var file = event.target.files[0];
      this.configs.icon = file;
      this.current_photo = URL.createObjectURL(file);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateOthers.vue?vue&type=template&id=25a33462&":
/*!*********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateOthers.vue?vue&type=template&id=25a33462& ***!
  \*********************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { id: "page-admin-edit" } },
    [
      _c(
        "vs-alert",
        {
          attrs: {
            color: "danger",
            title: "News Not Found",
            active: _vm.config_not_found
          },
          on: {
            "update:active": function($event) {
              _vm.config_not_found = $event
            }
          }
        },
        [
          _c("span", [
            _vm._v(
              "News record with id: " +
                _vm._s(_vm.$route.params.configsId) +
                " not found. "
            )
          ]),
          _vm._v(" "),
          _c(
            "span",
            [
              _c("span", [_vm._v("Check ")]),
              _c(
                "router-link",
                {
                  staticClass: "text-inherit underline",
                  attrs: { to: { name: "page-config-list" } }
                },
                [_vm._v("All News")]
              )
            ],
            1
          )
        ]
      ),
      _vm._v(" "),
      _vm.configs_data
        ? _c("vx-card", [
            _c(
              "div",
              {
                staticClass: "tabs-container px-6 pt-6",
                attrs: { slot: "no-body" },
                slot: "no-body"
              },
              [
                _c(
                  "vs-tabs",
                  {
                    staticClass: "tab-action-btn-fill-conatiner",
                    model: {
                      value: _vm.activeTab,
                      callback: function($$v) {
                        _vm.activeTab = $$v
                      },
                      expression: "activeTab"
                    }
                  },
                  [
                    _c(
                      "vs-tab",
                      {
                        attrs: {
                          label: "Pages Settings",
                          "icon-pack": "feather",
                          icon: "icon-config"
                        }
                      },
                      [
                        _c(
                          "div",
                          { staticClass: "tab-text" },
                          [
                            _c("settings-update-tab-account-others", {
                              staticClass: "mt-4",
                              attrs: { data: _vm.configs_data },
                              on: { change_tab: _vm.chnage_tab }
                            })
                          ],
                          1
                        )
                      ]
                    )
                  ],
                  1
                )
              ],
              1
            )
          ])
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateTabAccountOthers.vue?vue&type=template&id=020de2cc&":
/*!*******************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateTabAccountOthers.vue?vue&type=template&id=020de2cc& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { id: "user-edit-tab-info" } },
    [
      _vm._l(_vm.configs, function(config) {
        return _c("div", { staticClass: "vx-row" }, [
          config.type == 1
            ? _c(
                "div",
                { staticClass: "vx-col md:w-1/2 w-full" },
                [
                  _c("vs-input", {
                    directives: [
                      {
                        name: "validate",
                        rawName: "v-validate",
                        value: "required",
                        expression: "'required'"
                      }
                    ],
                    staticClass: "w-full mt-4",
                    attrs: { label: config.var, name: config.var },
                    model: {
                      value: config.value_ar,
                      callback: function($$v) {
                        _vm.$set(config, "value_ar", $$v)
                      },
                      expression: "config.value_ar"
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "span",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.errors.has("title"),
                          expression: "errors.has('title')"
                        }
                      ],
                      staticClass: "text-danger text-sm"
                    },
                    [_vm._v(_vm._s(_vm.errors.first(config.var)))]
                  )
                ],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          config.type == 1
            ? _c(
                "div",
                { staticClass: "vx-col md:w-1/2 w-full" },
                [
                  _c("vs-input", {
                    directives: [
                      {
                        name: "validate",
                        rawName: "v-validate",
                        value: "required",
                        expression: "'required'"
                      }
                    ],
                    staticClass: "w-full mt-4",
                    attrs: { label: config.var, name: config.var },
                    model: {
                      value: config.value_en,
                      callback: function($$v) {
                        _vm.$set(config, "value_en", $$v)
                      },
                      expression: "config.value_en"
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "span",
                    {
                      directives: [
                        {
                          name: "show",
                          rawName: "v-show",
                          value: _vm.errors.has("title"),
                          expression: "errors.has('title')"
                        }
                      ],
                      staticClass: "text-danger text-sm"
                    },
                    [_vm._v(_vm._s(_vm.errors.first(config.var)))]
                  )
                ],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          config.type == 3
            ? _c("div", { staticClass: "vx-col md:w-1/2 w-full" }, [
                _c(
                  "div",
                  { staticClass: "mt-5" },
                  [
                    _c("vs-textarea", {
                      directives: [
                        {
                          name: "validate",
                          rawName: "v-validate",
                          value: "required",
                          expression: "'required'"
                        }
                      ],
                      staticClass: "w-full ckeditor",
                      attrs: { label: config.var, name: config.var },
                      model: {
                        value: config.value_ar,
                        callback: function($$v) {
                          _vm.$set(config, "value_ar", $$v)
                        },
                        expression: "config.value_ar"
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "span",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.errors.has("description"),
                            expression: "errors.has('description')"
                          }
                        ],
                        staticClass: "text-danger text-sm"
                      },
                      [_vm._v(_vm._s(_vm.errors.first(config.var)))]
                    )
                  ],
                  1
                )
              ])
            : _vm._e(),
          _vm._v(" "),
          config.type == 3
            ? _c("div", { staticClass: "vx-col md:w-1/2 w-full ckeditor" }, [
                _c(
                  "div",
                  { staticClass: "mt-5" },
                  [
                    _c("vs-textarea", {
                      directives: [
                        {
                          name: "validate",
                          rawName: "v-validate",
                          value: "required",
                          expression: "'required'"
                        }
                      ],
                      staticClass: "w-full",
                      attrs: { label: config.var, name: config.var },
                      model: {
                        value: config.value_en,
                        callback: function($$v) {
                          _vm.$set(config, "value_en", $$v)
                        },
                        expression: "config.value_en"
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "span",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.errors.has("description"),
                            expression: "errors.has('description')"
                          }
                        ],
                        staticClass: "text-danger text-sm"
                      },
                      [_vm._v(_vm._s(_vm.errors.first(config.var)))]
                    )
                  ],
                  1
                )
              ])
            : _vm._e()
        ])
      }),
      _vm._v(" "),
      _c("div", { staticClass: "vx-row" }, [
        _c("div", { staticClass: "vx-col w-full" }, [
          _c(
            "div",
            { staticClass: "mt-8 flex flex-wrap items-center justify-end" },
            [
              _c(
                "vs-button",
                {
                  staticClass: "ml-auto mt-2",
                  attrs: { disabled: !_vm.validateForm },
                  on: { click: _vm.save_changes }
                },
                [_vm._v("Save Changes")]
              ),
              _vm._v(" "),
              _c(
                "vs-button",
                {
                  staticClass: "ml-4 mt-2",
                  attrs: { type: "border", color: "warning" },
                  on: { click: _vm.reset_configs }
                },
                [_vm._v("Reset")]
              )
            ],
            1
          )
        ])
      ])
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateOthers.vue":
/*!********************************************************************************************!*\
  !*** ./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateOthers.vue ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SettingsUpdateOthers_vue_vue_type_template_id_25a33462___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SettingsUpdateOthers.vue?vue&type=template&id=25a33462& */ "./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateOthers.vue?vue&type=template&id=25a33462&");
/* harmony import */ var _SettingsUpdateOthers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SettingsUpdateOthers.vue?vue&type=script&lang=js& */ "./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateOthers.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _SettingsUpdateOthers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SettingsUpdateOthers_vue_vue_type_template_id_25a33462___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SettingsUpdateOthers_vue_vue_type_template_id_25a33462___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateOthers.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateOthers.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************!*\
  !*** ./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateOthers.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SettingsUpdateOthers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./SettingsUpdateOthers.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateOthers.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SettingsUpdateOthers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateOthers.vue?vue&type=template&id=25a33462&":
/*!***************************************************************************************************************************!*\
  !*** ./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateOthers.vue?vue&type=template&id=25a33462& ***!
  \***************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SettingsUpdateOthers_vue_vue_type_template_id_25a33462___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./SettingsUpdateOthers.vue?vue&type=template&id=25a33462& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateOthers.vue?vue&type=template&id=25a33462&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SettingsUpdateOthers_vue_vue_type_template_id_25a33462___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SettingsUpdateOthers_vue_vue_type_template_id_25a33462___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateTabAccountOthers.vue":
/*!******************************************************************************************************!*\
  !*** ./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateTabAccountOthers.vue ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SettingsUpdateTabAccountOthers_vue_vue_type_template_id_020de2cc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SettingsUpdateTabAccountOthers.vue?vue&type=template&id=020de2cc& */ "./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateTabAccountOthers.vue?vue&type=template&id=020de2cc&");
/* harmony import */ var _SettingsUpdateTabAccountOthers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SettingsUpdateTabAccountOthers.vue?vue&type=script&lang=js& */ "./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateTabAccountOthers.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _SettingsUpdateTabAccountOthers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SettingsUpdateTabAccountOthers_vue_vue_type_template_id_020de2cc___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SettingsUpdateTabAccountOthers_vue_vue_type_template_id_020de2cc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateTabAccountOthers.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateTabAccountOthers.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************!*\
  !*** ./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateTabAccountOthers.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SettingsUpdateTabAccountOthers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./SettingsUpdateTabAccountOthers.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateTabAccountOthers.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SettingsUpdateTabAccountOthers_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateTabAccountOthers.vue?vue&type=template&id=020de2cc&":
/*!*************************************************************************************************************************************!*\
  !*** ./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateTabAccountOthers.vue?vue&type=template&id=020de2cc& ***!
  \*************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SettingsUpdateTabAccountOthers_vue_vue_type_template_id_020de2cc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./SettingsUpdateTabAccountOthers.vue?vue&type=template&id=020de2cc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/settings/settings-update/SettingsUpdateTabAccountOthers.vue?vue&type=template&id=020de2cc&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SettingsUpdateTabAccountOthers_vue_vue_type_template_id_020de2cc___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SettingsUpdateTabAccountOthers_vue_vue_type_template_id_020de2cc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);