(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[41],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/PackagesView.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/front/PackagesView.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../axios */ "./resources/js/src/axios.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      configs: [],
      packages: [],
      lang: this.$i18n.locale
    };
  },
  mounted: function mounted() {
    this.fetch_configs_data();
  },
  created: function created() {
    var _this = this;

    window.VueInstant.$on('change_locale', function (lang) {
      _this.lang = lang;
    });
  },
  methods: {
    fetch_configs_data: function fetch_configs_data() {
      var _this2 = this;

      _axios__WEBPACK_IMPORTED_MODULE_0__["default"].get("/front/configs").then(function (response) {
        _this2.configs = response.data.configs;
        _this2.packages = response.data.packages;
        var vm = _this2;
        window.Fire.nextTick(function () {
          $('.packages .owl-carousel').owlCarousel({
            loop: true,
            margin: 30,
            nav: true,
            navText: ["<img src='/front/img/previos.png'>", "<img src='/front/img/next.png'>"],
            responsive: {
              0: {
                items: 1,
                stagePadding: 100,
                margin: 10
              },
              580: {
                items: 1,
                stagePadding: 200
              },
              1000: {
                items: 3
              }
            }
          });
          $('.partner .owl-carousel').owlCarousel({
            loop: true,
            margin: 30,
            responsive: {
              0: {
                items: 3
              },
              600: {
                items: 3
              },
              1000: {
                items: 5
              }
            }
          });
        }).catch(function (error) {
          reject(error);
        });
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/PackagesView.vue?vue&type=template&id=0219b7fc&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/front/PackagesView.vue?vue&type=template&id=0219b7fc& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "section",
      { staticClass: "packages", attrs: { id: "packages-section" } },
      [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12 text-center" }, [
              _c("div", { staticClass: "head" }, [
                _c("h1", [
                  _c(
                    "span",
                    {
                      staticStyle: {
                        "font-family": "AlmaraiBold",
                        "font-style": "normal",
                        "font-weight": "800",
                        "font-size": "42px",
                        "line-height": "118.1%"
                      }
                    },
                    [_vm._v(" " + _vm._s(_vm.$t("packages")))]
                  ),
                  _vm._v(" "),
                  _c(
                    "span",
                    {
                      staticStyle: {
                        "font-family": "AlmaraiBold",
                        "font-style": "normal",
                        "font-weight": "800",
                        "font-size": "42px",
                        "line-height": "118.1%",
                        color: "#00928A"
                      }
                    },
                    [_vm._v(" " + _vm._s(_vm.$t("tadween")))]
                  )
                ]),
                _vm._v(" "),
                _c(
                  "p",
                  {
                    staticStyle: {
                      "font-family": "AlmaraiBold",
                      "font-style": "normal",
                      "font-weight": "normal",
                      "font-size": "16px",
                      "line-height": "22px",
                      "text-align": "center"
                    }
                  },
                  [
                    _vm._v(
                      _vm._s(_vm.configs["packages-description-" + _vm.lang])
                    )
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "owl-carousel owl-theme" },
              _vm._l(_vm.packages, function(packago) {
                return _c("div", { class: "package " + packago }, [
                  _c("div", { staticClass: "head" }, [
                    _c(
                      "h2",
                      {
                        staticStyle: {
                          "font-family": "AlmaraiBold",
                          "font-style": "normal",
                          "font-weight": "900",
                          "font-size": "35px",
                          "line-height": "132%",
                          "align-items": "center",
                          "text-align": "center",
                          "letter-spacing": "0.25px"
                        }
                      },
                      [_vm._v(_vm._s(packago["title_" + _vm.lang]))]
                    ),
                    _vm._v(" "),
                    _c(
                      "p",
                      {
                        staticStyle: {
                          "font-family": "AlmaraiRegular",
                          "font-style": "normal",
                          "font-weight": "normal",
                          "font-size": "12px",
                          "line-height": "15px",
                          "align-items": "center",
                          "text-align": "center",
                          "letter-spacing": "0.25px"
                        }
                      },
                      [_vm._v(_vm._s(packago["description_" + _vm.lang]))]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "details" }, [
                    _c("ul", [
                      packago.salesman
                        ? _c(
                            "li",
                            {
                              staticStyle: { "font-family": "AlmaraiRegular" }
                            },
                            [
                              _vm._v(_vm._s(_vm.$t("Salesmans"))),
                              _c("span", [_vm._v(_vm._s(packago.salesman))])
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      packago.number_of_admins
                        ? _c(
                            "li",
                            {
                              staticStyle: { "font-family": "AlmaraiRegular" }
                            },
                            [
                              _vm._v(_vm._s(_vm.$t("num_of_admins"))),
                              _c("span", [
                                _vm._v(_vm._s(packago.number_of_admins))
                              ])
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      packago.number_of_customers
                        ? _c(
                            "li",
                            {
                              staticStyle: { "font-family": "AlmaraiRegular" }
                            },
                            [
                              _vm._v(_vm._s(_vm.$t("num_customers"))),
                              _c("span", [
                                _vm._v(_vm._s(packago.number_of_customers))
                              ])
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      packago.number_of_product
                        ? _c(
                            "li",
                            {
                              staticStyle: { "font-family": "AlmaraiRegular" }
                            },
                            [
                              _vm._v(_vm._s(_vm.$t("num_products"))),
                              _c("span", [
                                _vm._v(_vm._s(packago.number_of_product))
                              ])
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      packago.price
                        ? _c(
                            "li",
                            {
                              staticStyle: { "font-family": "AlmaraiRegular" }
                            },
                            [
                              _vm._v(_vm._s(_vm.$t("price"))),
                              _c("span", [
                                _vm._v(
                                  _vm._s(packago.price + " " + _vm.$t("rs")) +
                                    " "
                                )
                              ])
                            ]
                          )
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("br"),
                    _vm._v(" "),
                    _c(
                      "a",
                      {
                        staticClass: "subscribe",
                        staticStyle: {
                          width: "130px",
                          "font-family": "AlmaraiRegular",
                          opacity: "1"
                        },
                        attrs: { href: "#" }
                      },
                      [
                        _vm._v(
                          "\n                                  " +
                            _vm._s(_vm.$t("sub_now")) +
                            "\n                              "
                        )
                      ]
                    )
                  ])
                ])
              }),
              0
            )
          ])
        ]),
        _vm._v(" "),
        _c("img", {
          staticClass: "shape1",
          attrs: { src: "front/img/shapes/shape3.png" }
        }),
        _vm._v(" "),
        _c("img", {
          staticClass: "shape2",
          attrs: { src: "front/img/shapes/shape2.png" }
        })
      ]
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/front/PackagesView.vue":
/*!*******************************************************!*\
  !*** ./resources/js/src/views/front/PackagesView.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PackagesView_vue_vue_type_template_id_0219b7fc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PackagesView.vue?vue&type=template&id=0219b7fc& */ "./resources/js/src/views/front/PackagesView.vue?vue&type=template&id=0219b7fc&");
/* harmony import */ var _PackagesView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PackagesView.vue?vue&type=script&lang=js& */ "./resources/js/src/views/front/PackagesView.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PackagesView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PackagesView_vue_vue_type_template_id_0219b7fc___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PackagesView_vue_vue_type_template_id_0219b7fc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/front/PackagesView.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/front/PackagesView.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/src/views/front/PackagesView.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PackagesView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./PackagesView.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/PackagesView.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PackagesView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/front/PackagesView.vue?vue&type=template&id=0219b7fc&":
/*!**************************************************************************************!*\
  !*** ./resources/js/src/views/front/PackagesView.vue?vue&type=template&id=0219b7fc& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PackagesView_vue_vue_type_template_id_0219b7fc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./PackagesView.vue?vue&type=template&id=0219b7fc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/PackagesView.vue?vue&type=template&id=0219b7fc&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PackagesView_vue_vue_type_template_id_0219b7fc___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PackagesView_vue_vue_type_template_id_0219b7fc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);