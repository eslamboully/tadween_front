(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[27],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreate.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreate.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TestimonialCreateTab_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TestimonialCreateTab.vue */ "./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreateTab.vue");
/* harmony import */ var _store_eCommerce_moduleECommerce_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/store/eCommerce/moduleECommerce.js */ "./resources/js/src/store/eCommerce/moduleECommerce.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
 // Store Module


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    TestimonialCreateTab: _TestimonialCreateTab_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      news_data: null,
      news_not_found: false,

      /*
        This property is created for fetching latest data from API when tab is changed
         Please check it's watcher
      */
      activeTab: 0
    };
  },
  methods: {
    chnage_tab: function chnage_tab(tab) {
      this.activeTab = tab;
    }
  },
  created: function created() {
    // Register Module AdminManagement Module
    if (!_store_eCommerce_moduleECommerce_js__WEBPACK_IMPORTED_MODULE_1__["default"].isRegistered) {
      this.$store.registerModule('moduleECommerce', _store_eCommerce_moduleECommerce_js__WEBPACK_IMPORTED_MODULE_1__["default"]);
      _store_eCommerce_moduleECommerce_js__WEBPACK_IMPORTED_MODULE_1__["default"].isRegistered = true;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreateTab.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreateTab.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-select */ "./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue_select__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    vSelect: vue_select__WEBPACK_IMPORTED_MODULE_0___default.a
  },
  data: function data() {
    return {
      testimonial: {},
      current_photo: "/assets/images/users/default.png"
    };
  },
  computed: {
    validateForm: function validateForm() {
      return !this.errors.any();
    }
  },
  methods: {
    capitalize: function capitalize(str) {
      return str.slice(0, 1).toUpperCase() + str.slice(1, str.length);
    },
    showError: function showError(message) {
      this.$vs.notify({
        color: 'danger',
        text: message
      });
    },
    showCreatedSuccess: function showCreatedSuccess() {
      this.$vs.notify({
        color: 'success',
        title: 'testimonials Created',
        text: 'testimonials was successfully created'
      });
    },
    save_changes: function save_changes() {
      var _this = this;

      /* eslint-disable */
      if (!this.validateForm) return;
      this.$store.dispatch('moduleECommerce/saveNewtestimonials', this.testimonial).then(function (res) {
        _this.$router.push({
          name: 'dashboard-testimonials-list'
        });

        _this.showCreatedSuccess();
      }).catch(function (err) {
        _this.showError(err);
      });
    },
    reset_user: function reset_user() {
      this.testimonial = JSON.parse(JSON.stringify(this.testimonial));
    },
    update_photo: function update_photo(event) {
      var file = event.target.files[0];
      this.testimonial.image = file;
      this.current_photo = URL.createObjectURL(file);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreate.vue?vue&type=template&id=a26c226a&":
/*!**************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreate.vue?vue&type=template&id=a26c226a& ***!
  \**************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { id: "page-news-create" } },
    [
      _c("vx-card", [
        _c(
          "div",
          {
            staticClass: "tabs-container px-6 pt-6",
            attrs: { slot: "no-body" },
            slot: "no-body"
          },
          [
            _c(
              "vs-tabs",
              {
                staticClass: "tab-action-btn-fill-conatiner",
                model: {
                  value: _vm.activeTab,
                  callback: function($$v) {
                    _vm.activeTab = $$v
                  },
                  expression: "activeTab"
                }
              },
              [
                _c(
                  "vs-tab",
                  {
                    attrs: {
                      label: "Testimonial",
                      "icon-pack": "feather",
                      icon: "icon-news"
                    }
                  },
                  [
                    _c(
                      "div",
                      { staticClass: "tab-text" },
                      [
                        _c("testimonial-create-tab", {
                          staticClass: "mt-4",
                          on: { change_tab: _vm.chnage_tab }
                        })
                      ],
                      1
                    )
                  ]
                )
              ],
              1
            )
          ],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreateTab.vue?vue&type=template&id=1257aa7a&":
/*!*****************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreateTab.vue?vue&type=template&id=1257aa7a& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { attrs: { id: "user-edit-tab-info" } }, [
    _c("div", { staticClass: "vx-row" }, [
      _c("div", { staticClass: "vx-col w-full" }, [
        _c("div", { staticClass: "flex items-start flex-col sm:flex-row" }, [
          _c("img", {
            staticClass: "mr-8 rounded h-24 w-24",
            attrs: { src: _vm.current_photo }
          }),
          _vm._v(" "),
          _c(
            "div",
            [
              _c(
                "p",
                { staticClass: "text-lg font-medium mb-2 mt-4 sm:mt-0" },
                [_vm._v(_vm._s(_vm.testimonial.name))]
              ),
              _vm._v(" "),
              _c("input", {
                ref: "update_photo_input",
                staticClass: "hidden",
                attrs: { type: "file", accept: "image/*" },
                on: { change: _vm.update_photo }
              }),
              _vm._v(" "),
              _c(
                "vs-button",
                {
                  staticClass: "mr-4",
                  attrs: { type: "border" },
                  on: {
                    click: function($event) {
                      return _vm.$refs.update_photo_input.click()
                    }
                  }
                },
                [_vm._v("Change")]
              )
            ],
            1
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "vx-row" }, [
      _c(
        "div",
        { staticClass: "vx-col md:w-1/2 w-full" },
        [
          _c("vs-input", {
            directives: [
              {
                name: "validate",
                rawName: "v-validate",
                value: "required",
                expression: "'required'"
              }
            ],
            staticClass: "w-full mt-4",
            attrs: { label: "Name Arabic", name: "name_ar" },
            model: {
              value: _vm.testimonial.name_ar,
              callback: function($$v) {
                _vm.$set(_vm.testimonial, "name_ar", $$v)
              },
              expression: "testimonial.name_ar"
            }
          }),
          _vm._v(" "),
          _c(
            "span",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.errors.has("name_ar"),
                  expression: "errors.has('name_ar')"
                }
              ],
              staticClass: "text-danger text-sm"
            },
            [_vm._v(_vm._s(_vm.errors.first("name_ar")))]
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "mt-5" },
            [
              _c("vs-textarea", {
                directives: [
                  {
                    name: "validate",
                    rawName: "v-validate",
                    value: "required",
                    expression: "'required'"
                  }
                ],
                staticClass: "w-full",
                attrs: { label: "Body Arabic", name: "body_ar" },
                model: {
                  value: _vm.testimonial.body_ar,
                  callback: function($$v) {
                    _vm.$set(_vm.testimonial, "body_ar", $$v)
                  },
                  expression: "testimonial.body_ar"
                }
              }),
              _vm._v(" "),
              _c(
                "span",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value: _vm.errors.has("body_ar"),
                      expression: "errors.has('body_ar')"
                    }
                  ],
                  staticClass: "text-danger text-sm"
                },
                [_vm._v(_vm._s(_vm.errors.first("body_ar")))]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c("vs-input", {
            directives: [
              {
                name: "validate",
                rawName: "v-validate",
                value: "required",
                expression: "'required'"
              }
            ],
            staticClass: "w-full mt-4",
            attrs: { label: "Job Title Arabic", name: "job_title_ar" },
            model: {
              value: _vm.testimonial.job_title_ar,
              callback: function($$v) {
                _vm.$set(_vm.testimonial, "job_title_ar", $$v)
              },
              expression: "testimonial.job_title_ar"
            }
          }),
          _vm._v(" "),
          _c(
            "span",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.errors.has("job_title_ar"),
                  expression: "errors.has('job_title_ar')"
                }
              ],
              staticClass: "text-danger text-sm"
            },
            [_vm._v(_vm._s(_vm.errors.first("job_title_ar")))]
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "vx-col md:w-1/2 w-full" },
        [
          _c("vs-input", {
            directives: [
              {
                name: "validate",
                rawName: "v-validate",
                value: "required",
                expression: "'required'"
              }
            ],
            staticClass: "w-full mt-4",
            attrs: { label: "Name English", name: "name_en" },
            model: {
              value: _vm.testimonial.name_en,
              callback: function($$v) {
                _vm.$set(_vm.testimonial, "name_en", $$v)
              },
              expression: "testimonial.name_en"
            }
          }),
          _vm._v(" "),
          _c(
            "span",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.errors.has("name_en"),
                  expression: "errors.has('name_en')"
                }
              ],
              staticClass: "text-danger text-sm"
            },
            [_vm._v(_vm._s(_vm.errors.first("name_en")))]
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "mt-5" },
            [
              _c("vs-textarea", {
                directives: [
                  {
                    name: "validate",
                    rawName: "v-validate",
                    value: "required",
                    expression: "'required'"
                  }
                ],
                staticClass: "w-full",
                attrs: { label: "Body English", name: "body_en" },
                model: {
                  value: _vm.testimonial.body_en,
                  callback: function($$v) {
                    _vm.$set(_vm.testimonial, "body_en", $$v)
                  },
                  expression: "testimonial.body_en"
                }
              }),
              _vm._v(" "),
              _c(
                "span",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value: _vm.errors.has("body_en"),
                      expression: "errors.has('body_en')"
                    }
                  ],
                  staticClass: "text-danger text-sm"
                },
                [_vm._v(_vm._s(_vm.errors.first("body_en")))]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c("vs-input", {
            directives: [
              {
                name: "validate",
                rawName: "v-validate",
                value: "required",
                expression: "'required'"
              }
            ],
            staticClass: "w-full mt-4",
            attrs: { label: "Job Title English", name: "job_title_en" },
            model: {
              value: _vm.testimonial.job_title_en,
              callback: function($$v) {
                _vm.$set(_vm.testimonial, "job_title_en", $$v)
              },
              expression: "testimonial.job_title_en"
            }
          }),
          _vm._v(" "),
          _c(
            "span",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.errors.has("job_title_en"),
                  expression: "errors.has('job_title_en')"
                }
              ],
              staticClass: "text-danger text-sm"
            },
            [_vm._v(_vm._s(_vm.errors.first("job_title_en")))]
          )
        ],
        1
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "vx-row" }, [
      _c("div", { staticClass: "vx-col w-full" }, [
        _c(
          "div",
          { staticClass: "mt-8 flex flex-wrap items-center justify-end" },
          [
            _c(
              "vs-button",
              {
                staticClass: "ml-auto mt-2",
                attrs: { disabled: !_vm.validateForm },
                on: { click: _vm.save_changes }
              },
              [_vm._v("Save Changes")]
            )
          ],
          1
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreate.vue":
/*!*************************************************************************************************!*\
  !*** ./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreate.vue ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TestimonialCreate_vue_vue_type_template_id_a26c226a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TestimonialCreate.vue?vue&type=template&id=a26c226a& */ "./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreate.vue?vue&type=template&id=a26c226a&");
/* harmony import */ var _TestimonialCreate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TestimonialCreate.vue?vue&type=script&lang=js& */ "./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreate.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _TestimonialCreate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TestimonialCreate_vue_vue_type_template_id_a26c226a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TestimonialCreate_vue_vue_type_template_id_a26c226a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreate.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreate.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************!*\
  !*** ./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreate.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TestimonialCreate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./TestimonialCreate.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreate.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TestimonialCreate_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreate.vue?vue&type=template&id=a26c226a&":
/*!********************************************************************************************************************************!*\
  !*** ./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreate.vue?vue&type=template&id=a26c226a& ***!
  \********************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TestimonialCreate_vue_vue_type_template_id_a26c226a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./TestimonialCreate.vue?vue&type=template&id=a26c226a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreate.vue?vue&type=template&id=a26c226a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TestimonialCreate_vue_vue_type_template_id_a26c226a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TestimonialCreate_vue_vue_type_template_id_a26c226a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreateTab.vue":
/*!****************************************************************************************************!*\
  !*** ./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreateTab.vue ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TestimonialCreateTab_vue_vue_type_template_id_1257aa7a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TestimonialCreateTab.vue?vue&type=template&id=1257aa7a& */ "./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreateTab.vue?vue&type=template&id=1257aa7a&");
/* harmony import */ var _TestimonialCreateTab_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TestimonialCreateTab.vue?vue&type=script&lang=js& */ "./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreateTab.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _TestimonialCreateTab_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TestimonialCreateTab_vue_vue_type_template_id_1257aa7a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TestimonialCreateTab_vue_vue_type_template_id_1257aa7a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreateTab.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreateTab.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************!*\
  !*** ./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreateTab.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TestimonialCreateTab_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./TestimonialCreateTab.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreateTab.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TestimonialCreateTab_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreateTab.vue?vue&type=template&id=1257aa7a&":
/*!***********************************************************************************************************************************!*\
  !*** ./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreateTab.vue?vue&type=template&id=1257aa7a& ***!
  \***********************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TestimonialCreateTab_vue_vue_type_template_id_1257aa7a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./TestimonialCreateTab.vue?vue&type=template&id=1257aa7a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/testimonials/testimonials-create/TestimonialCreateTab.vue?vue&type=template&id=1257aa7a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TestimonialCreateTab_vue_vue_type_template_id_1257aa7a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TestimonialCreateTab_vue_vue_type_template_id_1257aa7a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);