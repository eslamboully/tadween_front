(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[76],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/product/product-edit/ProductEdit.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/dashboard/product/product-edit/ProductEdit.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProductEditTabInformation_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProductEditTabInformation.vue */ "./resources/js/src/views/dashboard/product/product-edit/ProductEditTabInformation.vue");
/* harmony import */ var _store_user_management_moduleUserManagement_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/store/user-management/moduleUserManagement.js */ "./resources/js/src/store/user-management/moduleUserManagement.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
 // Store Module


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    ProductEditTabInformation: _ProductEditTabInformation_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      product_data: null,
      product_not_found: false
    };
  },
  methods: {
    showUpdatedSuccess: function showUpdatedSuccess() {
      this.$vs.notify({
        color: 'success',
        title: 'Product Updated',
        text: 'Product was successfully updated'
      });
    },
    fetch_product_data: function fetch_product_data(productId) {
      var _this = this;

      this.$store.dispatch('eCommerce/fetchProduct', productId).then(function (res) {
        _this.product_data = res;
      }).catch(function (err) {
        if (err.response.status === 404) {
          _this.product_not_found = true;
          return;
        }

        console.error(err);
      });
    }
  },
  created: function created() {
    // Register Module ProductManagement Module
    if (!_store_user_management_moduleUserManagement_js__WEBPACK_IMPORTED_MODULE_1__["default"].isRegistered) {
      this.$store.registerModule('userManagement', _store_user_management_moduleUserManagement_js__WEBPACK_IMPORTED_MODULE_1__["default"]);
      _store_user_management_moduleUserManagement_js__WEBPACK_IMPORTED_MODULE_1__["default"].isRegistered = true;
    }

    this.fetch_product_data(this.$route.params.productId);
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/product/product-edit/ProductEditTabInformation.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/dashboard/product/product-edit/ProductEditTabInformation.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-select */ "./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_select__WEBPACK_IMPORTED_MODULE_1__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    vSelect: vue_select__WEBPACK_IMPORTED_MODULE_1___default.a
  },
  props: {
    data: {
      required: true
    }
  },
  created: function () {
    var _created = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var _this = this;

      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return this.$store.dispatch('eCommerce/fetchStores').then(function (stores) {
                _this.stores = stores.length > 0 ? stores : [];
              });

            case 2:
              _context.next = 4;
              return this.$store.dispatch('global/fetchCategories').then(function (categories) {
                _this.categories = categories;
              });

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    function created() {
      return _created.apply(this, arguments);
    }

    return created;
  }(),
  data: function data() {
    return {
      product: this.data,
      stores: [],
      categories: [],
      current_photo: this.data.photo
    };
  },
  computed: {
    validateForm: function validateForm() {
      return !this.errors.any();
    },
    options: function options() {
      var _this2 = this;

      var types = [];

      if (this.stores.length > 0) {
        this.stores.filter(function (store) {
          var option = {
            label: _this2.capitalize(store.name),
            value: store.id
          };
          types.push(option);
        });
      }

      return types;
    },
    categories_options: function categories_options() {
      var _this3 = this;

      var types = [];
      this.categories.filter(function (category) {
        var option = {
          label: _this3.capitalize(category.name),
          value: category.id
        };
        types.push(option);
      });
      return types;
    },
    store_local: {
      get: function get() {
        var _this4 = this;

        var my_status = this.options.filter(function (store) {
          return store.value == _this4.product.store_id;
        });
        return my_status[0];
      },
      set: function set(obj) {
        this.product.store_id = obj.value;
      }
    },
    category_local: {
      get: function get() {
        var _this5 = this;

        var my_status = this.categories_options.filter(function (category) {
          return category.value == _this5.product.category_id;
        });
        return my_status[0];
      },
      set: function set(obj) {
        this.product.category_id = obj.value;
      }
    }
  },
  methods: {
    showUpdatedSuccess: function showUpdatedSuccess() {
      this.$vs.notify({
        color: 'success',
        title: 'Product Updated',
        text: 'Product was successfully updated'
      });
    },
    showError: function showError(message) {
      this.$vs.notify({
        color: 'danger',
        text: message
      });
    },
    capitalize: function capitalize(str) {
      return str.slice(0, 1).toUpperCase() + str.slice(1, str.length);
    },
    save_changes: function save_changes() {
      var _this6 = this;

      /* eslint-disable */
      if (!this.validateForm) return;
      this.$store.dispatch('eCommerce/updateProduct', this.product).then(function (res) {
        _this6.$router.push({
          name: 'dashboard-product-list'
        });

        _this6.showUpdatedSuccess();
      }).catch(function (err) {
        _this6.showError(err);
      });
    },
    reset_store: function reset_store() {
      this.product = {};
    },
    update_photo: function update_photo(event) {
      var file = event.target.files[0];
      this.product.photo = file;
      this.current_photo = URL.createObjectURL(file);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/product/product-edit/ProductEdit.vue?vue&type=template&id=3ac75957&":
/*!********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/dashboard/product/product-edit/ProductEdit.vue?vue&type=template&id=3ac75957& ***!
  \********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { id: "page-product-edit" } },
    [
      _c(
        "vs-alert",
        {
          attrs: {
            color: "danger",
            title: "Product Not Found",
            active: _vm.product_not_found
          },
          on: {
            "update:active": function($event) {
              _vm.product_not_found = $event
            }
          }
        },
        [
          _c("span", [
            _vm._v(
              "Product record with id: " +
                _vm._s(_vm.$route.params.productId) +
                " not found. "
            )
          ]),
          _vm._v(" "),
          _c(
            "span",
            [
              _c("span", [_vm._v("Check ")]),
              _c(
                "router-link",
                {
                  staticClass: "text-inherit underline",
                  attrs: { to: { name: "page-product-list" } }
                },
                [_vm._v("All Products")]
              )
            ],
            1
          )
        ]
      ),
      _vm._v(" "),
      _vm.product_data
        ? _c("vx-card", [
            _c(
              "div",
              {
                staticClass: "tabs-container px-6 pt-6",
                attrs: { slot: "no-body" },
                slot: "no-body"
              },
              [
                _c(
                  "vs-tabs",
                  { staticClass: "tab-action-btn-fill-conatiner" },
                  [
                    _c(
                      "vs-tab",
                      {
                        attrs: {
                          label: "Information",
                          "icon-pack": "feather",
                          icon: "icon-info"
                        }
                      },
                      [
                        _c(
                          "div",
                          { staticClass: "tab-text" },
                          [
                            _c("product-edit-tab-information", {
                              staticClass: "mt-4",
                              attrs: { data: _vm.product_data }
                            })
                          ],
                          1
                        )
                      ]
                    )
                  ],
                  1
                )
              ],
              1
            )
          ])
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/product/product-edit/ProductEditTabInformation.vue?vue&type=template&id=d6f3d3e4&":
/*!**********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/dashboard/product/product-edit/ProductEditTabInformation.vue?vue&type=template&id=d6f3d3e4& ***!
  \**********************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { attrs: { id: "store-edit-tab-info" } }, [
    _c("div", { staticClass: "vx-row" }, [
      _c("div", { staticClass: "vx-col w-full" }, [
        _c("div", { staticClass: "flex items-start flex-col sm:flex-row" }, [
          _c("img", {
            staticClass: "mr-8 rounded h-24 w-24",
            attrs: { src: _vm.current_photo }
          }),
          _vm._v(" "),
          _c(
            "div",
            [
              _c(
                "p",
                { staticClass: "text-lg font-medium mb-2 mt-4 sm:mt-0" },
                [_vm._v(_vm._s(_vm.product.name))]
              ),
              _vm._v(" "),
              _c("input", {
                ref: "update_photo_input",
                staticClass: "hidden",
                attrs: { type: "file", accept: "image/*" },
                on: { change: _vm.update_photo }
              }),
              _vm._v(" "),
              _c(
                "vs-button",
                {
                  staticClass: "mr-4",
                  attrs: { type: "border" },
                  on: {
                    click: function($event) {
                      return _vm.$refs.update_photo_input.click()
                    }
                  }
                },
                [_vm._v("Change Product Photo")]
              )
            ],
            1
          )
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "vx-row" }, [
      _c(
        "div",
        { staticClass: "vx-col md:w-1/2 w-full" },
        [
          _c("vs-input", {
            directives: [
              {
                name: "validate",
                rawName: "v-validate",
                value: "required",
                expression: "'required'"
              }
            ],
            staticClass: "w-full mt-4",
            attrs: { label: "Name", name: "name" },
            model: {
              value: _vm.product.name,
              callback: function($$v) {
                _vm.$set(_vm.product, "name", $$v)
              },
              expression: "product.name"
            }
          }),
          _vm._v(" "),
          _c(
            "span",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.errors.has("name"),
                  expression: "errors.has('name')"
                }
              ],
              staticClass: "text-danger text-sm"
            },
            [_vm._v(_vm._s(_vm.errors.first("name")))]
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "mt-4" },
            [
              _c("label", { staticClass: "vs-input--label" }, [
                _vm._v("Store")
              ]),
              _vm._v(" "),
              _c("v-select", {
                directives: [
                  {
                    name: "validate",
                    rawName: "v-validate",
                    value: "required",
                    expression: "'required'"
                  }
                ],
                attrs: {
                  clearable: false,
                  options: _vm.options,
                  name: "store_id",
                  dir: _vm.$vs.rtl ? "rtl" : "ltr"
                },
                model: {
                  value: _vm.store_local,
                  callback: function($$v) {
                    _vm.store_local = $$v
                  },
                  expression: "store_local"
                }
              }),
              _vm._v(" "),
              _c(
                "span",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value: _vm.errors.has("store_id"),
                      expression: "errors.has('store_id')"
                    }
                  ],
                  staticClass: "text-danger text-sm"
                },
                [_vm._v(_vm._s(_vm.errors.first("store_id")))]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c("vs-input", {
            directives: [
              {
                name: "validate",
                rawName: "v-validate",
                value: "required|numeric",
                expression: "'required|numeric'"
              }
            ],
            staticClass: "w-full mt-4",
            attrs: { label: "Price", name: "price" },
            model: {
              value: _vm.product.price,
              callback: function($$v) {
                _vm.$set(_vm.product, "price", $$v)
              },
              expression: "product.price"
            }
          }),
          _vm._v(" "),
          _c(
            "span",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.errors.has("price"),
                  expression: "errors.has('price')"
                }
              ],
              staticClass: "text-danger text-sm"
            },
            [_vm._v(_vm._s(_vm.errors.first("price")))]
          ),
          _vm._v(" "),
          _c("vs-input", {
            directives: [
              {
                name: "validate",
                rawName: "v-validate",
                value: "required|numeric",
                expression: "'required|numeric'"
              }
            ],
            staticClass: "w-full mt-4",
            attrs: { label: "Stock", type: "text", name: "stock" },
            model: {
              value: _vm.product.stock,
              callback: function($$v) {
                _vm.$set(_vm.product, "stock", $$v)
              },
              expression: "product.stock"
            }
          }),
          _vm._v(" "),
          _c(
            "span",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.errors.has("stock"),
                  expression: "errors.has('stock')"
                }
              ],
              staticClass: "text-danger text-sm"
            },
            [_vm._v(_vm._s(_vm.errors.first("stock")))]
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "vx-col md:w-1/2 w-full" },
        [
          _c("vs-input", {
            directives: [
              {
                name: "validate",
                rawName: "v-validate",
                value: "required",
                expression: "'required'"
              }
            ],
            staticClass: "w-full mt-4",
            attrs: { label: "Qr Code", type: "text", name: "barcode" },
            model: {
              value: _vm.product.barcode,
              callback: function($$v) {
                _vm.$set(_vm.product, "barcode", $$v)
              },
              expression: "product.barcode"
            }
          }),
          _vm._v(" "),
          _c(
            "span",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.errors.has("barcode"),
                  expression: "errors.has('barcode')"
                }
              ],
              staticClass: "text-danger text-sm"
            },
            [_vm._v(_vm._s(_vm.errors.first("barcode")))]
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "mt-4" },
            [
              _c("label", { staticClass: "vs-input--label" }, [
                _vm._v("Category ID")
              ]),
              _vm._v(" "),
              _c("v-select", {
                directives: [
                  {
                    name: "validate",
                    rawName: "v-validate",
                    value: "required",
                    expression: "'required'"
                  }
                ],
                attrs: {
                  clearable: false,
                  options: _vm.categories_options,
                  name: "category_id",
                  dir: _vm.$vs.rtl ? "rtl" : "ltr"
                },
                model: {
                  value: _vm.category_local,
                  callback: function($$v) {
                    _vm.category_local = $$v
                  },
                  expression: "category_local"
                }
              }),
              _vm._v(" "),
              _c(
                "span",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value: _vm.errors.has("category_id"),
                      expression: "errors.has('category_id')"
                    }
                  ],
                  staticClass: "text-danger text-sm"
                },
                [_vm._v(_vm._s(_vm.errors.first("category_id")))]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c("vs-textarea", {
            directives: [
              {
                name: "validate",
                rawName: "v-validate",
                value: "required",
                expression: "'required'"
              }
            ],
            staticClass: "w-full mt-4",
            attrs: { label: "Details", type: "text", name: "details" },
            model: {
              value: _vm.product.details,
              callback: function($$v) {
                _vm.$set(_vm.product, "details", $$v)
              },
              expression: "product.details"
            }
          }),
          _vm._v(" "),
          _c(
            "span",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.errors.has("details"),
                  expression: "errors.has('details')"
                }
              ],
              staticClass: "text-danger text-sm"
            },
            [_vm._v(_vm._s(_vm.errors.first("details")))]
          )
        ],
        1
      )
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "vx-row" }, [
      _c("div", { staticClass: "vx-col w-full" }, [
        _c(
          "div",
          { staticClass: "mt-8 flex flex-wrap items-center justify-end" },
          [
            _c(
              "vs-button",
              {
                staticClass: "ml-auto mt-2",
                attrs: { disabled: !_vm.validateForm },
                on: { click: _vm.save_changes }
              },
              [_vm._v("Save Changes")]
            )
          ],
          1
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/dashboard/product/product-edit/ProductEdit.vue":
/*!*******************************************************************************!*\
  !*** ./resources/js/src/views/dashboard/product/product-edit/ProductEdit.vue ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProductEdit_vue_vue_type_template_id_3ac75957___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProductEdit.vue?vue&type=template&id=3ac75957& */ "./resources/js/src/views/dashboard/product/product-edit/ProductEdit.vue?vue&type=template&id=3ac75957&");
/* harmony import */ var _ProductEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProductEdit.vue?vue&type=script&lang=js& */ "./resources/js/src/views/dashboard/product/product-edit/ProductEdit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ProductEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProductEdit_vue_vue_type_template_id_3ac75957___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProductEdit_vue_vue_type_template_id_3ac75957___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/dashboard/product/product-edit/ProductEdit.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/dashboard/product/product-edit/ProductEdit.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************!*\
  !*** ./resources/js/src/views/dashboard/product/product-edit/ProductEdit.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductEdit.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/product/product-edit/ProductEdit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductEdit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/dashboard/product/product-edit/ProductEdit.vue?vue&type=template&id=3ac75957&":
/*!**************************************************************************************************************!*\
  !*** ./resources/js/src/views/dashboard/product/product-edit/ProductEdit.vue?vue&type=template&id=3ac75957& ***!
  \**************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductEdit_vue_vue_type_template_id_3ac75957___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductEdit.vue?vue&type=template&id=3ac75957& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/product/product-edit/ProductEdit.vue?vue&type=template&id=3ac75957&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductEdit_vue_vue_type_template_id_3ac75957___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductEdit_vue_vue_type_template_id_3ac75957___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/src/views/dashboard/product/product-edit/ProductEditTabInformation.vue":
/*!*********************************************************************************************!*\
  !*** ./resources/js/src/views/dashboard/product/product-edit/ProductEditTabInformation.vue ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProductEditTabInformation_vue_vue_type_template_id_d6f3d3e4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProductEditTabInformation.vue?vue&type=template&id=d6f3d3e4& */ "./resources/js/src/views/dashboard/product/product-edit/ProductEditTabInformation.vue?vue&type=template&id=d6f3d3e4&");
/* harmony import */ var _ProductEditTabInformation_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProductEditTabInformation.vue?vue&type=script&lang=js& */ "./resources/js/src/views/dashboard/product/product-edit/ProductEditTabInformation.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ProductEditTabInformation_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProductEditTabInformation_vue_vue_type_template_id_d6f3d3e4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProductEditTabInformation_vue_vue_type_template_id_d6f3d3e4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/dashboard/product/product-edit/ProductEditTabInformation.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/dashboard/product/product-edit/ProductEditTabInformation.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************!*\
  !*** ./resources/js/src/views/dashboard/product/product-edit/ProductEditTabInformation.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductEditTabInformation_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductEditTabInformation.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/product/product-edit/ProductEditTabInformation.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductEditTabInformation_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/dashboard/product/product-edit/ProductEditTabInformation.vue?vue&type=template&id=d6f3d3e4&":
/*!****************************************************************************************************************************!*\
  !*** ./resources/js/src/views/dashboard/product/product-edit/ProductEditTabInformation.vue?vue&type=template&id=d6f3d3e4& ***!
  \****************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductEditTabInformation_vue_vue_type_template_id_d6f3d3e4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductEditTabInformation.vue?vue&type=template&id=d6f3d3e4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/product/product-edit/ProductEditTabInformation.vue?vue&type=template&id=d6f3d3e4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductEditTabInformation_vue_vue_type_template_id_d6f3d3e4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductEditTabInformation_vue_vue_type_template_id_d6f3d3e4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);