(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[37],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/BlogDetailsView.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/front/BlogDetailsView.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../axios */ "./resources/js/src/axios.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      configs: [],
      news: [],
      single: [],
      lang: this.$i18n.locale
    };
  },
  mounted: function mounted() {
    this.fetch_configs_data();
    this.fetch_news_single_data();
  },
  created: function created() {
    var _this = this;

    window.VueInstant.$on('change_locale', function (lang) {
      _this.lang = lang;
    });
  },
  methods: {
    fetch_configs_data: function fetch_configs_data() {
      var _this2 = this;

      _axios__WEBPACK_IMPORTED_MODULE_0__["default"].get("/front/configs").then(function (response) {
        _this2.configs = response.data.configs;
        _this2.news = response.data.news;
      });
    },
    fetch_news_single_data: function fetch_news_single_data() {
      var _this3 = this;

      _axios__WEBPACK_IMPORTED_MODULE_0__["default"].get("/dashboard/news-single/".concat(this.$route.params.id)).then(function (response) {
        _this3.single = response.data;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/BlogDetailsView.vue?vue&type=template&id=7065d6e5&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/front/BlogDetailsView.vue?vue&type=template&id=7065d6e5& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("section", { staticClass: "blog-details" }, [
      _c("div", { staticClass: "container" }, [
        _c(
          "div",
          { class: _vm.lang == "ar" ? "row flex-row-reverse" : "row" },
          [
            _c("div", { staticClass: "col-12" }, [
              _c("div", { staticClass: "head" }, [
                _c("h2", [_vm._v(_vm._s(this.$route.params.title))])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-12 col-md-6 order-md-1 order-12" }, [
              _c("div", { staticClass: "blogs row" }, [
                _c("div", { staticClass: "col-12" }, [
                  _c("div", { staticClass: "image" }, [
                    _c("img", {
                      attrs: { src: "/assets/images/news/" + _vm.single.photo }
                    })
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-12 col-md-6 order-md-12 order-1" }, [
              _c("div", { staticClass: "blog-body" }, [
                _c("div", { staticClass: "blog-date" }),
                _vm._v(" "),
                _c("div", { staticClass: "desc" }, [
                  _c("h3", { class: _vm.lang == "ar" ? "text-right" : "" }, [
                    _vm._v(_vm._s(_vm.single["title_" + _vm.lang]))
                  ]),
                  _vm._v(" "),
                  _c("p", { class: _vm.lang == "ar" ? "text-right" : "" }, [
                    _vm._v(
                      "\n                                    " +
                        _vm._s(_vm.single["description_" + _vm.lang]) +
                        "\n                                "
                    )
                  ])
                ])
              ])
            ])
          ]
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/front/BlogDetailsView.vue":
/*!**********************************************************!*\
  !*** ./resources/js/src/views/front/BlogDetailsView.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BlogDetailsView_vue_vue_type_template_id_7065d6e5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BlogDetailsView.vue?vue&type=template&id=7065d6e5& */ "./resources/js/src/views/front/BlogDetailsView.vue?vue&type=template&id=7065d6e5&");
/* harmony import */ var _BlogDetailsView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BlogDetailsView.vue?vue&type=script&lang=js& */ "./resources/js/src/views/front/BlogDetailsView.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _BlogDetailsView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _BlogDetailsView_vue_vue_type_template_id_7065d6e5___WEBPACK_IMPORTED_MODULE_0__["render"],
  _BlogDetailsView_vue_vue_type_template_id_7065d6e5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/front/BlogDetailsView.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/front/BlogDetailsView.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/src/views/front/BlogDetailsView.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogDetailsView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./BlogDetailsView.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/BlogDetailsView.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogDetailsView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/front/BlogDetailsView.vue?vue&type=template&id=7065d6e5&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/src/views/front/BlogDetailsView.vue?vue&type=template&id=7065d6e5& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogDetailsView_vue_vue_type_template_id_7065d6e5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./BlogDetailsView.vue?vue&type=template&id=7065d6e5& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/BlogDetailsView.vue?vue&type=template&id=7065d6e5&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogDetailsView_vue_vue_type_template_id_7065d6e5___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogDetailsView_vue_vue_type_template_id_7065d6e5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);