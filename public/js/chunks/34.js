(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[34],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/main/Front.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/layouts/main/Front.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../axios */ "./resources/js/src/axios.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {},
  data: function data() {
    return {
      configs: [],
      lang: 'ar',
      email_newsletter: ''
    };
  },
  watch: {
    '$route': function $route() {
      this.routeTitle = this.$route.meta.pageTitle;
    }
  },
  computed: {},
  methods: {
    showSuccessMessage: function showSuccessMessage() {
      this.$vs.notify({
        color: 'success',
        title: 'Success',
        text: 'Proccess Done successfully'
      });
    },
    showError: function showError(message) {
      this.$vs.notify({
        color: 'danger',
        text: message
      });
    },
    newsletter_subscribe: function () {
      var _newsletter_subscribe = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var _this = this;

        var email;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                email = this.email_newsletter;
                _context.next = 3;
                return this.$vs.loading();

              case 3:
                _context.next = 5;
                return _axios__WEBPACK_IMPORTED_MODULE_1__["default"].post('/newsletter', {
                  email: email
                }).then(function (res) {
                  _this.showSuccessMessage();

                  _this.$vs.loading.close();
                }).catch(function (message) {
                  _this.$vs.loading.close();

                  _this.showError(_this.$t('invalid_email'));
                });

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function newsletter_subscribe() {
        return _newsletter_subscribe.apply(this, arguments);
      }

      return newsletter_subscribe;
    }(),
    fetch_configs_data: function fetch_configs_data() {
      var _this2 = this;

      _axios__WEBPACK_IMPORTED_MODULE_1__["default"].get("/front/configs").then(function (response) {
        _this2.configs = response.data.configs;
      });
    },
    changeLang: function changeLang(lang) {
      this.lang = lang;
      this.$i18n.locale = this.lang;
      window.VueInstant.$emit('change_locale', this.lang);
    }
  },
  mounted: function mounted() {
    this.fetch_configs_data();
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/main/Front.vue?vue&type=template&id=55850182&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/layouts/main/Front.vue?vue&type=template&id=55850182& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _vm.lang == "ar"
        ? _c("link", {
            attrs: { rel: "stylesheet", href: "/front/css/main_rtl.css" }
          })
        : _vm._e(),
      _vm._v(" "),
      _vm.lang == "en"
        ? _c("link", {
            attrs: { rel: "stylesheet", href: "/front/css/main_ltr.css" }
          })
        : _vm._e(),
      _vm._v(" "),
      _vm.lang == "en"
        ? _c(
            "nav",
            { staticClass: "navbar navbar-expand-lg navbar-light bg-light" },
            [
              _c(
                "div",
                { staticClass: "container" },
                [
                  _c(
                    "router-link",
                    { staticClass: "navbar-brand", attrs: { to: "/" } },
                    [
                      _c("img", {
                        staticStyle: { width: "129px", height: "81px" },
                        attrs: {
                          src: "/front/img/logo.svg",
                          alt: "tadween logo",
                          width: "100%"
                        }
                      })
                    ]
                  ),
                  _vm._v(" "),
                  _vm._m(0),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "collapse navbar-collapse",
                      attrs: { id: "navbarNav" }
                    },
                    [
                      _c("ul", { staticClass: "navbar-nav m-auto" }, [
                        _c(
                          "li",
                          {
                            class:
                              this.$router.currentRoute.fullPath == "/"
                                ? "nav-item active"
                                : "nav-item",
                            staticStyle: { "font-family": "AlmaraiRegular" }
                          },
                          [
                            _c(
                              "router-link",
                              { staticClass: "nav-link", attrs: { to: "/" } },
                              [
                                _vm._v(_vm._s(_vm.$t("home")) + " "),
                                _c("span", { staticClass: "sr-only" }, [
                                  _vm._v("(current)")
                                ])
                              ]
                            ),
                            _vm._v(" "),
                            this.$router.currentRoute.fullPath == "/"
                              ? _c("img", {
                                  attrs: { src: "/front/img/arrow.png" }
                                })
                              : _vm._e()
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "li",
                          {
                            class:
                              this.$router.currentRoute.fullPath == "/packages"
                                ? "nav-item active"
                                : "nav-item",
                            staticStyle: { "font-family": "AlmaraiRegular" }
                          },
                          [
                            _c(
                              "router-link",
                              {
                                staticClass: "nav-link",
                                attrs: { to: "/packages" }
                              },
                              [_vm._v(_vm._s(_vm.$t("packages")))]
                            ),
                            _vm._v(" "),
                            this.$router.currentRoute.fullPath == "/packages"
                              ? _c("img", {
                                  attrs: { src: "/front/img/arrow.png" }
                                })
                              : _vm._e()
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "li",
                          {
                            class:
                              this.$router.currentRoute.fullPath == "/blogs"
                                ? "nav-item active"
                                : "nav-item",
                            staticStyle: { "font-family": "AlmaraiRegular" }
                          },
                          [
                            _c(
                              "router-link",
                              {
                                staticClass: "nav-link",
                                attrs: { to: "/blogs" }
                              },
                              [_vm._v(_vm._s(_vm.$t("blog")))]
                            ),
                            _vm._v(" "),
                            this.$router.currentRoute.fullPath == "/blogs"
                              ? _c("img", {
                                  attrs: { src: "/front/img/arrow.png" }
                                })
                              : _vm._e()
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "li",
                          {
                            class:
                              this.$router.currentRoute.fullPath == "/about-us"
                                ? "nav-item active"
                                : "nav-item",
                            staticStyle: { "font-family": "AlmaraiRegular" }
                          },
                          [
                            _c(
                              "router-link",
                              {
                                staticClass: "nav-link",
                                attrs: { to: "/about-us" }
                              },
                              [_vm._v(_vm._s(_vm.$t("aboutus")) + " ")]
                            ),
                            _vm._v(" "),
                            this.$router.currentRoute.fullPath == "/about-us"
                              ? _c("img", {
                                  attrs: { src: "/front/img/arrow.png" }
                                })
                              : _vm._e()
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("ul", { staticClass: "navbar-nav" }, [
                        _c(
                          "li",
                          {
                            class:
                              this.$router.currentRoute.fullPath == "/"
                                ? "nav-item active"
                                : "nav-item",
                            staticStyle: { "font-family": "AlmaraiRegular" }
                          },
                          [
                            _c(
                              "router-link",
                              {
                                staticClass: "nav-link",
                                attrs: { to: "/how-work" }
                              },
                              [
                                _vm._v(_vm._s(_vm.$t("howitwork")) + " "),
                                _c("span", { staticClass: "sr-only" }, [
                                  _vm._v("(current)")
                                ])
                              ]
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c("li", { staticClass: "nav-item active" }, [
                          _c(
                            "a",
                            {
                              staticClass: "nav-link button-mobile",
                              staticStyle: {
                                width: "125px",
                                "font-size": "20px",
                                color: "white",
                                "background-color": "#00928A"
                              },
                              attrs: { href: "" },
                              on: {
                                click: function($event) {
                                  $event.preventDefault()
                                  return _vm.changeLang("ar")
                                }
                              }
                            },
                            [_vm._v(_vm._s("عربى"))]
                          )
                        ])
                      ])
                    ]
                  )
                ],
                1
              )
            ]
          )
        : _c(
            "nav",
            { staticClass: "navbar navbar-expand-lg navbar-light bg-light" },
            [
              _c(
                "div",
                { staticClass: "container" },
                [
                  _c(
                    "div",
                    {
                      staticClass: "collapse navbar-collapse",
                      style: _vm.lang == "ar" ? "text-align:right" : "",
                      attrs: { id: "navbarNav" }
                    },
                    [
                      _c("ul", { staticClass: "navbar-nav" }, [
                        _c("li", { staticClass: "nav-item active" }, [
                          _c(
                            "a",
                            {
                              staticClass: "nav-link button-mobile",
                              staticStyle: {
                                width: "125px",
                                "font-size": "20px",
                                color: "white",
                                "background-color": "#00928A"
                              },
                              attrs: { href: "" },
                              on: {
                                click: function($event) {
                                  $event.preventDefault()
                                  return _vm.changeLang("en")
                                }
                              }
                            },
                            [_vm._v(_vm._s("English"))]
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "li",
                          {
                            class:
                              this.$router.currentRoute.fullPath == "/"
                                ? "nav-item active"
                                : "nav-item",
                            staticStyle: { "font-family": "AlmaraiRegular" }
                          },
                          [
                            _c(
                              "router-link",
                              {
                                staticClass: "nav-link",
                                attrs: { to: "/how-work" }
                              },
                              [
                                _vm._v(_vm._s(_vm.$t("howitwork")) + " "),
                                _c("span", { staticClass: "sr-only" }, [
                                  _vm._v("(current)")
                                ])
                              ]
                            )
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("ul", { staticClass: "navbar-nav" }, [
                        _c(
                          "li",
                          {
                            class:
                              this.$router.currentRoute.fullPath == "/about-us"
                                ? "nav-item active"
                                : "nav-item",
                            staticStyle: { "font-family": "AlmaraiRegular" }
                          },
                          [
                            _c(
                              "router-link",
                              {
                                staticClass: "nav-link",
                                attrs: { to: "/about-us" }
                              },
                              [_vm._v(_vm._s(_vm.$t("aboutus")) + " ")]
                            ),
                            _vm._v(" "),
                            this.$router.currentRoute.fullPath == "/about-us"
                              ? _c("img", {
                                  attrs: { src: "/front/img/arrow.png" }
                                })
                              : _vm._e()
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "li",
                          {
                            class:
                              this.$router.currentRoute.fullPath == "/blogs"
                                ? "nav-item active"
                                : "nav-item",
                            staticStyle: { "font-family": "AlmaraiRegular" }
                          },
                          [
                            _c(
                              "router-link",
                              {
                                staticClass: "nav-link",
                                attrs: { to: "/blogs" }
                              },
                              [_vm._v(_vm._s(_vm.$t("blog")))]
                            ),
                            _vm._v(" "),
                            this.$router.currentRoute.fullPath == "/blogs"
                              ? _c("img", {
                                  attrs: { src: "/front/img/arrow.png" }
                                })
                              : _vm._e()
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "li",
                          {
                            class:
                              this.$router.currentRoute.fullPath == "/packages"
                                ? "nav-item active"
                                : "nav-item",
                            staticStyle: { "font-family": "AlmaraiRegular" }
                          },
                          [
                            _c(
                              "router-link",
                              {
                                staticClass: "nav-link",
                                attrs: { to: "/packages" }
                              },
                              [_vm._v(_vm._s(_vm.$t("packages")))]
                            ),
                            _vm._v(" "),
                            this.$router.currentRoute.fullPath == "/packages"
                              ? _c("img", {
                                  attrs: { src: "/front/img/arrow.png" }
                                })
                              : _vm._e()
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "li",
                          {
                            class:
                              this.$router.currentRoute.fullPath == "/"
                                ? "nav-item active"
                                : "nav-item",
                            staticStyle: { "font-family": "AlmaraiRegular" }
                          },
                          [
                            _c(
                              "router-link",
                              { staticClass: "nav-link", attrs: { to: "/" } },
                              [
                                _vm._v(_vm._s(_vm.$t("home")) + " "),
                                _c("span", { staticClass: "sr-only" }, [
                                  _vm._v("(current)")
                                ])
                              ]
                            ),
                            _vm._v(" "),
                            this.$router.currentRoute.fullPath == "/"
                              ? _c("img", {
                                  attrs: { src: "/front/img/arrow.png" }
                                })
                              : _vm._e()
                          ],
                          1
                        )
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _vm._m(1),
                  _vm._v(" "),
                  _c(
                    "router-link",
                    {
                      staticClass: "navbar-brand",
                      staticStyle: { "margin-left": "1rem" },
                      attrs: { to: "/" }
                    },
                    [
                      _c("img", {
                        staticStyle: { width: "129px", height: "81px" },
                        attrs: {
                          src: "/front/img/logo.svg",
                          alt: "tadweend logo",
                          width: "100%"
                        }
                      })
                    ]
                  )
                ],
                1
              )
            ]
          ),
      _vm._v(" "),
      _c("router-view"),
      _vm._v(" "),
      _c("footer", [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12 col-lg-6" }, [
              _c(
                "div",
                {
                  staticClass: "text",
                  staticStyle: { position: "static", "margin-bottom": "20px" }
                },
                [
                  _c("img", {
                    staticClass: "mx-auto d-block",
                    staticStyle: {
                      "max-width": "100%",
                      width: "147px",
                      height: "86px"
                    },
                    attrs: { src: "front/img/white_logo.svg" }
                  }),
                  _vm._v(" "),
                  _c(
                    "p",
                    {
                      class: _vm.lang == "ar" ? "text-right" : "",
                      staticStyle: {
                        "font-family": "AlmaraiLight",
                        "font-style": "normal",
                        "font-weight": "normal",
                        "font-size": "16px",
                        "margin-bottom": "15px",
                        height: "auto"
                      }
                    },
                    [
                      _vm._v(
                        _vm._s(
                          _vm.configs[
                            "about-description-" + _vm.lang
                          ].substring(0, 400) + ".."
                        )
                      )
                    ]
                  )
                ]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-12 col-lg-6" }, [
              _c(
                "div",
                { staticClass: "mail", staticStyle: { position: "static" } },
                [
                  _c(
                    "h3",
                    {
                      class: _vm.lang == "ar" ? "float-right" : "",
                      staticStyle: {
                        "font-family": "AlmaraiLight",
                        "font-style": "normal",
                        "font-weight": "900",
                        "font-size": "20px",
                        "line-height": "27px"
                      }
                    },
                    [_vm._v(_vm._s(_vm.$t("email_newsletter")))]
                  ),
                  _vm._v(" "),
                  _c(
                    "p",
                    {
                      class: _vm.lang == "ar" ? "float-right" : "",
                      staticStyle: {
                        "font-family": "AlmaraiLight",
                        "font-style": "normal",
                        "font-weight": "normal",
                        "font-size": "11px",
                        "line-height": "16px",
                        "word-spacing": "1px"
                      }
                    },
                    [_vm._v(_vm._s(_vm.$t("newsletter_desc")))]
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "input-group" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.email_newsletter,
                          expression: "email_newsletter"
                        }
                      ],
                      staticClass: "form-control",
                      style: _vm.lang == "ar" ? "direction: rtl" : "",
                      attrs: {
                        type: "text",
                        placeholder: _vm.$t("email_newsletter"),
                        "aria-label": "Username",
                        "aria-describedby": "basic-addon1"
                      },
                      domProps: { value: _vm.email_newsletter },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.email_newsletter = $event.target.value
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "a",
                      {
                        staticClass: "subscribe-link",
                        attrs: { href: "" },
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            return _vm.newsletter_subscribe($event)
                          }
                        }
                      },
                      [_vm._v(_vm._s(_vm.$t("SUBSCRIBE")))]
                    )
                  ]),
                  _vm._v(" "),
                  _c("br"),
                  _vm._v(" "),
                  _c(
                    "h3",
                    {
                      class: _vm.lang == "ar" ? "float-right" : "",
                      staticStyle: {
                        "font-family": "AlmaraiLight",
                        "font-style": "normal",
                        "font-weight": "900",
                        "font-size": "20px",
                        "line-height": "27px"
                      }
                    },
                    [
                      _c(
                        "router-link",
                        {
                          staticStyle: { color: "#fff0f0" },
                          attrs: { to: "/privacy-policy" }
                        },
                        [_vm._v(" " + _vm._s(_vm.$t("tadween_policy")) + " ")]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "p",
                    {
                      class: _vm.lang == "ar" ? "float-right" : "",
                      staticStyle: {
                        "font-family": "AlmaraiLight",
                        "font-style": "normal",
                        "font-weight": "normal",
                        "font-size": "11px",
                        "line-height": "16px",
                        "word-spacing": "1px"
                      }
                    },
                    [_vm._v(_vm._s(_vm.$t("cookie_message")))]
                  )
                ]
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-12 text-center text-lg-left mt-5" }, [
              _c("div", { staticClass: "social" }, [
                _c("ul", [
                  _c("li", [
                    _c("a", { attrs: { href: _vm.configs["youtube-link"] } }, [
                      _c("img", {
                        attrs: {
                          src: "/front/img/social/youtube.svg",
                          alt: "youtube"
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c("a", { attrs: { href: _vm.configs["twitter-link"] } }, [
                      _c("img", {
                        attrs: {
                          src: "/front/img/social/twitter.svg",
                          alt: "twitter"
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c("a", { attrs: { href: _vm.configs["linkedin-link"] } }, [
                      _c("img", {
                        attrs: {
                          src: "/front/img/social/linkdin.svg",
                          alt: "linkedin"
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _c(
                      "a",
                      { attrs: { href: _vm.configs["instagram-link"] } },
                      [
                        _c("img", {
                          attrs: {
                            src: "/front/img/social/insta.svg",
                            alt: "instagram"
                          }
                        })
                      ]
                    )
                  ])
                ])
              ])
            ])
          ])
        ])
      ])
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "navbar-toggler",
        attrs: {
          type: "button",
          "data-toggle": "collapse",
          "data-target": "#navbarNav",
          "aria-controls": "navbarNav",
          "aria-expanded": "false",
          "aria-label": "Toggle navigation"
        }
      },
      [
        _c("span", { staticClass: "navbar-toggler-icon2" }, [
          _c("img", { attrs: { src: "/front/img/menu-icon.png" } })
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "navbar-toggler",
        attrs: {
          type: "button",
          "data-toggle": "collapse",
          "data-target": "#navbarNav",
          "aria-controls": "navbarNav",
          "aria-expanded": "false",
          "aria-label": "Toggle navigation"
        }
      },
      [
        _c("span", { staticClass: "navbar-toggler-icon2" }, [
          _c("img", { attrs: { src: "/front/img/menu-icon.png" } })
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/layouts/main/Front.vue":
/*!*************************************************!*\
  !*** ./resources/js/src/layouts/main/Front.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Front_vue_vue_type_template_id_55850182___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Front.vue?vue&type=template&id=55850182& */ "./resources/js/src/layouts/main/Front.vue?vue&type=template&id=55850182&");
/* harmony import */ var _Front_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Front.vue?vue&type=script&lang=js& */ "./resources/js/src/layouts/main/Front.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Front_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Front_vue_vue_type_template_id_55850182___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Front_vue_vue_type_template_id_55850182___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/layouts/main/Front.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/layouts/main/Front.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/src/layouts/main/Front.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Front_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Front.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/main/Front.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Front_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/layouts/main/Front.vue?vue&type=template&id=55850182&":
/*!********************************************************************************!*\
  !*** ./resources/js/src/layouts/main/Front.vue?vue&type=template&id=55850182& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Front_vue_vue_type_template_id_55850182___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Front.vue?vue&type=template&id=55850182& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/layouts/main/Front.vue?vue&type=template&id=55850182&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Front_vue_vue_type_template_id_55850182___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Front_vue_vue_type_template_id_55850182___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);