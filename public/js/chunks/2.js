(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[2],{

/***/ "./resources/js/src/store/user-management/moduleUserManagement.js":
/*!************************************************************************!*\
  !*** ./resources/js/src/store/user-management/moduleUserManagement.js ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _moduleUserManagementState_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./moduleUserManagementState.js */ "./resources/js/src/store/user-management/moduleUserManagementState.js");
/* harmony import */ var _moduleUserManagementMutations_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./moduleUserManagementMutations.js */ "./resources/js/src/store/user-management/moduleUserManagementMutations.js");
/* harmony import */ var _moduleUserManagementActions_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./moduleUserManagementActions.js */ "./resources/js/src/store/user-management/moduleUserManagementActions.js");
/* harmony import */ var _moduleUserManagementGetters_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./moduleUserManagementGetters.js */ "./resources/js/src/store/user-management/moduleUserManagementGetters.js");
/*=========================================================================================
  File Name: moduleUserManagement.js
  Description: Calendar Module
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/




/* harmony default export */ __webpack_exports__["default"] = ({
  isRegistered: false,
  namespaced: true,
  state: _moduleUserManagementState_js__WEBPACK_IMPORTED_MODULE_0__["default"],
  mutations: _moduleUserManagementMutations_js__WEBPACK_IMPORTED_MODULE_1__["default"],
  actions: _moduleUserManagementActions_js__WEBPACK_IMPORTED_MODULE_2__["default"],
  getters: _moduleUserManagementGetters_js__WEBPACK_IMPORTED_MODULE_3__["default"]
});

/***/ }),

/***/ "./resources/js/src/store/user-management/moduleUserManagementActions.js":
/*!*******************************************************************************!*\
  !*** ./resources/js/src/store/user-management/moduleUserManagementActions.js ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/axios.js */ "./resources/js/src/axios.js");
/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

/* harmony default export */ __webpack_exports__["default"] = ({
  // addItem({ commit }, item) {
  //   return new Promise((resolve, reject) => {
  //     axios.post("/api/data-list/products/", {item: item})
  //       .then((response) => {
  //         commit('ADD_ITEM', Object.assign(item, {id: response.data.id}))
  //         resolve(response)
  //       })
  //       .catch((error) => { reject(error) })
  //   })
  // },
  userTypes: function userTypes(_ref) {
    var commit = _ref.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].get('/user-types').then(function (response) {
        commit('SET_USER_TYPES', response.data);
        resolve(response.data);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  setNewUser: function setNewUser(_ref2, user) {
    var commit = _ref2.commit;
    return new Promise(function (resolve, reject) {
      commit('NEW_USER', user);
      resolve(user);
    });
  },
  currentNewUser: function currentNewUser(_ref3) {
    var getters = _ref3.getters;
    return new Promise(function (resolve, reject) {
      var user = getters.currentNewUser;
      var keys = Object.keys(user);
      var new_user = {};
      keys.forEach(function (attr) {
        new_user[attr] = user[attr];
      });
      resolve(new_user);
    });
  },
  saveNewUser: function saveNewUser(_ref4) {
    var getters = _ref4.getters,
        commit = _ref4.commit;
    var user = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
    return new Promise(function (resolve, reject) {
      if (!user) {
        user = getters.currentNewUser;
      }

      var form_data = new FormData();
      commit("RESET_CURRENT_USER");

      for (var key in user) {
        form_data.append(key, user[key]);
      }

      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post('/users', form_data).then(function (res) {
        if (res.data && res.data.message) {
          reject(res.data.message);
        }

        resolve(user);
      });
    });
  },
  saveNewAdmin: function saveNewAdmin(_ref5, admin) {
    var commit = _ref5.commit;
    return new Promise(function (resolve, reject) {
      var form_data = new FormData();

      for (var key in admin) {
        form_data.append(key, admin[key]);
      }

      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post('/dashboard/admins', form_data).then(function (res) {
        if (res.data && res.data.message) {
          reject(res.data.message);
        }

        resolve(admin);
      });
    });
  },
  fetchInvitedClients: function fetchInvitedClients(_ref6, type) {
    var commit = _ref6.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].get('/dashboard/invited-clients').then(function (response) {
        commit('SET_INVITED_CLIENTS', response.data);
        resolve(response.data);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  fetchInvitedStores: function fetchInvitedStores(_ref7, type) {
    var commit = _ref7.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].get('/dashboard/invited-stores').then(function (response) {
        commit('SET_INVITED_STORES', response.data);
        resolve(response.data);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  fetchInvitedRepresentatives: function fetchInvitedRepresentatives(_ref8, type) {
    var commit = _ref8.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].get('/dashboard/invited-representatives').then(function (response) {
        commit('SET_INVITED_EMPLOYEES', response.data);
        resolve(response.data);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  saveNewInvitedClient: function saveNewInvitedClient(_ref9, invited_client) {
    var commit = _ref9.commit;
    return new Promise(function (resolve, reject) {
      var form_data = new FormData();

      for (var key in invited_client) {
        form_data.append(key, invited_client[key]);
      }

      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post('/dashboard/invited-clients', form_data).then(function (res) {
        if (res.data && res.data.message) {
          reject(res.data.message);
        }

        resolve(invited_client);
      });
    });
  },
  saveNewInvitedStore: function saveNewInvitedStore(_ref10, invited_store) {
    var commit = _ref10.commit;
    return new Promise(function (resolve, reject) {
      var form_data = new FormData();

      for (var key in invited_store) {
        form_data.append(key, invited_store[key]);
      }

      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post('/dashboard/invited-stores', form_data).then(function (res) {
        if (res.data && res.data.message) {
          reject(res.data.message);
        }

        resolve(invited_store);
      });
    });
  },
  saveNewInvitedRepresentative: function saveNewInvitedRepresentative(_ref11, invited_representative) {
    var commit = _ref11.commit;
    return new Promise(function (resolve, reject) {
      var form_data = new FormData();

      for (var key in invited_representative) {
        form_data.append(key, invited_representative[key]);
      }

      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post('/dashboard/invited-representatives', form_data).then(function (res) {
        if (res.data && res.data.message) {
          reject(res.data.message);
        }

        resolve(invited_representative);
      });
    });
  },
  fetchAdmins: function fetchAdmins(_ref12) {
    var commit = _ref12.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].get('/dashboard/admins').then(function (response) {
        commit('SET_ADMINS', response.data);
        resolve(response.data);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  fetchAdmin: function fetchAdmin(context, adminId) {
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].get("/dashboard/admins/".concat(adminId)).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  fetchInvitedClient: function fetchInvitedClient(context, invitedClientId) {
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].get("/dashboard/invited-clients/".concat(invitedClientId)).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  fetchInvitedStore: function fetchInvitedStore(context, invitedStoreId) {
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].get("/dashboard/invited-stores/".concat(invitedStoreId)).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  fetchInvitedRepresentative: function fetchInvitedRepresentative(context, invitedRepresentativeId) {
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].get("/dashboard/invited-representatives/".concat(invitedRepresentativeId)).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  fetchUserPermissions: function fetchUserPermissions(context, userId) {
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].get("/dashboard/users/permissions/".concat(userId)).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  updateUserPermissions: function updateUserPermissions(_ref13, data) {
    var commit = _ref13.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post("/dashboard/users/update-permissions/".concat(data.userId), data.permissions).then(function (res) {
        if (res.data && res.data.message) {
          reject(res.data.message);
        }

        resolve(data);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  fetchAdminPermissions: function fetchAdminPermissions(context, adminId) {
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].get("/dashboard/admins/permissions/".concat(adminId)).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  updateAdminPermissions: function updateAdminPermissions(_ref14, data) {
    var commit = _ref14.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post("/dashboard/admins/update-permissions/".concat(data.adminId), data.permissions).then(function (res) {
        if (res.data && res.data.message) {
          reject(res.data.message);
        }

        resolve(data);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  removeInvitedClient: function removeInvitedClient(_ref15, invitedClientId) {
    var commit = _ref15.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].delete("/dashboard/invited-clients/".concat(invitedClientId)).then(function (response) {
        commit('REMOVE_INVITED_CLIENT', invitedClientId);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  removeInvitedStore: function removeInvitedStore(_ref16, invitedStoreId) {
    var commit = _ref16.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].delete("/dashboard/invited-stores/".concat(invitedStoreId)).then(function (response) {
        commit('REMOVE_INVITED_STORE', invitedStoreId);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  removeInvitedRepresentative: function removeInvitedRepresentative(_ref17, invitedRepresentativeId) {
    var commit = _ref17.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].delete("/dashboard/invited-representatives/".concat(invitedRepresentativeId)).then(function (response) {
        commit('REMOVE_INVITED_EMPLOYEE', invitedRepresentativeId);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  removeAdmin: function removeAdmin(_ref18, adminId) {
    var commit = _ref18.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].delete("/dashboard/admins/".concat(adminId)).then(function (response) {
        commit('REMOVE_ADMIN', adminId);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  fetchUsers: function fetchUsers(_ref19, type) {
    var commit = _ref19.commit;
    return new Promise(function (resolve, reject) {
      // var formData = new FormData
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].get('/users?type=' + type).then(function (response) {
        commit('SET_USERS', response.data);
        resolve(response.data);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  fetchUser: function fetchUser(context, userId) {
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].get("/users/".concat(userId)).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  updateAdminPassword: function updateAdminPassword(_ref20, data) {
    var commit = _ref20.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post("/dashboard/admins/update-password", data).then(function (res) {
        if (res.data && res.data.message) {
          reject(res.data.message);
        }

        resolve(data);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  updateAdmin: function updateAdmin(_ref21, admin) {
    var commit = _ref21.commit;
    return new Promise(function (resolve, reject) {
      var form_data = new FormData();

      for (var key in admin) {
        form_data.append(key, admin[key]);
      }

      form_data.append('_method', 'PUT');
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post("/dashboard/admins/".concat(admin.id), form_data).then(function (res) {
        if (res.data && res.data.message) {
          reject(res.data.message);
        }

        resolve(res.data);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  updateInvitedClient: function updateInvitedClient(_ref22, invitedClient) {
    var commit = _ref22.commit;
    return new Promise(function (resolve, reject) {
      var form_data = new FormData();

      for (var key in invitedClient) {
        form_data.append(key, invitedClient[key]);
      }

      form_data.append('_method', 'PUT');
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post("/dashboard/invited-clients/".concat(invitedClient.id), form_data).then(function (res) {
        if (res.data && res.data.message) {
          reject(res.data.message);
        }

        resolve(res.data);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  updateInvitedStore: function updateInvitedStore(_ref23, invitedStore) {
    var commit = _ref23.commit;
    return new Promise(function (resolve, reject) {
      var form_data = new FormData();

      for (var key in invitedStore) {
        form_data.append(key, invitedStore[key]);
      }

      form_data.append('_method', 'PUT');
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post("/dashboard/invited-stores/".concat(invitedStore.id), form_data).then(function (res) {
        if (res.data && res.data.message) {
          reject(res.data.message);
        }

        resolve(res.data);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  updateInvitedRepresentative: function updateInvitedRepresentative(_ref24, invitedRepresentative) {
    var commit = _ref24.commit;
    return new Promise(function (resolve, reject) {
      var form_data = new FormData();

      for (var key in invitedRepresentative) {
        form_data.append(key, invitedRepresentative[key]);
      }

      form_data.append('_method', 'PUT');
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post("/dashboard/invited-representatives/".concat(invitedRepresentative.id), form_data).then(function (res) {
        if (res.data && res.data.message) {
          reject(res.data.message);
        }

        resolve(res.data);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  updateUserStep: function updateUserStep(context, _ref25) {
    var user = _ref25.user,
        step = _ref25.step;
    return new Promise(function (resolve, reject) {
      var form_data = new FormData();

      for (var key in user) {
        form_data.append(key, user[key]);
      }

      form_data.append('_method', 'PUT');
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post("/users/".concat(user.id, "/").concat(step), form_data).then(function (res) {
        if (res.data && res.data.message) {
          reject(res.data.message);
        }

        resolve(user);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  removeRecord: function removeRecord(_ref26, userId) {
    var commit = _ref26.commit;
    return new Promise(function (resolve, reject) {
      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].delete("/users/".concat(userId)).then(function (response) {
        commit('REMOVE_RECORD', userId);
        resolve(response);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  registerStep1: function registerStep1(_ref27, user) {
    var commit = _ref27.commit;
    return new Promise(function (resolve, reject) {
      var form_data = new FormData();

      for (var key in user) {
        form_data.append(key, user[key]);
      }

      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post('/register-step1', form_data).then(function (res) {
        if (res.data && res.data.message) {
          reject(res.data.message);
        }

        resolve(user);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  registerStep2: function registerStep2(_ref28, user) {
    var commit = _ref28.commit;
    return new Promise(function (resolve, reject) {
      var form_data = new FormData();

      for (var key in user) {
        form_data.append(key, user[key]);
      }

      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post('/register-step2', form_data).then(function (res) {
        if (res.data && res.data.message) {
          reject(res.data.message);
        }

        resolve(user);
      }).catch(function (error) {
        reject(error);
      });
    });
  },
  registerStep3: function registerStep3(_ref29, user) {
    var commit = _ref29.commit;
    return new Promise(function (resolve, reject) {
      var form_data = new FormData();

      for (var key in user) {
        form_data.append(key, user[key]);
      }

      _axios_js__WEBPACK_IMPORTED_MODULE_0__["default"].post('/register-step3', form_data).then(function (res) {
        if (res.data && res.data.message) {
          reject(res.data.message);
        }

        resolve(user);
      }).catch(function (error) {
        reject(error);
      });
    });
  }
});

/***/ }),

/***/ "./resources/js/src/store/user-management/moduleUserManagementGetters.js":
/*!*******************************************************************************!*\
  !*** ./resources/js/src/store/user-management/moduleUserManagementGetters.js ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: moduleCalendarGetters.js
  Description: Calendar Module Getters
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({
  currentNewUser: function currentNewUser(state) {
    return state.new_user;
  }
});

/***/ }),

/***/ "./resources/js/src/store/user-management/moduleUserManagementMutations.js":
/*!*********************************************************************************!*\
  !*** ./resources/js/src/store/user-management/moduleUserManagementMutations.js ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: moduleCalendarMutations.js
  Description: Calendar Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({
  SET_USERS: function SET_USERS(state, users) {
    state.users = users;
  },
  SET_ADMINS: function SET_ADMINS(state, admins) {
    state.admins = admins;
  },
  SET_INVITED_CLIENTS: function SET_INVITED_CLIENTS(state, invited_clients) {
    state.invited_clients = invited_clients;
  },
  RESET_CURRENT_USER: function RESET_CURRENT_USER(state) {
    state.new_user = {};
  },
  SET_INVITED_STORES: function SET_INVITED_STORES(state, invited_stores) {
    state.invited_stores = invited_stores;
  },
  SET_INVITED_EMPLOYEES: function SET_INVITED_EMPLOYEES(state, invited_representatives) {
    state.invited_representatives = invited_representatives;
  },
  SET_USER_TYPES: function SET_USER_TYPES(state, user_types) {
    state.user_types = user_types;
  },
  REMOVE_RECORD: function REMOVE_RECORD(state, itemId) {
    var userIndex = state.users.findIndex(function (u) {
      return u.id === itemId;
    });
    state.users.splice(userIndex, 1);
  },
  REMOVE_ADMIN: function REMOVE_ADMIN(state, itemId) {
    var adminIndex = state.admins.findIndex(function (u) {
      return u.id === itemId;
    });
    state.admins.splice(adminIndex, 1);
  },
  REMOVE_INVITED_CLIENT: function REMOVE_INVITED_CLIENT(state, itemId) {
    var invitedClientIndex = state.invited_clients.findIndex(function (u) {
      return u.id === itemId;
    });
    state.invited_clients.splice(invitedClientIndex, 1);
  },
  REMOVE_INVITED_STORE: function REMOVE_INVITED_STORE(state, itemId) {
    var invitedStoreIndex = state.invited_stores.findIndex(function (u) {
      return u.id === itemId;
    });
    state.invited_stores.splice(invitedStoreIndex, 1);
  },
  REMOVE_INVITED_EMPLOYEE: function REMOVE_INVITED_EMPLOYEE(state, itemId) {
    var invitedRepresentativeIndex = state.invited_representatives.findIndex(function (u) {
      return u.id === itemId;
    });
    state.invited_representatives.splice(invitedRepresentativeIndex, 1);
  },
  NEW_USER: function NEW_USER(state, user) {
    Object.assign(state.new_user, user);
  }
});

/***/ }),

/***/ "./resources/js/src/store/user-management/moduleUserManagementState.js":
/*!*****************************************************************************!*\
  !*** ./resources/js/src/store/user-management/moduleUserManagementState.js ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*=========================================================================================
  File Name: moduleCalendarState.js
  Description: Calendar Module State
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
/* harmony default export */ __webpack_exports__["default"] = ({
  users: [],
  user_types: [],
  invited_clients: [],
  invited_stores: [],
  invited_representatives: [],
  admins: [],
  news: [],
  new_user: {
    'email': '',
    'name': '',
    'password': '',
    'type': '',
    'username': '',
    'commercial_registered': '',
    'issue_place': '',
    'issue_date': '',
    'expiry_date': '',
    'building_number': '',
    'street_name': '',
    'city': '',
    'neighborhood': '',
    'postal_code': '',
    'website': '',
    'phone': '',
    'telephone': '',
    'fax': '',
    'longitude': '',
    'latitude': '',
    'photo': '/assets/images/users/default.png'
  }
});

/***/ })

}]);