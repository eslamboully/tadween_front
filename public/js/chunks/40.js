(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[40],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/HowView.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/front/HowView.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../axios */ "./resources/js/src/axios.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      configs: [],
      news: [],
      packages: [],
      services: [],
      lang: this.$i18n.locale
    };
  },
  mounted: function mounted() {
    this.fetch_configs_data();
  },
  created: function created() {
    var _this = this;

    window.VueInstant.$on('change_locale', function (lang) {
      _this.lang = lang;
    });
  },
  methods: {
    fetch_configs_data: function fetch_configs_data() {
      var _this2 = this;

      _axios__WEBPACK_IMPORTED_MODULE_0__["default"].get("/front/configs").then(function (response) {
        _this2.configs = response.data.configs;
        _this2.news = response.data.news;
        _this2.packages = response.data.packages;
        _this2.services = response.data.services;
        var vm = _this2;
        window.Fire.nextTick(function () {
          $('.partner .owl-carousel').owlCarousel({
            loop: true,
            margin: 30,
            responsive: {
              0: {
                items: 3
              },
              600: {
                items: 3
              },
              1000: {
                items: 5
              }
            }
          });
        }).catch(function (error) {
          reject(error);
        });
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/HowView.vue?vue&type=template&id=39487a16&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/front/HowView.vue?vue&type=template&id=39487a16& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("section", { staticClass: "work" }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12 text-center" }, [
            _c("div", { staticClass: "head" }, [
              _c(
                "h1",
                {
                  staticStyle: {
                    "font-family": "AlmaraiRegular",
                    "font-style": "normal",
                    "font-weight": "800",
                    "font-size": "42px",
                    "line-height": "118.1%"
                  }
                },
                [_vm._v(_vm._s(_vm.configs["how-title-" + _vm.lang]))]
              ),
              _vm._v(" "),
              _c("p", {
                staticStyle: {
                  "font-family": "AlmaraiRegular",
                  "font-style": "normal",
                  "font-weight": "normal",
                  "font-size": "16px",
                  "line-height": "24px",
                  "letter-spacing": "-0.41px",
                  "text-indent": "1px",
                  "text-transform": "capitalize"
                },
                domProps: {
                  innerHTML: _vm._s(_vm.configs["how-description-" + _vm.lang])
                }
              })
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-12" }, [
            _c(
              "ul",
              {
                staticClass: "nav nav-tabs",
                attrs: { id: "myTab", role: "tablist" }
              },
              [
                _c(
                  "li",
                  {
                    staticClass: "nav-item",
                    staticStyle: {
                      "font-family": "AlmaraiRegular",
                      "font-style": "normal",
                      "font-weight": "normal",
                      "font-size": "16px",
                      "line-height": "24px",
                      "letter-spacing": "-0.41px",
                      "text-indent": "1px",
                      "text-transform": "capitalize"
                    }
                  },
                  [
                    _c(
                      "a",
                      {
                        staticClass: "nav-link active",
                        attrs: {
                          id: "wholesaler-tab",
                          "data-toggle": "tab",
                          href: "#wholesaler",
                          role: "tab",
                          "aria-controls": "home",
                          "aria-selected": "true"
                        }
                      },
                      [_vm._v(_vm._s(_vm.$t("wholesaler")))]
                    )
                  ]
                ),
                _vm._v(" "),
                _c(
                  "li",
                  {
                    staticClass: "nav-item",
                    staticStyle: {
                      "font-family": "AlmaraiRegular",
                      "font-style": "normal",
                      "font-weight": "normal",
                      "font-size": "16px",
                      "line-height": "24px",
                      "letter-spacing": "-0.41px",
                      "text-indent": "1px",
                      "text-transform": "capitalize"
                    }
                  },
                  [
                    _c(
                      "a",
                      {
                        staticClass: "nav-link",
                        attrs: {
                          id: "salesman-tab",
                          "data-toggle": "tab",
                          href: "#salesman",
                          role: "tab",
                          "aria-controls": "profile",
                          "aria-selected": "false"
                        }
                      },
                      [_vm._v(_vm._s(_vm.$t("salesman")))]
                    )
                  ]
                ),
                _vm._v(" "),
                _c(
                  "li",
                  {
                    staticClass: "nav-item",
                    staticStyle: {
                      "font-family": "AlmaraiRegular",
                      "font-style": "normal",
                      "font-weight": "normal",
                      "font-size": "16px",
                      "line-height": "24px",
                      "letter-spacing": "-0.41px",
                      "text-indent": "1px",
                      "text-transform": "capitalize"
                    }
                  },
                  [
                    _c(
                      "a",
                      {
                        staticClass: "nav-link",
                        attrs: {
                          id: "retailer-tab",
                          "data-toggle": "tab",
                          href: "#retailer",
                          role: "tab",
                          "aria-controls": "contact",
                          "aria-selected": "false"
                        }
                      },
                      [_vm._v(_vm._s(_vm.$t("retailer")))]
                    )
                  ]
                )
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "tab-content", attrs: { id: "myTabContent" } },
              [
                _c(
                  "div",
                  {
                    staticClass: "tab-pane fade show active",
                    attrs: {
                      id: "wholesaler",
                      role: "tabpanel",
                      "aria-labelledby": "wholesaler-tab"
                    }
                  },
                  [
                    _c(
                      "div",
                      {
                        staticClass: "desc",
                        staticStyle: {
                          "font-family": "AlmaraiRegular",
                          "font-style": "normal",
                          "font-weight": "normal",
                          "font-size": "14px",
                          "line-height": "24px",
                          "letter-spacing": "-0.41px",
                          "text-indent": "1px",
                          "text-transform": "capitalize"
                        }
                      },
                      [
                        _c("p", {
                          staticStyle: { "text-align": "center" },
                          domProps: {
                            innerHTML: _vm._s(
                              _vm.configs["wholesaler-description-" + _vm.lang]
                            )
                          }
                        })
                      ]
                    )
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "tab-pane fade",
                    attrs: {
                      id: "salesman",
                      role: "tabpanel",
                      "aria-labelledby": "salesman-tab"
                    }
                  },
                  [
                    _c(
                      "div",
                      {
                        staticClass: "desc",
                        staticStyle: {
                          "font-family": "AlmaraiRegular",
                          "font-style": "normal",
                          "font-weight": "normal",
                          "font-size": "14px",
                          "line-height": "24px",
                          "letter-spacing": "-0.41px",
                          "text-indent": "1px",
                          "text-transform": "capitalize"
                        }
                      },
                      [
                        _c("p", {
                          staticStyle: { "text-align": "center" },
                          domProps: {
                            innerHTML: _vm._s(
                              _vm.configs["salesman-description-" + _vm.lang]
                            )
                          }
                        })
                      ]
                    )
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "tab-pane fade",
                    attrs: {
                      id: "retailer",
                      role: "tabpanel",
                      "aria-labelledby": "retailer-tab"
                    }
                  },
                  [
                    _c(
                      "div",
                      {
                        staticClass: "desc",
                        staticStyle: {
                          "font-family": "AlmaraiRegular",
                          "font-style": "normal",
                          "font-weight": "normal",
                          "font-size": "14px",
                          "line-height": "24px",
                          "letter-spacing": "-0.41px",
                          "text-indent": "1px",
                          "text-transform": "capitalize"
                        }
                      },
                      [
                        _c("p", {
                          staticStyle: { "text-align": "center" },
                          domProps: {
                            innerHTML: _vm._s(
                              _vm.configs["retailer-description-" + _vm.lang]
                            )
                          }
                        })
                      ]
                    )
                  ]
                )
              ]
            )
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("section", { staticClass: "download" }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12 text-center" }, [
            _c("h2", [_vm._v(_vm._s(_vm.$t("download_app")))]),
            _vm._v(" "),
            _c("div", { staticClass: "stores" }, [
              _c("a", { attrs: { href: _vm.configs["app-store-link"] } }, [
                _c("img", {
                  attrs: { src: "front/img/app-store.png", alt: "app-store" }
                })
              ]),
              _vm._v(" "),
              _c(
                "a",
                {
                  attrs: {
                    target: "_blank",
                    href: _vm.configs["google-play-link"]
                  }
                },
                [
                  _c("img", {
                    attrs: {
                      src: "front/img/google-play.png",
                      alt: "google-play"
                    }
                  })
                ]
              )
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/front/HowView.vue":
/*!**************************************************!*\
  !*** ./resources/js/src/views/front/HowView.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _HowView_vue_vue_type_template_id_39487a16___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./HowView.vue?vue&type=template&id=39487a16& */ "./resources/js/src/views/front/HowView.vue?vue&type=template&id=39487a16&");
/* harmony import */ var _HowView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./HowView.vue?vue&type=script&lang=js& */ "./resources/js/src/views/front/HowView.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _HowView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _HowView_vue_vue_type_template_id_39487a16___WEBPACK_IMPORTED_MODULE_0__["render"],
  _HowView_vue_vue_type_template_id_39487a16___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/front/HowView.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/front/HowView.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/src/views/front/HowView.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HowView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./HowView.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/HowView.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HowView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/front/HowView.vue?vue&type=template&id=39487a16&":
/*!*********************************************************************************!*\
  !*** ./resources/js/src/views/front/HowView.vue?vue&type=template&id=39487a16& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HowView_vue_vue_type_template_id_39487a16___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./HowView.vue?vue&type=template&id=39487a16& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/HowView.vue?vue&type=template&id=39487a16&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HowView_vue_vue_type_template_id_39487a16___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HowView_vue_vue_type_template_id_39487a16___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);