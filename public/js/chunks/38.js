(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[38],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/BlogsView.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/front/BlogsView.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../axios */ "./resources/js/src/axios.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      configs: [],
      news: [],
      lang: this.$i18n.locale
    };
  },
  mounted: function mounted() {
    this.fetch_configs_data();
  },
  created: function created() {
    var _this = this;

    window.VueInstant.$on('change_locale', function (lang) {
      _this.lang = lang;
    });
  },
  methods: {
    fetch_configs_data: function fetch_configs_data() {
      var _this2 = this;

      _axios__WEBPACK_IMPORTED_MODULE_0__["default"].get("/front/configs").then(function (response) {
        _this2.configs = response.data.configs;
        _this2.news = response.data.news;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/BlogsView.vue?vue&type=template&id=e6925e94&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/front/BlogsView.vue?vue&type=template&id=e6925e94& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("section", { staticClass: "news", attrs: { id: "blogs-section" } }, [
      _c("div", { staticClass: "container" }, [
        _c(
          "div",
          { staticClass: "row" },
          [
            _c("div", { staticClass: "col-12 text-center" }, [
              _c("div", { staticClass: "head" }, [
                _c(
                  "h2",
                  {
                    staticStyle: {
                      "font-family": "AlmaraiBold",
                      "font-style": "normal",
                      "font-weight": "800",
                      "font-size": "35px",
                      "line-height": "118.1%"
                    }
                  },
                  [_vm._v(_vm._s(_vm.$t("last_news")))]
                ),
                _vm._v(" "),
                _c(
                  "p",
                  {
                    staticStyle: {
                      "font-family": "AlmaraiRegular",
                      "font-style": "normal",
                      "font-weight": "normal",
                      "font-size": "16px",
                      "line-height": "118.1%",
                      "text-align": "center"
                    }
                  },
                  [_vm._v(_vm._s(_vm.configs["news-description-" + _vm.lang]))]
                )
              ])
            ]),
            _vm._v(" "),
            _vm._l(_vm.news, function(newsSingle) {
              return _c(
                "div",
                {
                  staticClass:
                    "col-12 col-lg-4 col-md-6 text-md-left text-center mt-5"
                },
                [
                  _c("div", { staticClass: "body" }, [
                    _c("img", {
                      attrs: { src: "/assets/images/news/" + newsSingle.photo }
                    }),
                    _vm._v(" "),
                    _c(
                      "h3",
                      {
                        staticStyle: {
                          "font-family": "AlmaraiRegular",
                          "font-style": "normal",
                          "font-weight": "900",
                          "font-size": "16px",
                          "line-height": "28px"
                        }
                      },
                      [
                        _c(
                          "router-link",
                          { attrs: { to: "/blogs/" + newsSingle.id } },
                          [_vm._v(_vm._s(newsSingle["title_" + _vm.lang]))]
                        )
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "p",
                      {
                        staticStyle: {
                          margin: "auto",
                          "font-family": "AlmaraiRegular",
                          "font-style": "normal",
                          "font-weight": "normal",
                          "font-size": "14px",
                          "line-height": "24px"
                        }
                      },
                      [_vm._v(_vm._s(newsSingle["description_" + _vm.lang]))]
                    )
                  ])
                ]
              )
            })
          ],
          2
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/front/BlogsView.vue":
/*!****************************************************!*\
  !*** ./resources/js/src/views/front/BlogsView.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _BlogsView_vue_vue_type_template_id_e6925e94___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BlogsView.vue?vue&type=template&id=e6925e94& */ "./resources/js/src/views/front/BlogsView.vue?vue&type=template&id=e6925e94&");
/* harmony import */ var _BlogsView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BlogsView.vue?vue&type=script&lang=js& */ "./resources/js/src/views/front/BlogsView.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _BlogsView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _BlogsView_vue_vue_type_template_id_e6925e94___WEBPACK_IMPORTED_MODULE_0__["render"],
  _BlogsView_vue_vue_type_template_id_e6925e94___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/front/BlogsView.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/front/BlogsView.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/src/views/front/BlogsView.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogsView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./BlogsView.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/BlogsView.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogsView_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/front/BlogsView.vue?vue&type=template&id=e6925e94&":
/*!***********************************************************************************!*\
  !*** ./resources/js/src/views/front/BlogsView.vue?vue&type=template&id=e6925e94& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogsView_vue_vue_type_template_id_e6925e94___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./BlogsView.vue?vue&type=template&id=e6925e94& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/front/BlogsView.vue?vue&type=template&id=e6925e94&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogsView_vue_vue_type_template_id_e6925e94___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BlogsView_vue_vue_type_template_id_e6925e94___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);