(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[35],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/admin/admin-permissions/AdminPermission.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/dashboard/admin/admin-permissions/AdminPermission.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _store_user_management_moduleUserManagement_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/store/user-management/moduleUserManagement.js */ "./resources/js/src/store/user-management/moduleUserManagement.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  created: function created() {
    var _this = this;

    // Register Module AdminManagement Module
    if (!_store_user_management_moduleUserManagement_js__WEBPACK_IMPORTED_MODULE_0__["default"].isRegistered) {
      this.$store.registerModule('userManagement', _store_user_management_moduleUserManagement_js__WEBPACK_IMPORTED_MODULE_0__["default"]);
      _store_user_management_moduleUserManagement_js__WEBPACK_IMPORTED_MODULE_0__["default"].isRegistered = true;
    }

    this.id = this.$route.params.adminId;
    this.$store.dispatch('userManagement/fetchAdminPermissions', this.id).then(function (res) {
      _this.permissions = res.data;
    }).catch(function (err) {
      if (err.response.status === 404) {
        return;
      }

      console.error(err);
    });
  },
  data: function data() {
    return {
      permissions: [],
      id: ''
    };
  },
  methods: {
    capitalize: function capitalize(str) {
      return str.slice(0, 1).toUpperCase() + str.slice(1, str.length);
    },
    showError: function showError(message) {
      this.$vs.notify({
        color: 'danger',
        text: message
      });
    },
    showCreatedSuccess: function showCreatedSuccess() {
      this.$vs.notify({
        color: 'success',
        title: 'Admin Updated',
        text: 'Admin was successfully updated'
      });
    },
    save_changes: function save_changes() {
      var _this2 = this;

      var data = {
        permissions: this.permissions,
        adminId: this.id
      };
      this.$store.dispatch('userManagement/updateAdminPermissions', data).then(function (res) {
        _this2.$router.push({
          name: 'dashboard-admin-list'
        });

        _this2.showCreatedSuccess();
      }).catch(function (err) {
        _this2.showError(err);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/admin/admin-permissions/AdminPermission.vue?vue&type=template&id=8a1bd900&":
/*!***************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/dashboard/admin/admin-permissions/AdminPermission.vue?vue&type=template&id=8a1bd900& ***!
  \***************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { id: "user-edit-tab-info" } },
    [
      _c(
        "vx-card",
        {
          staticClass: "mt-base",
          attrs: { "no-shadow": "", "card-border": "" }
        },
        [
          _c("div", { staticClass: "vx-row" }, [
            _c(
              "div",
              { staticClass: "vx-col w-full" },
              [
                _c(
                  "div",
                  { staticClass: "flex items-end px-3" },
                  [
                    _c("feather-icon", {
                      staticClass: "mr-2",
                      attrs: { svgClasses: "w-6 h-6", icon: "LockIcon" }
                    }),
                    _vm._v(" "),
                    _c(
                      "span",
                      { staticClass: "font-medium text-lg leading-none" },
                      [_vm._v("Permissions")]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c("vs-divider")
              ],
              1
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "block overflow-x-auto" }, [
            _c(
              "table",
              { staticClass: "w-full" },
              [
                _c(
                  "tr",
                  _vm._l(
                    ["Module", "Create", "Read", "Update", "Delete"],
                    function(heading) {
                      return _c(
                        "th",
                        {
                          key: heading,
                          staticClass:
                            "font-semibold text-base text-left px-3 py-2"
                        },
                        [_vm._v(_vm._s(heading))]
                      )
                    }
                  ),
                  0
                ),
                _vm._v(" "),
                _vm._l(_vm.permissions, function(val, name) {
                  return _c(
                    "tr",
                    { key: name },
                    [
                      _c("td", { staticClass: "px-3 py-2" }, [
                        _vm._v(_vm._s(name))
                      ]),
                      _vm._v(" "),
                      _vm._l(val, function(permission, name) {
                        return _c(
                          "td",
                          { key: name + permission, staticClass: "px-3 py-2" },
                          [
                            _c("vs-checkbox", {
                              model: {
                                value: val[name],
                                callback: function($$v) {
                                  _vm.$set(val, name, $$v)
                                },
                                expression: "val[name]"
                              }
                            })
                          ],
                          1
                        )
                      })
                    ],
                    2
                  )
                })
              ],
              2
            )
          ])
        ]
      ),
      _vm._v(" "),
      _c("div", { staticClass: "vx-row" }, [
        _c("div", { staticClass: "vx-col w-full" }, [
          _c(
            "div",
            { staticClass: "mt-8 flex flex-wrap items-center justify-end" },
            [
              _c(
                "vs-button",
                {
                  staticClass: "ml-auto mt-2",
                  on: { click: _vm.save_changes }
                },
                [_vm._v("Save Changes")]
              )
            ],
            1
          )
        ])
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/dashboard/admin/admin-permissions/AdminPermission.vue":
/*!**************************************************************************************!*\
  !*** ./resources/js/src/views/dashboard/admin/admin-permissions/AdminPermission.vue ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AdminPermission_vue_vue_type_template_id_8a1bd900___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AdminPermission.vue?vue&type=template&id=8a1bd900& */ "./resources/js/src/views/dashboard/admin/admin-permissions/AdminPermission.vue?vue&type=template&id=8a1bd900&");
/* harmony import */ var _AdminPermission_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AdminPermission.vue?vue&type=script&lang=js& */ "./resources/js/src/views/dashboard/admin/admin-permissions/AdminPermission.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _AdminPermission_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AdminPermission_vue_vue_type_template_id_8a1bd900___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AdminPermission_vue_vue_type_template_id_8a1bd900___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/dashboard/admin/admin-permissions/AdminPermission.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/dashboard/admin/admin-permissions/AdminPermission.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/src/views/dashboard/admin/admin-permissions/AdminPermission.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AdminPermission_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./AdminPermission.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/admin/admin-permissions/AdminPermission.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AdminPermission_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/dashboard/admin/admin-permissions/AdminPermission.vue?vue&type=template&id=8a1bd900&":
/*!*********************************************************************************************************************!*\
  !*** ./resources/js/src/views/dashboard/admin/admin-permissions/AdminPermission.vue?vue&type=template&id=8a1bd900& ***!
  \*********************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AdminPermission_vue_vue_type_template_id_8a1bd900___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./AdminPermission.vue?vue&type=template&id=8a1bd900& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/dashboard/admin/admin-permissions/AdminPermission.vue?vue&type=template&id=8a1bd900&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AdminPermission_vue_vue_type_template_id_8a1bd900___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AdminPermission_vue_vue_type_template_id_8a1bd900___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);