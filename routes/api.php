<?php

Route::group(['prefix' => 'front', 'namespace' => 'Admin'], function () {
    Route::get('configs','ConfigController@frontIndex');
});

Route::group(['prefix' => 'dashboard', 'namespace' => 'Admin'], function () {

	Route::group(['middleware' => ['api_middleware']], function () {
		Route::any('logout', 'Authentication@logout');
	});

	Route::post('admins/update-password', 'AdminController@update_password');

	Route::post('admins/update-permissions/{id}', 'AdminController@update_permissions');
	Route::get('admins/permissions/{id}', 'AdminController@get_permissions');
	Route::apiResource('admins', 'AdminController');

//	Route::any('logout', 'Authentication@logout');
	Route::post('login', 'Authentication@login');

//	Route::apiResource('languages', 'LanguageController');

//	Route::post('/translations' , 'TranslationController@save');
//	Route::get('/translations' , 'TranslationController@index');

	Route::apiResource('news','NewsController');

	Route::apiResource('packages','PackageController');

    Route::apiResource('services','ServiceController');

    Route::apiResource('partners','PartnerController');

    Route::apiResource('testimonials','TestimonialController');

    Route::get('configs/{id}','ConfigController@index');

    Route::put('configs','ConfigController@update');

    Route::get('news-single/{id}','NewsController@newsSingle');


});

//Route::get('/products/history/{id}', 'ProductController@history');

Route::group(['middleware' => ['api_middleware']], function () {

//	Route::get('/my-notifications', 'NotificationController@my_notifications');

	Route::any('logout', 'Authentication@logout');
	Route::put('/update-profile/{user}', 'UserController@update');

	Route::get('/current-user', function(){
		return auth('api')->user();

	});

});
//
Route::get('/statistics', 'HomeController@statistics');

Route::post('/newsletter', 'NewsletterController@subscribe');


Route::post('login', 'Authentication@login');
Route::any('logout', 'Authentication@logout');
