<?php

Route::get('/dashboard/{any}', 'ApplicationController')->where('any', '.*');
Route::get('/dashboard', 'ApplicationController')->where('any', '.*');

Route::get('/','FrontController@index');
Route::get('/about-us','FrontController@index');
Route::get('/how-work','FrontController@index');
Route::get('/packages','FrontController@index');
Route::get('/blogs','FrontController@index');
Route::get('/blogs/{id}','FrontController@index');
Route::get('/privacy-policy','FrontController@index');
