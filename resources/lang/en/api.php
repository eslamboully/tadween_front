<?php

return [
    'return_invoice' => 'Return Invoice',
    'date' => 'Date',
    'invoice_no' => 'Invoice no',
    'crn' => 'CRN',
    'tax' => 'TAX',
    'return_invoice_to' => 'Return Invoice to',
    'pay_to' => 'Pay To',
    'product' => 'Product',
    'qty' => 'QTY',
    'total' => 'Total',
    'sub_total' => 'Sub Total',
    'sar' => 'SAR',
    'price' => 'Price',
    'status' => 'Status',
    'quantity' => 'Quantity',
    'return_invoice_message' => 'This is computer generated receipt and does not require physical signature',
    'note' => 'NOTE',
];
