/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  Strucutre:
          url     => router path
          name    => name to display in sidebar
          slug    => router path name
          icon    => Feather Icon component/icon name
          tag     => text to display on badge
          tagColor  => class to apply on badge element
          i18n    => Internationalization
          submenu   => submenu of current item (current item will become dropdown )
                NOTE: Submenu don't have any icon(you can add icon if u want to display)
          isDisabled  => disable sidebar item/group
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default [
  // {
  //   url: "/dashboard/email",
  //   name: "Email",
  //   slug: "email",
  //   icon: "MailIcon",
  //   i18n: "Email",
  // },
  {
    url: null,
    name: 'Dashboard',
    tagColor: 'warning',
    icon: 'HomeIcon',
    i18n: 'Dashboard',
    submenu: [
      {
        url: '/dashboard/analytics',
        name: 'Analytics',
        slug: 'dashboard-analytics',
        i18n: 'Analytics'
      },
    ]
  },
  {
    header: 'Apps',
    icon: 'PackageIcon',
    i18n: 'Dashboard',
    items: [
      {
        url: null,
        name: 'Admins',
        i18n: 'Admins',
        icon: 'UsersIcon',
        submenu: [
          {
            url: '/dashboard/admin/admin-list',
            name: 'List',
            slug: 'dashboard-admin-list',
            i18n: 'List'
          },
          {
            url: '/dashboard/admin/admin-create',
            name: 'View',
            slug: 'dashboard-admin-create',
            i18n: 'Create'
          },
        ]
      },
      {
        url: null,
        name: 'News',
        i18n: 'News',
        icon: 'CalendarIcon',
        submenu: [
          {
            url: '/dashboard/news/news-list',
            name: 'List',
            slug: 'dashboard-news-list',
            i18n: 'List'
          },
          {
            url: '/dashboard/news/news-create',
            name: 'View',
            slug: 'dashboard-news-create',
            i18n: 'Create'
          },
        ]
      },
       {
            url: null,
            name: 'Packages',
            i18n: 'Packages',
            icon: 'GridIcon',
            submenu: [
                {
                    url: '/dashboard/packages/packages-list',
                    name: 'List',
                    slug: 'dashboard-packages-list',
                    i18n: 'List'
                },
                {
                    url: '/dashboard/packages/packages-create',
                    name: 'View',
                    slug: 'dashboard-packages-create',
                    i18n: 'Create'
                },
            ]
        },
        {
            url: null,
            name: 'Services',
            i18n: 'Services',
            icon: 'TruckIcon',
            submenu: [
                {
                    url: '/dashboard/services/services-list',
                    name: 'List',
                    slug: 'dashboard-services-list',
                    i18n: 'List'
                },
                {
                    url: '/dashboard/services/services-create',
                    name: 'View',
                    slug: 'dashboard-services-create',
                    i18n: 'Create'
                },
            ]
        },
        {
            url: null,
            name: 'Testimonials',
            i18n: 'Testimonials',
            icon: 'UsersIcon',
            submenu: [
                {
                    url: '/dashboard/testimonials/testimonials-list',
                    name: 'List',
                    slug: 'dashboard-testimonials-list',
                    i18n: 'List'
                },
                {
                    url: '/dashboard/testimonials/testimonials-create',
                    name: 'View',
                    slug: 'dashboard-testimonials-create',
                    i18n: 'Create'
                },
            ]
        },
        {
            url: null,
            name: 'Partners',
            i18n: 'Partners',
            icon: 'CalendarIcon',
            submenu: [
                {
                    url: '/dashboard/partners/partners-list',
                    name: 'List',
                    slug: 'dashboard-partners-list',
                    i18n: 'List'
                },
                {
                    url: '/dashboard/partners/partners-create',
                    name: 'View',
                    slug: 'dashboard-partners-create',
                    i18n: 'Create'
                },
            ]
        },
        {
            url: null,
            name: 'Settings',
            i18n: 'Settings',
            icon: 'SettingsIcon',
            submenu: [
                {
                    url: '/dashboard/settings/1/general',
                    name: 'General',
                    slug: 'dashboard-settings-general',
                    i18n: 'General'
                },
                {
                    url: '/dashboard/settings/2/home',
                    name: 'Home',
                    slug: 'dashboard-settings-home',
                    i18n: 'About Us'
                },
                {
                    url: '/dashboard/settings/3/others',
                    name: 'Others',
                    slug: 'dashboard-settings-others',
                    i18n: 'How It Work'
                },
            ]
        },

    ]
  },
]
