// axios
import axios from 'axios'
import router from './router'

axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('adminToken')}`


if (process.env.NODE_ENV == 'development') {
    window.getAppURL = 'http://127.0.0.1:8000/'
} else {
    window.getAppURL = 'https://beta.tadween.app/'
}

export default axios.create({
    baseURL: window.getAppURL + 'api',
    transformResponse: [function (response) {

        response = JSON.parse(response)

        if (response.status == false && response.message) {

            if (response.message == 'UnAuthinticated') {
                logout()
            } else if (response.message == 'Not Found') {
                router.push('/dashboard/error-404').catch(() => {
                })
            } else if (response.message == 'Not Authorized') {
                router.push('/dashboard/not-authorized').catch(() => {
                })
            }

            return {
                message: response.message
            }
        } else if (response.exception) {

            router.push('/dashboard/error-500').catch(() => {
            })

        }

        return response.data ? response.data : []
    }]

})


function logout () {
    localStorage.removeItem('adminToken')
    localStorage.removeItem('adminInfo')

    router.push('/dashboard/login').catch(() => {
    })

}
