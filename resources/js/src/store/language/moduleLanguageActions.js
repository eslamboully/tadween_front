/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Language Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import axios from '@/axios.js'

export default {

  fetchLanguages ({ commit } , type) {
    return new Promise((resolve, reject) => {

      axios.get('/dashboard/languages')
        .then((response) => {
          commit('SET_LANGUAGES', response.data)

          resolve(response.data)
        })
        .catch((error) => { reject(error) })
    })
  },

  saveNewLanguage({commit} , category){

    return new Promise( (resolve , reject ) => {

      var form_data = new FormData();

      for ( var key in category ) {
        form_data.append(key, category[key]);
      }

      axios.post('/dashboard/languages', form_data)
        .then(res => {

          if (res.data && res.data.message) {
            reject(res.data.message);
          }
          resolve(category);
      })

    });


  },

  fetchLanguage (context, categoryId) {
    return new Promise((resolve, reject) => {
      axios.get(`/dashboard/languages/${categoryId}`)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },



  updateLanguage({commit} , category){

    return new Promise((resolve, reject) => {

      var form_data = new FormData();

      for ( var key in category ) {
          form_data.append(key, category[key]);
      }

      form_data.append('_method' , 'PUT');

      axios.post(`/dashboard/languages/${category.id}` , form_data)
        .then((res) => {

          if (res.data && res.data.message) {
            reject(res.data.message);
          }

          resolve(res.data);

        })
        .catch((error) => { reject(error) })
    })

  },

  removeLanguage ({ commit }, categoryId) {
    return new Promise((resolve, reject) => {
      axios.delete(`/dashboard/languages/${categoryId}`)
        .then((response) => {
          commit('REMOVE_LANGUAGE', categoryId)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  }

}
