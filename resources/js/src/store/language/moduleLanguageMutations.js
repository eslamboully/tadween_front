/*=========================================================================================
  File Name: moduleAuthMutations.js
  Description: Auth Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/language/pixinvent
==========================================================================================*/

import axios from '../../http/axios/index.js'

export default {

  SET_LANGUAGES (state , languages) {
    state.languages = languages;
  },
  REMOVE_LANGUAGE (state, languageId) {
    const languageIndex = state.languages.findIndex((u) => u.id === languageId)
    state.languages.splice(languageIndex, 1)
  },

}
