/*=========================================================================================
  File Name: moduleLanguage.js
  Description: Language Module
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Languageor: Pixinvent
  Languageor URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import state from './moduleLanguageState.js'
import mutations from './moduleLanguageMutations.js'
import actions from './moduleLanguageActions.js'
import getters from './moduleLanguageGetters.js'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
