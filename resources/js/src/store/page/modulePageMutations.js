/*=========================================================================================
  File Name: moduleAuthMutations.js
  Description: Auth Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/page/pixinvent
==========================================================================================*/

import axios from '../../http/axios/index.js'

export default {

  SET_PAGES (state , pages) {
    state.pages = pages;
  },
  REMOVE_PAGE (state, pageId) {
    const pageIndex = state.pages.findIndex((u) => u.id === pageId)
    state.pages.splice(pageIndex, 1)
  },

}
