/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Page Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import axios from '@/axios.js'

export default {

  fetchPages ({ commit } , type) {
    return new Promise((resolve, reject) => {

      axios.get('/dashboard/pages')
        .then((response) => {
          commit('SET_PAGES', response.data)

          resolve(response.data)
        })
        .catch((error) => { reject(error) })
    })
  },

  saveNewPage({commit} , page){

    return new Promise( (resolve , reject ) => {

      var form_data = new FormData();

      for ( var key in page ) {
        form_data.append(key, page[key]);

      }

      axios.post('/dashboard/pages', form_data)
        .then(res => {

          if (res.data && res.data.message) {
            reject(res.data.message);
          }
          resolve(page);
      })

    });


  },

  fetchPage (context, pageId) {
    return new Promise((resolve, reject) => {
      axios.get(`/dashboard/pages/${pageId}`)
        .then((response) => {
          resolve(response.data)
        })
        .catch((error) => { reject(error) })
    })
  },



  updatePage({commit} , page){

    return new Promise((resolve, reject) => {

      var form_data = new FormData();

      for ( var key in page ) {
          form_data.append(key, page[key]);
      }

      form_data.append('_method' , 'PUT');

      axios.post(`/dashboard/pages/${page.id}` , form_data)
        .then((res) => {

          if (res.data && res.data.message) {
            reject(res.data.message);
          }

          resolve(res.data);

        })
        .catch((error) => { reject(error) })
    })

  },

  removePage ({ commit }, pageId) {
    return new Promise((resolve, reject) => {
      axios.delete(`/dashboard/pages/${pageId}`)
        .then((response) => {
          commit('REMOVE_PAGE', pageId)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  }

}
