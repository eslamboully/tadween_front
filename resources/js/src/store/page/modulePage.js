/*=========================================================================================
  File Name: modulePage.js
  Description: Page Module
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Pageor: Pixinvent
  Pageor URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import state from './modulePageState.js'
import mutations from './modulePageMutations.js'
import actions from './modulePageActions.js'
import getters from './modulePageGetters.js'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
