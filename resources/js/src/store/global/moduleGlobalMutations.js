/*=========================================================================================
  File Name: moduleAuthMutations.js
  Description: Auth Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

export default {

    REMOVE_CONTACTUS(state , id){

    const index = state.contactus.findIndex(c => c.id == id);

    state.contactus = state.contactus.splice(index , 1);

    },

    SET_CONTACTUS(state , contactus){

    state.contactus = contactus;

    },

    REMOVE_REPORT_ISSUES(state , id){

        const index = state.report_issues.findIndex(c => c.id == id);

        state.report_issues = state.report_issues.splice(index , 1);

    },

    SET_REPORT_ISSUES(state , report_issues){

        state.report_issues = report_issues;

}

};
