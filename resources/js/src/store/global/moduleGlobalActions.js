/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import axios from '@/axios.js'

export default {

  sendContact ({ commit } , data) {
    return new Promise((resolve, reject) => {
      axios.post('/contact' , data)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },

  sendSupport ({ commit } , data) {
    return new Promise((resolve, reject) => {

      var form_data = new FormData();

      for ( var key in data ) {
        form_data.append(key, data[key]);
      }

      axios.post('/support' , form_data)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },


  fetchSupports( {commit} ){

    return new Promise((resolve , reject)=>{

      return axios.get('/support').then(support  => {
        resolve(support.data);
      }).catch(error => {
        reject(error);
      });

    });

  },

    fetchReportIssues( {commit} ){

        return new Promise((resolve , reject)=>{

            return axios.get('/report-issues').then(reportIssues  => {
                resolve(reportIssues.data);
            }).catch(error => {
                reject(error);
            });

        });

    },



  fetchSupport( {commit} , id){

    return new Promise((resolve , reject)=>{

      return axios.get('/support/' + id).then(support => {
        resolve(support.data);
      }).catch(error => {
        reject(error);
      });

    });

  },


    fetchReportIssue( {commit} , id){

        return new Promise((resolve , reject)=>{

            return axios.get('/report-issues/' + id).then(reportIssue => {

                resolve(reportIssue.data);
                commit('SET_REPORT_ISSUES' , reportIssue.data);

            }).catch(error => {
                reject(error);
            });

        });

    },


  fetchCategories( {commit} ){

    return new Promise((resolve , reject)=>{

      return axios.get('/categories').then(categories => {
        resolve(categories.data);
      }).catch(error => {
        reject(error);
      });

    });

  },

  statistics( {commit} ){

    return new Promise((resolve , reject)=>{

      return axios.get('/statistics').then(statistics => {
        resolve(statistics.data);
      }).catch(error => {
        reject(error);
      });

    });

  },

  fetchSetting( {commit} ){

    return new Promise((resolve , reject)=>{

      return axios.get('/setting').then(setting => {
        resolve(setting.data);
      }).catch(error => {
        reject(error);
      });

    });

  },


  updateSetting( {commit} , setting ){

    return new Promise((resolve , reject)=>{

      var form_data = new FormData();

      for ( var key in setting ) {
        form_data.append(key, setting[key]);
      }

      return axios.post('/setting' , form_data).then(response => {

        if (response.data && response.data.message)
          reject(response.data);

        resolve(response);

      }).catch(error => {
        reject(error);
      });

    });

  },



  fetchContactus( {commit} ){

    return new Promise((resolve , reject)=>{

      return axios.get('/contactus').then(contactsus  => {

        commit('SET_CONTACTUS' , contactsus.data);

        resolve(contactsus.data);
      }).catch(error => {
        reject(error);
      });

    });

  },


  fetchContact( {commit} , id){

    return new Promise((resolve , reject)=>{

      return axios.get('/contactus/' + id).then(contact => {
        resolve(contact.data);
      }).catch(error => {
        reject(error);
      });

    });

  },

  fetchNewsletter( {commit} ){

    return new Promise((resolve , reject)=>{

      return axios.get('/newsletter').then(contactsus  => {

        resolve(contactsus.data);
      }).catch(error => {
        reject(error);
      });

    });

  },

  subscribe( {commit} , email ){

    return new Promise((resolve , reject)=>{

      var formData = new FormData();

      formData.append('email' , email);

      return axios.post('/newsletter' , formData).then(res  => {
        resolve(res);
      }).catch(error => {
        reject(error);
      });

    });

  },


  removeContactus( {commit} , id ){

    return new Promise((resolve , reject)=>{

      var formData = new FormData();

      formData.append('_method' , 'DELETE');

      return axios.post('/contactus/' + id , formData ).then(res => {

        commit('REMOVE_CONTACTUS' , id);
        resolve(id);

      }).catch(error => {
        reject(error);
      });

    });

  },

    removeReportIssue( {commit} , id ){

        return new Promise((resolve , reject)=>{

            var formData = new FormData();

            formData.append('_method' , 'DELETE');

            return axios.post('/report-issues/' + id , formData ).then(res => {

                commit('REMOVE_REPORT_ISSUES' , id);
                resolve(id);

            }).catch(error => {
                reject(error);
            });

        });

    },




}
