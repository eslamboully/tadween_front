/*=========================================================================================
  File Name: moduleGlobal.js
  Description: Global Module
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Globalor: Pixinvent
  Globalor URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import state from './moduleGlobalState.js'
import mutations from './moduleGlobalMutations.js'
import actions from './moduleGlobalActions.js'
import getters from './moduleGlobalGetters.js'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
