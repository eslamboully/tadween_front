/*=========================================================================================
  File Name: moduleEcommerceState.js
  Description: Ecommerce Module State
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

/* eslint-disable */
export default {
  stores:[],
  orders:[],
  new_store:'',
  new_product:'',
  new_news:'',
  new_partner:'',
  new_package:'',
  new_service:'',
  products:[],
  news:[],
  testimonials:[],
  packages:[],
  partners:[],
  services:[],
  configs:[],
  payment_gatewaies:[],
  payment_statuses:[],
  order_statuses:[],
}
/* eslint-enable */
