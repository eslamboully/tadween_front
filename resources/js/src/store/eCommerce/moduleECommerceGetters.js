/*=========================================================================================
  File Name: moduleEcommerceGetters.js
  Description: Ecommerce Module Getters
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default {

  get_orders: state => state.orders,

  currentNewStore:state => state.new_store,

  currentNewProduct:state => state.new_product,

  currentNewNews:state => state.new_news,

  currentNewPackage:state => state.new_package,

  currentNewService:state => state.new_service,

  currentNewPartner:state => state.new_partner,

}
