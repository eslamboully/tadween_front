/*=========================================================================================
  File Name: moduleEcommerceActions.js
  Description: Ecommerce Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import axios from '../../axios'

export default {

    fetchNews ({ commit }) {
        return new Promise((resolve, reject) => {
            axios.get('/dashboard/news')
                .then((response) => {
                    commit('SET_NEWS', response.data)

                    resolve(response.data)
                })
                .catch((error) => { reject(error) })
        })
    },
    fetchNew (context, newId) {
        return new Promise((resolve, reject) => {
            axios.get(`dashboard/news/${newId}`)
                .then((response) => {

                    resolve(response)
                })
                .catch((error) => { reject(error) })
        })
    },
    saveNewNews({getters , commit } , news = ''){

        return new Promise( (resolve , reject ) => {

            if (!news) {
                news = getters.currentNewNews;
            }

            var form_data = new FormData();

            commit("RESET_CURRENT_NEWS");

            for ( var key in news ) {
                form_data.append(key, news[key]);

            }

            axios.post('/dashboard/news', form_data)
                .then(res => {

                    if (res.data && res.data.message) {
                        reject(res.data.message);
                    }

                    resolve(news);

                })

        });


    },

    saveNewPackage({getters , commit } , packages = ''){

        return new Promise( (resolve , reject ) => {

            if (!packages) {
                packages = getters.currentNewPackage;
            }

            var form_data = new FormData();

            commit("RESET_CURRENT_PACKAGES");

            for ( var key in packages ) {
                form_data.append(key, packages[key]);

            }

            axios.post('/dashboard/packages', form_data)
                .then(res => {

                    if (res.data && res.data.message) {
                        reject(res.data.message);
                    }

                    resolve(packages);

                })

        });


    },

    saveNewtestimonials(context , testimonial){

        return new Promise( (resolve , reject ) => {
            var form_data = new FormData();

            for ( var key in testimonial ) {

                var value = testimonial[key];

                form_data.append(key, value);
            }

            axios.post('/dashboard/testimonials', form_data)
                .then(res => {
                    if (res.data && res.data.message) {
                        reject(res.data.message);
                    }
                    resolve(testimonial);
                })
        });


    },

    saveNewService({getters , commit } , news = ''){

        return new Promise( (resolve , reject ) => {

            if (!news) {
                news = getters.currentNewService;
            }

            var form_data = new FormData();

            commit("RESET_CURRENT_NEWS");

            for ( var key in news ) {
                form_data.append(key, news[key]);

            }

            axios.post('/dashboard/services', form_data)
                .then(res => {

                    if (res.data && res.data.message) {
                        reject(res.data.message);
                    }

                    resolve(news);

                })

        });


    },
    saveNewPartner({getters , commit } , partners = ''){

        return new Promise( (resolve , reject ) => {

            if (!partners) {
                partners = getters.currentNewPartner;
            }

            var form_data = new FormData();

            commit("RESET_CURRENT_PARTNERS");

            for ( var key in partners ) {
                form_data.append(key, partners[key]);

            }

            axios.post('/dashboard/partners', form_data)
                .then(res => {

                    if (res.data && res.data.message) {
                        reject(res.data.message);
                    }

                    resolve(partners);

                })

        });


    },
    updateNews({commit} , news){

        return new Promise((resolve, reject) => {

            var form_data = new FormData();

            for ( var key in news ) {
                form_data.append(key, news[key]);
            }

            form_data.append('_method' , 'PUT');

            axios.post(`/dashboard/news/${news.id}` , form_data)
                .then((res) => {

                    if (res.data && res.data.message) {
                        reject(res.data.message);
                    }

                    resolve(res.data);

                })
                .catch((error) => { reject(error) })
        })

    },
    removeNews ({ commit }, newsId) {
        return new Promise((resolve, reject) => {
            axios.delete(`/dashboard/news/${newsId}`)
                .then((response) => {
                    commit('REMOVE_NEWS', newsId)
                    resolve(response)
                })
                .catch((error) => { reject(error) })
        })
    },

    removeTestimonials ({ commit }, testimonialId) {
        return new Promise((resolve, reject) => {
            axios.delete(`/dashboard/testimonials/${testimonialId}`)
                .then((response) => {
                    commit('REMOVE_TESTIMONIAL', testimonialId)
                    resolve(response)
                })
                .catch((error) => { reject(error) })
        })
    },

    fetchTestimonials ({ commit }) {
        return new Promise((resolve, reject) => {
            axios.get('/dashboard/testimonials')
                .then((response) => {
                    commit('SET_TESTIMONIALS', response.data)

                    resolve(response.data)
                })
                .catch((error) => { reject(error) })
        })
    },

    fetchPackages ({ commit }) {
        return new Promise((resolve, reject) => {
            axios.get('/dashboard/packages')
                .then((response) => {
                    commit('SET_PACKAGES', response.data)

                    resolve(response.data)
                })
                .catch((error) => { reject(error) })
        })
    },

    fetchPartners ({ commit }) {
        return new Promise((resolve, reject) => {
            axios.get('/dashboard/partners')
                .then((response) => {
                    commit('SET_PARTNERS', response.data)

                    resolve(response.data)
                })
                .catch((error) => { reject(error) })
        })
    },
    fetchPackage (context, newId) {
        return new Promise((resolve, reject) => {
            axios.get(`dashboard/packages/${newId}`)
                .then((response) => {

                    resolve(response)
                })
                .catch((error) => { reject(error) })
        })
    },

    fetchTestimonial (context, testimonialId) {
        return new Promise((resolve, reject) => {
            axios.get(`dashboard/testimonials/${testimonialId}`)
                .then((response) => {
                    resolve(response)
                })
                .catch((error) => { reject(error) })
        })
    },

    fetchPartner (context, newId) {
        return new Promise((resolve, reject) => {
            axios.get(`dashboard/partners/${newId}`)
                .then((response) => {

                    resolve(response)
                })
                .catch((error) => { reject(error) })
        })
    },
    updatePackage({commit} , packages){

        return new Promise((resolve, reject) => {

            var form_data = new FormData();

            for ( var key in packages ) {
                form_data.append(key, packages[key]);
            }

            form_data.append('_method' , 'PUT');

            axios.post(`/dashboard/packages/${packages.id}` , form_data)
                .then((res) => {

                    if (res.data && res.data.message) {
                        reject(res.data.message);
                    }

                    resolve(res.data);

                })
                .catch((error) => { reject(error) })
        })

    },

    updateTestimonial({commit} , testimonial){

        return new Promise((resolve, reject) => {

            var form_data = new FormData();

            for ( var key in testimonial ) {
                form_data.append(key, testimonial[key]);
            }

            form_data.append('_method' , 'PUT');

            axios.post(`/dashboard/testimonials/${testimonial.id}` , form_data)
                .then((res) => {

                    if (res.data && res.data.message) {
                        reject(res.data.message);
                    }

                    resolve(res.data);

                })
                .catch((error) => { reject(error) })
        })

    },

    updatePartners({commit} , partners){

        return new Promise((resolve, reject) => {

            var form_data = new FormData();

            for ( var key in partners ) {
                form_data.append(key, partners[key]);
            }

            form_data.append('_method' , 'PUT');

            axios.post(`/dashboard/partners/${partners.id}` , form_data)
                .then((res) => {

                    if (res.data && res.data.message) {
                        reject(res.data.message);
                    }

                    resolve(res.data);

                })
                .catch((error) => { reject(error) })
        })

    },

    removePackages ({ commit }, packagesId) {
        return new Promise((resolve, reject) => {
            axios.delete(`/dashboard/packages/${packagesId}`)
                .then((response) => {
                    commit('REMOVE_PACKAGES', packagesId)
                    resolve(response)
                })
                .catch((error) => { reject(error) })
        })
    },
    removePartners ({ commit }, partnersId) {
        return new Promise((resolve, reject) => {
            axios.delete(`/dashboard/partners/${partnersId}`)
                .then((response) => {
                    commit('REMOVE_PARTNERS', partnersId)
                    resolve(response)
                })
                .catch((error) => { reject(error) })
        })
    },



    fetchServices ({ commit }) {
        return new Promise((resolve, reject) => {
            axios.get('/dashboard/services')
                .then((response) => {
                    commit('SET_SERVICES', response.data)

                    resolve(response.data)
                })
                .catch((error) => { reject(error) })
        })
    },
    fetchService (context, serviceId) {
        return new Promise((resolve, reject) => {
            axios.get(`dashboard/services/${serviceId}`)
                .then((response) => {
                    resolve(response)
                })
                .catch((error) => { reject(error) })
        })
    },
    updateService ({commit} , service){

        return new Promise((resolve, reject) => {

            var form_data = new FormData();

            for ( var key in service ) {
                form_data.append(key, service[key]);
            }

            form_data.append('_method' , 'PUT');

            axios.post(`/dashboard/services/${service.id}` , form_data)
                .then((res) => {

                    if (res.data && res.data.message) {
                        reject(res.data.message);
                    }

                    resolve(res.data);

                })
                .catch((error) => { reject(error) })
        })

    },

    removeServices ({ commit }, serviceId) {
        return new Promise((resolve, reject) => {
            axios.delete(`/dashboard/services/${serviceId}`)
                .then((response) => {
                    commit('REMOVE_SERVICE', serviceId)
                    resolve(response)
                })
                .catch((error) => { reject(error) })
        })
    },


    fetchConfigs ({ commit },configsId) {
        return new Promise((resolve, reject) => {
            axios.get(`/dashboard/configs/${configsId}`)
                .then((response) => {
                    commit('SET_CONFIGS', response.data)

                    resolve(response.data)
                })
                .catch((error) => { reject(error) })
        })
    },

    updateConfigs ({commit} , configs){

        return new Promise((resolve, reject) => {

            var form_data = new FormData();

            for ( let config of configs ) {
                if (config.static == 0) {
                    form_data.append(config.var+'-ar', config.value_ar);
                    form_data.append(config.var+'-en', config.value_en);
                }else{
                    form_data.append(config.var, config.static_value);
                }
            }
            console.log(form_data);
            form_data.append('_method' , 'PUT');

            axios.post(`/dashboard/configs` , form_data)
                .then((res) => {

                    if (res.data && res.data.message) {
                        reject(res.data.message);
                    }

                    resolve(res.data);

                })
                .catch((error) => { reject(error) })
        })

    },

}
