/*=========================================================================================
  File Name: moduleEcommerceMutations.js
  Description: Ecommerce Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

export default {

  SET_ORDERS(state , orders){
    state.orders = orders;
  },
  SET_PRODUCTS(state , products){
    state.products = products;
  },

  SET_STORES(state , stores){
    state.stores = stores;
  },

  SET_TESTIMONIALS(state , testimonials){
    state.testimonials = testimonials;
  },

  SET_PAYMENT_GATEWAIES(state , payment_gatewaies){
    state.payment_gatewaies = payment_gatewaies;
  },

  SET_PAYMENT_STATUSES(state , payment_statuses){
    state.payment_statuses = payment_statuses;
  },

  SET_ORDER_STATUSES(state , order_statuses){
    state.order_statuses = order_statuses;
  },

  REMOVE_ORDER(state , id){
    const index = state.orders.findIndex( order => order.id == id );
    state.orders.splice(index , 1);
  },

  REMOVE_STORE(state , id){
    const index = state.stores.findIndex( store => store.id == id );
    state.stores.splice(index , 1);
  },

  REMOVE_TESTIMONIAL(state , id){
    const index = state.testimonials.findIndex( testimonial => testimonial.id == id );
    state.testimonials.splice(index , 1);
  },

  REMOVE_PRODUCT(state , id){
    const index = state.products.findIndex( product => product.id == id );
    state.products.splice(index , 1);
  },

  REMOVE_PAYMENT_STATUS(state , id){
    const index = state.payment_statuses.findIndex( payment_status => payment_status.id == id );
    state.payment_statuses.splice(index , 1);
  },

  REMOVE_ORDER_STATUS(state , id){
    const index = state.order_statuses.findIndex( order_status => order_status.id == id );
    state.order_statuses.splice(index , 1);
  },

  REMOVE_PAYMENT_GATEWAY(state , id){
    const index = state.payment_gatewaies.findIndex( payment_gateway => payment_gateway.id == id );
    state.payment_gatewaies.splice(index , 1);
  },

  SET_NEWS(state , news) {
    state.news = news;
  },

  SET_PACKAGES(state , packages) {
    state.packages = packages;
  },

  SET_SERVICES(state , services) {
    state.services = services;
  },

  SET_PARTNERS(state , partners) {
    state.partners = partners;
  },

  SET_CONFIGS(state , configs) {
    state.configs = configs;
  },
  RESET_CURRENT_NEWS (state) {
    state.new_news = {}
  },

  RESET_CURRENT_PARTNERS (state) {
    state.new_partner = {}
  },

  RESET_CURRENT_PACKAGES (state) {
    state.new_package = {}
  },
  REMOVE_NEWS(state , id){
    const index = state.news.findIndex( news => news.id == id );
    state.news.splice(index , 1);
  },

  REMOVE_PARTNERS(state , id){
    const index = state.partners.findIndex( partner => partner.id == id );
    state.partners.splice(index , 1);
  },

  REMOVE_PACKAGES(state , id){
    const index = state.packages.findIndex( packages => packages.id == id );
    state.packages.splice(index , 1);
  },

  REMOVE_SERVICE(state , id) {
    const index = state.services.findIndex( services => services.id == id );
    state.services.splice(index , 1);
  },


}
