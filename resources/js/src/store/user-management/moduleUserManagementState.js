/*=========================================================================================
  File Name: moduleCalendarState.js
  Description: Calendar Module State
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

export default {
  users: [],
  user_types:[],
  invited_clients:[],
  invited_stores:[],
  invited_representatives:[],
  admins:[],
  news:[],
  new_user:{
    'email' : '',
    'name' : '',
    'password' : '',
    'type': '',
    'username' :  '',
    'commercial_registered' : '',
    'issue_place' :  '',
    'issue_date' : '',
    'expiry_date':  '',
    'building_number' :  '',
    'street_name' :  '',
    'city' :  '',
    'neighborhood' : '',
    'postal_code' :  '',
    'website' :  '',
    'phone' :  '',
    'telephone' : '',
    'fax' :  '',
    'longitude' : '',
    'latitude' :  '',
    'photo' : '/assets/images/users/default.png'
  }
}
