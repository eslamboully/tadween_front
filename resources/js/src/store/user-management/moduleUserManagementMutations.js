/*=========================================================================================
  File Name: moduleCalendarMutations.js
  Description: Calendar Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default {
  SET_USERS (state, users) {
    state.users = users
  },
  SET_ADMINS (state, admins) {
    state.admins = admins
  },

  SET_INVITED_CLIENTS (state, invited_clients) {
    state.invited_clients = invited_clients
  },

  RESET_CURRENT_USER (state) {
    state.new_user = {}
  },

  SET_INVITED_STORES (state, invited_stores) {
    state.invited_stores = invited_stores
  },

  SET_INVITED_EMPLOYEES (state, invited_representatives) {
    state.invited_representatives = invited_representatives
  },

  SET_USER_TYPES (state, user_types) {
    state.user_types = user_types
  },

  REMOVE_RECORD (state, itemId) {
    const userIndex = state.users.findIndex((u) => u.id === itemId)
    state.users.splice(userIndex, 1)
  },
  REMOVE_ADMIN (state, itemId) {
    const adminIndex = state.admins.findIndex((u) => u.id === itemId)
    state.admins.splice(adminIndex, 1)
  },

  REMOVE_INVITED_CLIENT (state, itemId) {
    const invitedClientIndex = state.invited_clients.findIndex((u) => u.id === itemId)
    state.invited_clients.splice(invitedClientIndex, 1)
  },

  REMOVE_INVITED_STORE (state, itemId) {
    const invitedStoreIndex = state.invited_stores.findIndex((u) => u.id === itemId)
    state.invited_stores.splice(invitedStoreIndex, 1)
  },

  REMOVE_INVITED_EMPLOYEE (state, itemId) {
    const invitedRepresentativeIndex = state.invited_representatives.findIndex((u) => u.id === itemId)
    state.invited_representatives.splice(invitedRepresentativeIndex, 1)
  },

  NEW_USER(state , user){
    Object.assign(state.new_user , user);
  }
}
