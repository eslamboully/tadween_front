/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import axios from '@/axios.js'

export default {
  // addItem({ commit }, item) {
  //   return new Promise((resolve, reject) => {
  //     axios.post("/api/data-list/products/", {item: item})
  //       .then((response) => {
  //         commit('ADD_ITEM', Object.assign(item, {id: response.data.id}))
  //         resolve(response)
  //       })
  //       .catch((error) => { reject(error) })
  //   })
  // },
  userTypes({commit}){

    return new Promise((resolve , reject)=>{

      axios.get('/user-types').then(response => {

        commit('SET_USER_TYPES' , response.data);

        resolve(response.data);

      }).catch(error => {
        reject(error);
      });

    });

  } ,
  setNewUser({commit} , user){

    return new Promise( (resolve , reject ) => {

      commit('NEW_USER' , user );

      resolve(user);

    });

  },
  currentNewUser({getters}){

    return new Promise( (resolve , reject ) => {

      var user = getters.currentNewUser;

      var keys = Object.keys(user);

      var new_user = {};

      keys.forEach(attr =>{
        new_user[attr] = user[attr];
      });

      resolve(new_user);

    });

  },

  saveNewUser({getters , commit } , user = ''){

    return new Promise( (resolve , reject ) => {

      if (!user) {
        user = getters.currentNewUser;
      }

      var form_data = new FormData();

      commit("RESET_CURRENT_USER");

      for ( var key in user ) {
        form_data.append(key, user[key]);

      }

      axios.post('/users', form_data)
    	  .then(res => {

          if (res.data && res.data.message) {
            reject(res.data.message);
          }

          resolve(user);

      })

    });


  },


  saveNewAdmin({commit} , admin){

    return new Promise( (resolve , reject ) => {

      var form_data = new FormData();

      for ( var key in admin ) {
        form_data.append(key, admin[key]);
      }

      axios.post('/dashboard/admins', form_data)
    	  .then(res => {


          if (res.data && res.data.message) {
            reject(res.data.message);
          }

          resolve(admin);

      })

    });


  },


  fetchInvitedClients ({ commit } , type) {
    return new Promise((resolve, reject) => {

      axios.get('/dashboard/invited-clients')
        .then((response) => {
          commit('SET_INVITED_CLIENTS', response.data)
          resolve(response.data)
        })
        .catch((error) => { reject(error) })
    })
  },

  fetchInvitedStores({ commit } , type) {
    return new Promise((resolve, reject) => {

      axios.get('/dashboard/invited-stores')
        .then((response) => {
          commit('SET_INVITED_STORES', response.data)
          resolve(response.data)
        })
        .catch((error) => { reject(error) })
    })
  },

  fetchInvitedRepresentatives ({ commit } , type) {
    return new Promise((resolve, reject) => {

      axios.get('/dashboard/invited-representatives')
        .then((response) => {
          commit('SET_INVITED_EMPLOYEES', response.data)
          resolve(response.data)
        })
        .catch((error) => { reject(error) })
    })
  },

  saveNewInvitedClient({commit} , invited_client){
    return new Promise( (resolve , reject ) => {
      var form_data = new FormData();
      for ( var key in invited_client ) {
        form_data.append(key, invited_client[key]);
      }
      axios.post('/dashboard/invited-clients', form_data)
    	  .then(res => {
          if (res.data && res.data.message) {
            reject(res.data.message);
          }
          resolve(invited_client);
      })
    });
  },

  saveNewInvitedStore({commit} , invited_store){
    return new Promise( (resolve , reject ) => {
      var form_data = new FormData();
      for ( var key in invited_store ) {
        form_data.append(key, invited_store[key]);
      }
      axios.post('/dashboard/invited-stores', form_data)
    	  .then(res => {
          if (res.data && res.data.message) {
            reject(res.data.message);
          }
          resolve(invited_store);
      })
    });
  },

  saveNewInvitedRepresentative({commit} , invited_representative){
    return new Promise( (resolve , reject ) => {
      var form_data = new FormData();
      for ( var key in invited_representative ) {
        form_data.append(key, invited_representative[key]);
      }
      axios.post('/dashboard/invited-representatives', form_data)
    	  .then(res => {
          if (res.data && res.data.message) {
            reject(res.data.message)
          }
          resolve(invited_representative)
      })
    });
  },

  fetchAdmins ({ commit }) {
    return new Promise((resolve, reject) => {

      axios.get('/dashboard/admins')
        .then((response) => {
          commit('SET_ADMINS', response.data)

          resolve(response.data)
        })
        .catch((error) => { reject(error) })
    })
  },

  fetchAdmin (context, adminId) {
    return new Promise((resolve, reject) => {
      axios.get(`/dashboard/admins/${adminId}`)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },



  fetchInvitedClient (context, invitedClientId) {
    return new Promise((resolve, reject) => {
      axios.get(`/dashboard/invited-clients/${invitedClientId}`)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },

  fetchInvitedStore (context, invitedStoreId) {
    return new Promise((resolve, reject) => {
      axios.get(`/dashboard/invited-stores/${invitedStoreId}`)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },

  fetchInvitedRepresentative (context, invitedRepresentativeId) {
    return new Promise((resolve, reject) => {
      axios.get(`/dashboard/invited-representatives/${invitedRepresentativeId}`)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },


  fetchUserPermissions (context, userId) {
    return new Promise((resolve, reject) => {
      axios.get(`/dashboard/users/permissions/${userId}`)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },

  updateUserPermissions({commit} , data){

    return new Promise((resolve, reject) => {

      axios.post(`/dashboard/users/update-permissions/${data.userId}` , data.permissions)
        .then((res) => {

          if (res.data && res.data.message) {
            reject(res.data.message);
          }

          resolve(data);

        })
        .catch((error) => { reject(error) })
    })

  },



  fetchAdminPermissions (context, adminId) {
    return new Promise((resolve, reject) => {
      axios.get(`/dashboard/admins/permissions/${adminId}`)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },

  updateAdminPermissions({commit} , data){

    return new Promise((resolve, reject) => {

      axios.post(`/dashboard/admins/update-permissions/${data.adminId}` , data.permissions)
        .then((res) => {

          if (res.data && res.data.message) {
            reject(res.data.message);
          }

          resolve(data);

        })
        .catch((error) => { reject(error) })
    })

  },

  removeInvitedClient ({ commit }, invitedClientId) {
    return new Promise((resolve, reject) => {
      axios.delete(`/dashboard/invited-clients/${invitedClientId}`)
        .then((response) => {
          commit('REMOVE_INVITED_CLIENT', invitedClientId)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },

  removeInvitedStore ({ commit }, invitedStoreId) {
    return new Promise((resolve, reject) => {
      axios.delete(`/dashboard/invited-stores/${invitedStoreId}`)
        .then((response) => {
          commit('REMOVE_INVITED_STORE', invitedStoreId)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },

  removeInvitedRepresentative ({ commit }, invitedRepresentativeId) {
    return new Promise((resolve, reject) => {
      axios.delete(`/dashboard/invited-representatives/${invitedRepresentativeId}`)
        .then((response) => {
          commit('REMOVE_INVITED_EMPLOYEE', invitedRepresentativeId)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },

  removeAdmin ({ commit }, adminId) {
    return new Promise((resolve, reject) => {
      axios.delete(`/dashboard/admins/${adminId}`)
        .then((response) => {
          commit('REMOVE_ADMIN', adminId)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },




  fetchUsers ({ commit } , type) {
    return new Promise((resolve, reject) => {

      // var formData = new FormData

      axios.get('/users?type=' + type)
        .then((response) => {
          commit('SET_USERS', response.data)

          resolve(response.data)
        })
        .catch((error) => { reject(error) })
    })
  },
  fetchUser (context, userId) {
    return new Promise((resolve, reject) => {
      axios.get(`/users/${userId}`)
        .then((response) => {

          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },



updateAdminPassword({commit} , data){

  return new Promise((resolve, reject) => {

    axios.post(`/dashboard/admins/update-password` , data)
      .then((res) => {

        if (res.data && res.data.message) {
          reject(res.data.message);
        }

        resolve(data);

      })
      .catch((error) => { reject(error) })
  })

},


updateAdmin({commit} , admin){

  return new Promise((resolve, reject) => {

    var form_data = new FormData();

    for ( var key in admin ) {
        form_data.append(key, admin[key]);
    }

    form_data.append('_method' , 'PUT');

    axios.post(`/dashboard/admins/${admin.id}` , form_data)
      .then((res) => {

        if (res.data && res.data.message) {
          reject(res.data.message);
        }

        resolve(res.data);

      })
      .catch((error) => { reject(error) })
  })

},


updateInvitedClient({commit} , invitedClient){
  return new Promise((resolve, reject) => {
    var form_data = new FormData();
    for ( var key in invitedClient ) {
        form_data.append(key, invitedClient[key]);
    }
    form_data.append('_method' , 'PUT');
    axios.post(`/dashboard/invited-clients/${invitedClient.id}` , form_data)
      .then((res) => {
        if (res.data && res.data.message) {
          reject(res.data.message);
        }
        resolve(res.data);
      })
      .catch((error) => { reject(error) })
  })
},


updateInvitedStore({commit} , invitedStore){
  return new Promise((resolve, reject) => {
    var form_data = new FormData();
    for ( var key in invitedStore ) {
        form_data.append(key, invitedStore[key]);
    }
    form_data.append('_method' , 'PUT');
    axios.post(`/dashboard/invited-stores/${invitedStore.id}` , form_data)
      .then((res) => {
        if (res.data && res.data.message) {
          reject(res.data.message);
        }
        resolve(res.data);
      })
      .catch((error) => { reject(error) })
  })
},

updateInvitedRepresentative({commit} , invitedRepresentative){
  return new Promise((resolve, reject) => {
    var form_data = new FormData();
    for ( var key in invitedRepresentative ) {
        form_data.append(key, invitedRepresentative[key]);
    }
    form_data.append('_method' , 'PUT');
    axios.post(`/dashboard/invited-representatives/${invitedRepresentative.id}` , form_data)
      .then((res) => {
        if (res.data && res.data.message) {
          reject(res.data.message);
        }
        resolve(res.data);
      })
      .catch((error) => { reject(error) })
  })
},


  updateUserStep(context , {user , step}){

    return new Promise((resolve, reject) => {

      var form_data = new FormData();

      for ( var key in user ) {
          form_data.append(key, user[key]);
      }

      form_data.append('_method' , 'PUT');

      axios.post(`/users/${user.id}/${step}` , form_data)
        .then((res) => {

          if (res.data && res.data.message) {
            reject(res.data.message);
          }

          resolve(user);


        })
        .catch((error) => { reject(error) })
    })

  },
  removeRecord ({ commit }, userId) {
    return new Promise((resolve, reject) => {
      axios.delete(`/users/${userId}`)
        .then((response) => {
          commit('REMOVE_RECORD', userId)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },

  registerStep1({commit} , user){
    return new Promise((resolve, reject) => {

      var form_data = new FormData();

      for ( var key in user ) {
          form_data.append(key, user[key]);
      }


      axios.post('/register-step1' , form_data)
        .then((res) => {

          if (res.data && res.data.message) {
            reject(res.data.message);
          }

          resolve(user);

        })
        .catch((error) => { reject(error) })
    })
  },

  registerStep2({commit} , user){
    return new Promise((resolve, reject) => {

      var form_data = new FormData();

      for ( var key in user ) {
          form_data.append(key, user[key]);
      }


      axios.post('/register-step2' , form_data)
        .then((res) => {

          if (res.data && res.data.message) {
            reject(res.data.message);
          }

          resolve(user);

        })
        .catch((error) => { reject(error) })
    })
  },

  registerStep3({commit} , user){
    return new Promise((resolve, reject) => {

      var form_data = new FormData();

      for ( var key in user ) {
          form_data.append(key, user[key]);
      }


      axios.post('/register-step3' , form_data)
        .then((res) => {

          if (res.data && res.data.message) {
            reject(res.data.message);
          }

          resolve(user);

        })
        .catch((error) => { reject(error) })
    })
  }



}
