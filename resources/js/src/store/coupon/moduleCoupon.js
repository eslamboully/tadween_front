/*=========================================================================================
  File Name: moduleCoupon.js
  Description: Coupon Module
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Couponor: Pixinvent
  Couponor URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import state from './moduleCouponState.js'
import mutations from './moduleCouponMutations.js'
import actions from './moduleCouponActions.js'
import getters from './moduleCouponGetters.js'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
