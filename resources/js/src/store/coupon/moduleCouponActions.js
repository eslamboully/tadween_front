/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Coupon Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import axios from '@/axios.js'

export default {

  fetchCoupons ({ commit } , type) {
    return new Promise((resolve, reject) => {

      axios.get('/dashboard/coupons')
        .then((response) => {
          commit('SET_COUPONS', response.data)

          resolve(response.data)
        })
        .catch((error) => { reject(error) })
    })
  },

  saveNewCoupon({commit} , coupon){

    return new Promise( (resolve , reject ) => {

      var form_data = new FormData();

      for ( var key in coupon ) {
        form_data.append(key, coupon[key]);

      }

      axios.post('/dashboard/coupons', form_data)
        .then(res => {

          if (res.data && res.data.message) {
            reject(res.data.message);
          }
          resolve(coupon);
      })

    });


  },

  fetchCoupon (context, couponId) {
    return new Promise((resolve, reject) => {
      axios.get(`/dashboard/coupons/${couponId}`)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },



  updateCoupon({commit} , coupon){

    return new Promise((resolve, reject) => {

      var form_data = new FormData();

      for ( var key in coupon ) {
          form_data.append(key, coupon[key]);
      }

      form_data.append('_method' , 'PUT');

      axios.post(`/dashboard/coupons/${coupon.id}` , form_data)
        .then((res) => {

          if (res.data && res.data.message) {
            reject(res.data.message);
          }

          resolve(res.data);

        })
        .catch((error) => { reject(error) })
    })

  },

  removeCoupon ({ commit }, couponId) {
    return new Promise((resolve, reject) => {
      axios.delete(`/dashboard/coupons/${couponId}`)
        .then((response) => {
          commit('REMOVE_COUPON', couponId)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  }

}
