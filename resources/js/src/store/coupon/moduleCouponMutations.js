/*=========================================================================================
  File Name: moduleAuthMutations.js
  Description: Auth Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/coupon/pixinvent
==========================================================================================*/

import axios from '../../http/axios/index.js'

export default {

  SET_COUPONS (state , coupons) {
    state.coupons = coupons;
  },
  REMOVE_COUPON (state, couponId) {
    const couponIndex = state.coupons.findIndex((u) => u.id === couponId)
    state.coupons.splice(couponIndex, 1)
  },

}
