/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import axios from '@/axios.js'

export default {

  fetchCities ({ commit }) {
    return new Promise((resolve, reject) => {
      axios.get('/dashboard/cities')
        .then((response) => {
          commit('SET_CITIES', response.data)
          resolve(response.data)
        })
        .catch((error) => { reject(error) })
    })
  },

  fetchNeighborhoods ({ commit }) {
    return new Promise((resolve, reject) => {
      axios.get('/dashboard/neighborhoods')
        .then((response) => {
          commit('SET_NEIIGHBORHOODS', response.data)
          resolve(response.data)
        })
        .catch((error) => { reject(error) })
    })
  },

  saveNewCity({state} , city){

    return new Promise( (resolve , reject ) => {

      var form_data = new FormData();

      for ( var key in city ) {
        form_data.append(key, city[key]);
      }

      axios.post('/dashboard/cities', form_data)
    	  .then(res => {

          if (!res.data.message)
            resolve(city);

          reject(res.data.message);

      })

    });
  },


  removeCity( {commit} , id ){

    return new Promise((resolve , reject)=>{

      var formData = new FormData();

      formData.append('_method' , 'DELETE');

      return axios.post('/dashboard/cities/' + id , formData ).then(res => {

        commit('REMOVE_CITY' , id);
        resolve(id);

      }).catch(error => {
        reject(error);
      });

    });

  },

  fetchCity( {commit} , id ){

    return new Promise((resolve , reject)=>{

      return axios.get('/dashboard/cities/' + id ).then(store => {
        resolve(store.data);
      }).catch(error => {
        reject(error);
      });

    });

  },

  updateCity( {commit} , city ){

    return new Promise((resolve , reject)=>{

      var formData = new FormData();

      for ( var key in city ) {
        formData.append(key, city[key]);
      }

      formData.append('_method' , 'PUT');

      return axios.post('/dashboard/cities/' + city.id , formData ).then(res => {


        if (res.data && res.data.message) {
          reject(res.data.message);
        }

        resolve(city);

      }).catch(error => {
      });

    });

  },





  saveNewNeighborhood({state} , neighborhood){

    return new Promise( (resolve , reject ) => {

      var form_data = new FormData();

      for ( var key in neighborhood ) {
        form_data.append(key, neighborhood[key]);
      }

      axios.post('/dashboard/neighborhoods', form_data)
    	  .then(res => {

          if (!res.data.message)
            resolve(neighborhood);

          reject(res.data.message);

      })

    });
  },


  removeNeighborhood( {commit} , id ){

    return new Promise((resolve , reject)=>{

      var formData = new FormData();

      formData.append('_method' , 'DELETE');

      return axios.post('/dashboard/neighborhoods/' + id , formData ).then(res => {

        commit('REMOVE_NEIGHOORHOOD' , id);
        resolve(id);

      }).catch(error => {
        reject(error);
      });

    });

  },

  fetchNeighborhood( {commit} , id ){

    return new Promise((resolve , reject)=>{

      return axios.get('/dashboard/neighborhoods/' + id ).then(store => {
        resolve(store.data);
      }).catch(error => {
        reject(error);
      });

    });

  },

  updateNeighborhood( {commit} , neighborhood ){

    return new Promise((resolve , reject)=>{

      var formData = new FormData();

      for ( var key in neighborhood ) {
        formData.append(key, neighborhood[key]);
      }

      formData.append('_method' , 'PUT');

      return axios.post('/dashboard/neighborhoods/' + neighborhood.id , formData ).then(res => {

        if (res.data && res.data.message) {
          reject(res.data.message);
        }

        resolve(neighborhood);

      }).catch(error => {
      });

    });

  },





}
