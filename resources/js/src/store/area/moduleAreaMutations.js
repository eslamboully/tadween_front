/*=========================================================================================
  File Name: moduleAuthMutations.js
  Description: Auth Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import axios from '../../http/axios/index.js'

export default {
  SET_CITIES (state , cities) {
    state.cities = cities;
  },

  SET_NEIIGHBORHOODS(state , neighborhoods) {
    state.neighborhoods = neighborhoods;
  },

  REMOVE_CITY(state , id) {
    const index = state.cities.findIndex( city => city.id == id );

    state.cities.splice(index , 1);
  },

  REMOVE_NEIGHOORHOOD(state , id) {
    const index = state.neighborhoods.findIndex( neighborhood => neighborhood.id == id );

    state.neighborhoods.splice(index , 1);
  }

}
