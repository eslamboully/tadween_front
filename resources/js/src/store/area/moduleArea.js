/*=========================================================================================
  File Name: moduleArea.js
  Description: Area Module
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Areaor: Pixinvent
  Areaor URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import state from './moduleAreaState.js'
import mutations from './moduleAreaMutations.js'
import actions from './moduleAreaActions.js'
import getters from './moduleAreaGetters.js'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
