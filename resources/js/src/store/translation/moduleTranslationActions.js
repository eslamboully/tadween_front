/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Translation Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import axios from '@/axios.js'

export default {

  fetchTranslations ({ commit } , data) {
    return new Promise((resolve, reject) => {

      axios.get(`/dashboard/translations?type=${data.type}&language_id=${data.language_id}`)

        .then((response) => {

          resolve(response.data)
        })
        .catch((error) => { reject(error) })
    })
  },

  saveTranslation({commit} , data){

    return new Promise( (resolve , reject ) => {


      axios.post('/dashboard/translations', data)
        .then(res => {

          if (res.data && res.data.message) {
            reject(res.data.message);
          }
          resolve(res.data.message);
      })

    });


  },

  fetchTranslation (context, categoryId) {
    return new Promise((resolve, reject) => {
      axios.get(`/dashboard/translations/${categoryId}`)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },



  updateTranslation({commit} , category){

    return new Promise((resolve, reject) => {

      var form_data = new FormData();

      for ( var key in category ) {
          form_data.append(key, category[key]);
      }

      form_data.append('_method' , 'PUT');

      axios.post(`/dashboard/translations/${category.id}` , form_data)
        .then((res) => {

          if (res.data && res.data.message) {
            reject(res.data.message);
          }

          resolve(res.data);

        })
        .catch((error) => { reject(error) })
    })

  },

  removeTranslation ({ commit }, categoryId) {
    return new Promise((resolve, reject) => {
      axios.delete(`/dashboard/translations/${categoryId}`)
        .then((response) => {
          commit('REMOVE_TRANSLATION', categoryId)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  }

}
