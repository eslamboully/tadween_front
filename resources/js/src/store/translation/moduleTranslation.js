/*=========================================================================================
  File Name: moduleTranslation.js
  Description: Translation Module
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Translationor: Pixinvent
  Translationor URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import state from './moduleTranslationState.js'
import mutations from './moduleTranslationMutations.js'
import actions from './moduleTranslationActions.js'
import getters from './moduleTranslationGetters.js'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
