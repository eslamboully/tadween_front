/*=========================================================================================
  File Name: moduleAuthMutations.js
  Description: Auth Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/translation/pixinvent
==========================================================================================*/

import axios from '../../http/axios/index.js'

export default {

  SET_TRANSLATIONS (state , translations) {
    state.translations = translations;
  },
  REMOVE_TRANSLATION (state, translationId) {
    const translationIndex = state.translations.findIndex((u) => u.id === translationId)
    state.translations.splice(translationIndex, 1)
  },

}
