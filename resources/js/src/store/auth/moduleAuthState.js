/*=========================================================================================
  File Name: moduleAuthState.js
  Description: Auth Module State
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

export default {
  isAdminLoggedIn: () => {
    return (localStorage.getItem('adminInfo') != null && localStorage.getItem('adminToken') != null);

  },

  isUserLoggedIn: () => {
    return (localStorage.getItem('userInfo') != null && localStorage.getItem('userToken') != null);
  }

}
