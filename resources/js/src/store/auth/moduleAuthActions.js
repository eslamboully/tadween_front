/*=========================================================================================
  File Name: moduleAuthActions.js
  Description: Auth Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import jwt from '../../http/requests/auth/jwt/index.js'

import router from '@/router'

export default {

  // JWT
  loginJWT ({ commit }, payload) {

    return new Promise((resolve, reject) => {
      jwt.login(payload.userDetails.email, payload.userDetails.password)
        .then(response => {

          // If there's user data in response
          if (response.data && !response.data.message) {
            // Navigate User to homepage
            router.push({name:'dashboard-analytics'})

            // Set adminToken
            localStorage.setItem('adminToken', response.data.api_token)

            // Update user details
            commit('UPDATE_ADMIN_INFO', response.data.admin, {root: true})

            // Set bearer token in axios
            commit('SET_BEARER', response.data.api_token)

            resolve(response)
          } else {
            reject({message: response.data.message})
          }

        })
        .catch(error => { reject(error) })
    })
  },


/////////////////////////////////////////////////////////// User Auth ///////////////////////////////////////////////////////////////////



  loginUserJWT ({ commit }, payload) {

    return new Promise((resolve, reject) => {

      jwt.userLogin(payload.email, payload.password)
        .then(response => {

          // If there's user data in response
          if (response.data) {
            // Navigate User to homepage
            router.push(router.currentRoute.query.to || '/')

            // Set adminToken
            localStorage.setItem('userToken', response.data.api_token)

            // Update user details
            commit('UPDATE_USER_INFO', response.data.user, {root: true})

            // Set bearer token in axios
            commit('SET_BEARER', response.data.api_token)

            resolve(response)
          } else {
            reject({message: 'Wrong Email or Password'})
          }

        })
        .catch(error => { reject(error) })
    })
  },

  logoutUser({commit, state}){

    return new Promise((resolve, reject) => {

      jwt.userLogout().then( async (response) => {

        // Set adminToken

        // Update user details
       await commit('REMOVE_USER_INFO' , null , {root:true})
       await commit('SET_BEARER', '');
       state.isUserLoggedIn();

       resolve(response)
      });

    });

  },



  registerUserJWT ({ commit }, payload) {

    const { displayName, email, password, confirmPassword } = payload.userDetails

    return new Promise((resolve, reject) => {

      // Check confirm password
      if (password !== confirmPassword) {
        reject({message: 'Password doesn\'t match. Please try again.'})
      }

      jwt.registerUser(displayName, email, password)
        .then(response => {
          // Redirect User
          router.push(router.currentRoute.query.to || '/')

          // Update data in localStorage
          localStorage.setItem('adminToken', response.data.adminToken)
          commit('UPDATE_ADMIN_INFO', response.data.userData, {root: true})

          resolve(response)
        })
        .catch(error => { reject(error) })
    })
  },
  fetchAccessToken () {
    return new Promise((resolve) => {
      jwt.refreshToken().then(response => { resolve(response) })
    })
  }
}
