/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Subscription Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import axios from '@/axios.js'

export default {

  fetchSubscriptions ({ commit } , type) {
    return new Promise((resolve, reject) => {

      axios.get('/dashboard/subscriptions')
        .then((response) => {
          commit('SET_SUBSCRIPTIONS', response.data)

          resolve(response.data)
        })
        .catch((error) => { reject(error) })
    })
  },

  saveNewSubscription({commit} , subscription){

    return new Promise( (resolve , reject ) => {

      var form_data = new FormData();

      for ( var key in subscription ) {
        form_data.append(key, subscription[key]);

      }

      axios.post('/dashboard/subscriptions', form_data)
        .then(res => {

          if (res.data && res.data.message) {
            reject(res.data.message);
          }
          resolve(subscription);
      })

    });


  },

  fetchSubscription (context, subscriptionId) {
    return new Promise((resolve, reject) => {
      axios.get(`/dashboard/subscriptions/${subscriptionId}`)
        .then((response) => {
          resolve(response.data)
        })
        .catch((error) => { reject(error) })
    })
  },



  updateSubscription({commit} , subscription){

    return new Promise((resolve, reject) => {

      var form_data = new FormData();

      for ( var key in subscription ) {
          form_data.append(key, subscription[key]);
      }

      form_data.append('_method' , 'PUT');

      axios.post(`/dashboard/subscriptions/${subscription.id}` , form_data)
        .then((res) => {

          if (res.data && res.data.message) {
            reject(res.data.message);
          }

          resolve(res.data);

        })
        .catch((error) => { reject(error) })
    })

  },

  removeSubscription ({ commit }, subscriptionId) {
    return new Promise((resolve, reject) => {
      axios.delete(`/dashboard/subscriptions/${subscriptionId}`)
        .then((response) => {
          commit('REMOVE_SUBSCRIPTION', subscriptionId)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  }

}
