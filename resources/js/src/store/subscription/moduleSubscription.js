/*=========================================================================================
  File Name: moduleSubscription.js
  Description: Subscription Module
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Subscriptionor: Pixinvent
  Subscriptionor URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import state from './moduleSubscriptionState.js'
import mutations from './moduleSubscriptionMutations.js'
import actions from './moduleSubscriptionActions.js'
import getters from './moduleSubscriptionGetters.js'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
