/*=========================================================================================
  File Name: moduleAuthMutations.js
  Description: Auth Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/Subscription/pixinvent
==========================================================================================*/

import axios from '../../http/axios/index.js'

export default {

  SET_SUBSCRIPTIONS (state , subscriptions) {
    state.subscriptions = subscriptions;
  },
  REMOVE_SUBSCRIPTION (state, subscriptionId) {
    const subscriptionIndex = state.subscriptions.findIndex((u) => u.id === subscriptionId)
    state.subscriptions.splice(subscriptionIndex, 1)
  },

}
