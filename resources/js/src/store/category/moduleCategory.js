/*=========================================================================================
  File Name: moduleCategory.js
  Description: Category Module
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Categoryor: Pixinvent
  Categoryor URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import state from './moduleCategoryState.js'
import mutations from './moduleCategoryMutations.js'
import actions from './moduleCategoryActions.js'
import getters from './moduleCategoryGetters.js'

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
