/*=========================================================================================
  File Name: moduleAuthMutations.js
  Description: Auth Module Mutations
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/category/pixinvent
==========================================================================================*/

import axios from '../../http/axios/index.js'

export default {

  SET_CATEGORIES (state , categories) {
    state.categories = categories;
  },
  REMOVE_CATEGORY (state, categoryId) {
    const categoryIndex = state.categories.findIndex((u) => u.id === categoryId)
    state.categories.splice(categoryIndex, 1)
  },

}
