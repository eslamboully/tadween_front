/*=========================================================================================
  File Name: moduleCalendarActions.js
  Description: Calendar Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Category Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import axios from '@/axios.js'

export default {

  fetchCategories ({ commit } , type) {
    return new Promise((resolve, reject) => {

      axios.get('/dashboard/categories')
        .then((response) => {
          commit('SET_CATEGORIES', response.data)

          resolve(response.data)
        })
        .catch((error) => { reject(error) })
    })
  },

  saveNewCategory({commit} , category){

    return new Promise( (resolve , reject ) => {

      var form_data = new FormData();

      for ( var key in category ) {

          var value = category[key];

          if (typeof value == 'object'){
              value = JSON.stringify(value);
          }

          form_data.append(key, value);
      }

      axios.post('/dashboard/categories', form_data)
        .then(res => {

          if (res.data && res.data.message) {
            reject(res.data.message);
          }
          resolve(category);
      })

    });


  },

  fetchCategory (context, categoryId) {
    return new Promise((resolve, reject) => {
      axios.get(`/dashboard/categories/${categoryId}`)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  },



  updateCategory({commit} , category){

    return new Promise((resolve, reject) => {

      var form_data = new FormData();

      for ( var key in category ) {
          var value = category[key];

          if (typeof value == 'object'){
              value = JSON.stringify(value);
          }

          form_data.append(key, value);
      }

      form_data.append('_method' , 'PUT');

      axios.post(`/dashboard/categories/${category.id}` , form_data)
        .then((res) => {

          if (res.data && res.data.message) {
            reject(res.data.message);
          }

          resolve(res.data);

        })
        .catch((error) => { reject(error) })
    })

  },

  removeCategory ({ commit }, categoryId) {
    return new Promise((resolve, reject) => {
      axios.delete(`/dashboard/categories/${categoryId}`)
        .then((response) => {
          commit('REMOVE_CATEGORY', categoryId)
          resolve(response)
        })
        .catch((error) => { reject(error) })
    })
  }

}
