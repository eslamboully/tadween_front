import axios from '../../../axios/index.js'
import store from '../../../../store/store.js'

// Token Refresh
let isAlreadyFetchingAccessToken = false
let subscribers = []

function onAccessTokenFetched (adminToken) {
  subscribers = subscribers.filter(callback => callback(adminToken))
}

function addSubscriber (callback) {
  subscribers.push(callback)
}

export default {
  init () {
    axios.interceptors.response.use(function (response) {
      return response
    }, function (error) {
      // const { config, response: { status } } = error
      const { config, response } = error
      const originalRequest = config

      // if (status === 401) {
      if (response && response.status === 401) {
        if (!isAlreadyFetchingAccessToken) {
          isAlreadyFetchingAccessToken = true
          store.dispatch('auth/fetchAccessToken')
            .then((adminToken) => {
              isAlreadyFetchingAccessToken = false
              onAccessTokenFetched(adminToken)
            })
        }

        const retryOriginalRequest = new Promise((resolve) => {
          addSubscriber(adminToken => {
            originalRequest.headers.Authorization = `Bearer ${adminToken}`
            resolve(axios(originalRequest))
          })
        })
        return retryOriginalRequest
      }
      return Promise.reject(error)
    })
  },
  login (email, pwd) {
    return axios.post('/dashboard/login', {
      email,
      password: pwd
    })
  },
  userLogin(email , password){
    return axios.post('/login', {
      email,
      password
    })
  },
  registerUser (name, email, pwd) {
    return axios.post('/api/auth/register', {
      displayName: name,
      email,
      password: pwd
    })
  },
  userLogout(){
    return axios.post('/logout')
  },
  refreshToken () {
    return axios.post('/api/auth/refresh-token', {adminToken: localStorage.getItem('adminToken')})
  }
}
