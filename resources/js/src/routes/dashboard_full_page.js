// =============================================================================
// FULL PAGE LAYOUTS
// =============================================================================

export default{
  path: '/dashboard',
  component: () => import('@/layouts/full-page/FullPage.vue'),
  children: [
    // =============================================================================
    // PAGES
    // =============================================================================

    {
      path: 'login',
      name: 'page-login',
      component: () => import('@/views/pages/login/Login.vue'),
      meta: {
        rule: 'editor'
      }
    },
    {
      path: 'lock-screen',
      name: 'page-lock-screen',
      component: () => import('@/views/pages/LockScreen.vue'),
      meta: {
        rule: 'editor'
      }
    },
    {
      path: 'comingsoon',
      name: 'page-coming-soon',
      component: () => import('@/views/pages/ComingSoon.vue'),
      meta: {
        rule: 'editor'
      }
    },
    {
      path: 'error-404',
      name: 'page-error-404',
      component: () => import('@/views/pages/Error404.vue'),
      meta: {
        rule: 'editor'
      }
    },
    {
      path: 'error-500',
      name: 'page-error-500',
      component: () => import('@/views/pages/Error500.vue'),
      meta: {
        rule: 'editor'
      }
    },
    {
      path: 'not-authorized',
      name: 'page-not-authorized',
      component: () => import('@/views/pages/NotAuthorized.vue'),
      meta: {
        rule: 'editor'
      }
    },
    {
      path: 'maintenance',
      name: 'page-maintenance',
      component: () => import('@/views/pages/Maintenance.vue'),
      meta: {
        rule: 'editor'
      }
    }
  ]
};
