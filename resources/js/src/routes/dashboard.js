export default{
  // =============================================================================
  // MAIN LAYOUT ROUTES
  // =============================================================================
    path: '',
    component: () => import('../layouts/main/Main.vue'),
    children: [
      // =============================================================================
      // Theme Routes
      // =============================================================================
      // {
      //   path: '/',
      //   redirect: '/dashboard/analytics'
      // },
      {
        path: '/dashboard',
        redirect: '/dashboard/analytics'
      },
      {
        path: '/dashboard/analytics',
        name: 'dashboard-analytics',
        component: () => import('../views/DashboardAnalytics.vue'),
        meta: {
          rule: 'editor',
          adminAuthRequired:true
        }
      },

      // =============================================================================
      // Application Routes
      // =============================================================================
      {
        path: '/dashboard/admin/admin-list',
        name: 'dashboard-admin-list',
        component: () => import('@/views/dashboard/admin/admin-list/AdminList.vue'),
        meta: {
          breadcrumb: [
            { title: 'Home', url: '/' },
            { title: 'Admin' },
            { title: 'List', active: true }
          ],
          pageTitle: 'Admin List',
          rule: 'editor',
          adminAuthRequired:true
        }
      },
      {
        path: '/dashboard/admin/admin-view/:adminId',
        name: 'dashboard-admin-view',
        component: () => import('@/views/dashboard/admin/AdminView.vue'),
        meta: {
          breadcrumb: [
            { title: 'Home', url: '/' },
            { title: 'Admin' },
            { title: 'View', active: true }
          ],
          pageTitle: 'Admin View',
          rule: 'editor',
          adminAuthRequired:true
        }
      },

      {
        path: '/dashboard/admin/admin-permissions/:adminId',
        name: 'dashboard-admin-permissions-view',
        component: () => import('@/views/dashboard/admin/admin-permissions/AdminPermission.vue'),
        meta: {
          breadcrumb: [
            { title: 'Home', url: '/' },
            { title: 'Admin' },
            { title: 'View', active: true }
          ],
          pageTitle: 'Admin View',
          rule: 'editor',
          adminAuthRequired:true
        }
      },

      {
        path: '/dashboard/admin/admin-edit/:adminId',
        name: 'dashboard-admin-edit',
        component: () => import('@/views/dashboard/admin/admin-edit/AdminEdit.vue'),
        meta: {
          breadcrumb: [
            { title: 'Home', url: '/' },
            { title: 'Admin' },
            { title: 'Edit', active: true }
          ],
          pageTitle: 'Admin Edit',
          rule: 'editor',
          adminAuthRequired:true
        }
      },
      {
        path: '/dashboard/admin/admin-create',
        name: 'dashboard-admin-create',
        component: () => import('@/views/dashboard/admin/admin-create/AdminCreate.vue'),
        meta: {
          breadcrumb: [
            { title: 'Home', url: '/' },
            { title: 'Admin' },
            { title: 'Create', active: true }
          ],
          pageTitle: 'Admin Create',
          rule: 'editor',
          adminAuthRequired:true
        }
      },
     /**
      * @Route("News")
      */
      {
        path: '/dashboard/news/news-list',
        name: 'dashboard-news-list',
        component: () => import('@/views/dashboard/news/news-list/NewsList.vue'),
        meta: {
          breadcrumb: [
            { title: 'Home', url: '/' },
            { title: 'News' },
            { title: 'List', active: true }
          ],
          pageTitle: 'News List',
          rule: 'editor',
          adminAuthRequired:true
        }
      },
      {
          path: '/dashboard/news/news-create',
          name: 'dashboard-news-create',
          component: () => import('@/views/dashboard/news/news-create/NewsCreate.vue'),
          meta: {
            breadcrumb: [
                { title: 'Home', url: '/' },
                { title: 'News' },
                { title: 'Create', active: true }
            ],
            pageTitle: 'News Create',
            rule: 'editor',
            adminAuthRequired:true
          }
      },
      {
      path: '/dashboard/news/news-edit/:newsId',
      component: () => import('@/views/dashboard/news/news-edit/NewsEdit.vue'),
      meta: {
        breadcrumb: [
          { title: 'Home', url: '/' },
          { title: 'News' },
          { title: 'Edit', active: true }
        ],
        pageTitle: 'News Edit',
        rule: 'editor',
        adminAuthRequired:true
      }
    },
     /**
      * @Route("Packages")
      */
        {
            path: '/dashboard/packages/packages-list',
            name: 'dashboard-packages-list',
            component: () => import('@/views/dashboard/packages/packages-list/PackagesList.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/' },
                    { title: 'Packages' },
                    { title: 'List', active: true }
                ],
                pageTitle: 'Packages List',
                rule: 'editor',
                adminAuthRequired:true
            }
        },
        {
            path: '/dashboard/packages/packages-create',
            name: 'dashboard-packages-create',
            component: () => import('@/views/dashboard/packages/packages-create/PackagesCreate.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/' },
                    { title: 'Packages' },
                    { title: 'Create', active: true }
                ],
                pageTitle: 'Packages Create',
                rule: 'editor',
                adminAuthRequired:true
            }
        },
        {
            path: '/dashboard/packages/packages-edit/:packagesId',
            name: 'dashboard-packages-edit',
            component: () => import('@/views/dashboard/packages/packages-edit/PackagesEdit.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/' },
                    { title: 'Package' },
                    { title: 'Edit', active: true }
                ],
                pageTitle: 'Package Edit',
                rule: 'editor',
                adminAuthRequired:true
            }
        },

        /**
         * @Route("Testimonials")
         */
        {
            path: '/dashboard/testimonials/testimonials-list',
            name: 'dashboard-testimonials-list',
            component: () => import('@/views/dashboard/testimonials/testimonials-list/TestimonialList.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/' },
                    { title: 'Testimonial' },
                    { title: 'List', active: true }
                ],
                pageTitle: 'Testimonial List',
                rule: 'editor',
                adminAuthRequired:true
            }
        },
        {
            path: '/dashboard/testimonials/testimonials-create',
            name: 'dashboard-testimonials-create',
            component: () => import('@/views/dashboard/testimonials/testimonials-create/TestimonialCreate.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/' },
                    { title: 'Testimonial' },
                    { title: 'Create', active: true }
                ],
                pageTitle: 'Testimonial Create',
                rule: 'editor',
                adminAuthRequired:true
            }
        },
        {
            path: '/dashboard/testimonials/testimonials-edit/:testimonialId',
            name: 'dashboard-testimonials-edit',
            component: () => import('@/views/dashboard/testimonials/testimonials-edit/TestimonialEdit.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/' },
                    { title: 'Testimonial' },
                    { title: 'Edit', active: true }
                ],
                pageTitle: 'Testimonial Edit',
                rule: 'editor',
                adminAuthRequired:true
            }
        },


        /**
         * @Route("Services")
         */
        {
            path: '/dashboard/services/services-list',
            name: 'dashboard-services-list',
            component: () => import('@/views/dashboard/services/services-list/ServicesList.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/' },
                    { title: 'Services' },
                    { title: 'List', active: true }
                ],
                pageTitle: 'Services List',
                rule: 'editor',
                adminAuthRequired:true
            }
        },
        {
            path: '/dashboard/services/services-create',
            name: 'dashboard-services-create',
            component: () => import('@/views/dashboard/services/services-create/ServicesCreate.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/' },
                    { title: 'Services' },
                    { title: 'Create', active: true }
                ],
                pageTitle: 'Services Create',
                rule: 'editor',
                adminAuthRequired:true
            }
        },
        {
            path: '/dashboard/services/services-edit/:servicesId',
            name: 'dashboard-services-edit',
            component: () => import('@/views/dashboard/services/services-edit/ServicesEdit.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/' },
                    { title: 'Service' },
                    { title: 'Edit', active: true }
                ],
                pageTitle: 'Service Edit',
                rule: 'editor',
                adminAuthRequired:true
            }
        },

        /**
         * @Route("partners")
         */
        {
            path: '/dashboard/partners/partners-list',
            name: 'dashboard-partners-list',
            component: () => import('@/views/dashboard/partners/partners-list/PartnersList.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/' },
                    { title: 'Partners' },
                    { title: 'List', active: true }
                ],
                pageTitle: 'Partners List',
                rule: 'editor',
                adminAuthRequired:true
            }
        },
        {
            path: '/dashboard/partners/partners-edit/:partnersId',
            name: 'dashboard-partners-edit',
            component: () => import('@/views/dashboard/partners/partners-edit/PartnersEdit.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/' },
                    { title: 'Partner' },
                    { title: 'Edit', active: true }
                ],
                pageTitle: 'Partner Edit',
                rule: 'editor',
                adminAuthRequired:true
            }
        },
        {
            path: '/dashboard/partners/partners-create',
            name: 'dashboard-partners-create',
            component: () => import('@/views/dashboard/partners/partners-create/PartnersCreate.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/' },
                    { title: 'Partners' },
                    { title: 'Create', active: true }
                ],
                pageTitle: 'Partners Create',
                rule: 'editor',
                adminAuthRequired:true
            }
        },

        /**
         * @Route("Settings")
         */
        {
            path: '/dashboard/settings/:configsId/general',
            name: 'dashboard-settings-general',
            component: () => import('@/views/dashboard/settings/settings-update/SettingsUpdate.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/' },
                    { title: 'Settings' },
                    { title: 'List', active: true }
                ],
                pageTitle: 'Settings List',
                rule: 'editor',
                adminAuthRequired:true
            }
        },
        {
            path: '/dashboard/settings/:configsId/home',
            name: 'dashboard-settings-home',
            component: () => import('@/views/dashboard/settings/settings-update/SettingsUpdateHome.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/' },
                    { title: 'Settings' },
                    { title: 'List', active: true }
                ],
                pageTitle: 'Settings List',
                rule: 'editor',
                adminAuthRequired:true
            }
        },
        {
            path: '/dashboard/settings/:configsId/others',
            name: 'dashboard-settings-others',
            component: () => import('@/views/dashboard/settings/settings-update/SettingsUpdateOthers.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/' },
                    { title: 'Settings' },
                    { title: 'List', active: true }
                ],
                pageTitle: 'Settings List',
                rule: 'editor',
                adminAuthRequired:true
            }
        },

    ]
  };
