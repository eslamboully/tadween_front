export default{
  // =============================================================================
  // MAIN LAYOUT ROUTES
  // =============================================================================

    path: '',
    component: () => import('../layouts/main/Front.vue'),
    children: [
        {
            path: '/',
            name: 'front-index',
            component: () => import('@/views/front/FrontView.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/' },
                    { title: 'FrontView' },
                    { title: 'List', active: true }
                ],
                pageTitle: 'Tadween | index',
                rule: 'editor',
                adminAuthRequired:false
            }
        },
        {
            path: '/privacy-policy',
            name: 'privacy-policy',
            component: () => import('@/views/front/PrivacyPolicyView.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/' },
                    { title: 'PrivacyPolicyView' },
                    { title: 'List', active: true }
                ],
                pageTitle: 'Tadween | Privacy Policy',
                rule: 'editor',
                adminAuthRequired:false
            }
        },
        {
            path: '/about-us',
            name: 'about-us',
            component: () => import('@/views/front/AboutView.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/' },
                    { title: 'AboutView' },
                    { title: 'List', active: true }
                ],
                pageTitle: 'Tadween | About Us',
                rule: 'editor',
                adminAuthRequired:false
            }
        },
        {
            path: '/packages',
            name: 'packages',
            component: () => import('@/views/front/PackagesView.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/' },
                    { title: 'Packages' },
                    { title: 'List', active: true }
                ],
                pageTitle: 'Tadween | Packages',
                rule: 'editor',
                adminAuthRequired:false
            }
        },
        {
            path: '/blogs',
            name: 'blogs',
            component: () => import('@/views/front/BlogsView.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/' },
                    { title: 'Blogs' },
                    { title: 'List', active: true }
                ],
                pageTitle: 'Tadween | Blogs',
                rule: 'editor',
                adminAuthRequired:false
            }
        },
        {
            path: '/blogs/:id',
            name: 'blog',
            component: () => import('@/views/front/BlogDetailsView.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/' },
                    { title: 'Blogs' },
                    { title: 'Details', active: true }
                ],
                pageTitle: 'Tadween | Blog',
                rule: 'editor',
                adminAuthRequired:false
            }
        },
        {
            path: '/how-work',
            name: 'how-work',
            component: () => import('@/views/front/HowView.vue'),
            meta: {
                breadcrumb: [
                    { title: 'Home', url: '/' },
                    { title: 'HowView' },
                    { title: 'List', active: true }
                ],
                pageTitle: 'Tadween | How Works',
                rule: 'editor',
                adminAuthRequired:false
            }
        },
    ]
  };
