/*=========================================================================================
  File Name: router.js
  Description: Routes for vue-router. Lazy loading is enabled.
  Object Strucutre:
                    path => router path
                    name => router name
                    component(lazy loading) => component to load
                    meta : {
                      rule => which user can have access (ACL)
                      breadcrumb => Add breadcrumb to specific page
                      pageTitle => Display title besides breadcrumb
                    }
  ----------------------------------------------------------------------------------------
  Item Name: Tadween - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import Vue from 'vue'
import Router from 'vue-router'


import dashboard from './routes/dashboard'
import front from './routes/front'
import dashboard_full_page from './routes/dashboard_full_page'

Vue.use(Router)


const router = new Router({
  mode: 'history',
  scrollBehavior () {
    return { x: 0, y: 0 }
  },
  routes: [
    front,
    dashboard,
    dashboard_full_page,
    {
      path: '/dashboard',
      redirect: '/dashboard/analytics'
    },
    // Redirect to 404 page, if no match found
    {
      path: '*',
      redirect: '/dashboard/error-404'
    }
  ]
})

router.afterEach(() => {
  // Remove initial loading
  const appLoading = document.getElementById('loading-bg')
  if (appLoading) {
    appLoading.style.display = 'none'
  }
})

router.beforeEach((to, from, next) => {

  if (to.meta.adminAuthRequired) {
    // get firebase current user
    const adminToken = localStorage.getItem('adminToken');
    const adminInfo = localStorage.getItem('adminInfo');

    if ( adminInfo == null  || adminInfo == null) {
      router.push({ path: '/dashboard/login' })
    }
  }

  return next()


})

export default router
