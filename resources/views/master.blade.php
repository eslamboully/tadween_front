<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Tadween Homepage</title>
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/vuesax.css') }}">
    <link rel="stylesheet" href="{{ url('front') }}/css/bootstrap.min.css">
    <script src="https://kit.fontawesome.com/91572df90b.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{ url('front') }}/css/howItWork.css">
    <link rel="stylesheet" href="{{ url('front') }}/css/about.css">
    <link rel="stylesheet" href="{{ url('front') }}/css/homepage.css">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('front') }}/css/swiper.min.css">
    <link rel="stylesheet" href="{{ url('front') }}/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ url('front') }}/css/animate.css">
    <link rel="stylesheet" href="{{ url('front') }}/css/hover-min.css">
    <link rel="stylesheet" href="{{ url('front/css/editor.css') }}">
    <link rel="icon" href="{{ asset('/front/img/logo.svg') }}">



    <style>
        @import url('https://fonts.googleapis.com/css2?family=Cairo:wght@200&display=swap');

        body, h1, h2, h3, h4, h5 ,h6 {
            font-family: 'Cairo', sans-serif;
        }


    </style>

</head>
<body>

<div id="app">

</div>
<!-- navbar -->

<script src="{{ url('front') }}/js/jquery-3.3.1.min.js"></script>                  <!-- jquery v3.3.1 -->
<script src="{{ url('front') }}/js/popper.min.js"></script>
<script src="{{ url('front') }}/js/bootstrap.min.js"></script>                     <!-- bootstrap v4.0.0 -->
<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script> <!-- Isotope filter -->
<script src="{{ url('front') }}/js/owl.carousel.min.js"></script>
<script src="{{ url('front') }}/js/script.js"></script>
<script src="{{ url('front') }}/js/numscroller-1.0.js"></script>                   <!-- numscroller v1 -->
<script src="{{ url('front') }}/js/tilt.jquery.min.js"></script>
<script src="{{ url('front') }}/js/swiper.min.js"></script>                        <!-- swiper v5.2.1 -->
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script> <!-- AOS animation library -->
<script src="{{ asset(mix('js/app.js')) }}"></script>
<script>
    AOS.init();                                                 // init AOS animation library //
</script>
</body>
</html>
