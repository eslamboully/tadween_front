@component('mail::message')

  <div class="left" id="tours">
    Send From :
    <br/>

    {{ $data['name'] }},
    {{ $data['email'] }}
  </div>

  <div class="right" id="guides">
    <h2>{{ $data['company'] }}</h2>
  </div>

  <div id="news-and-views">
    {{ $data['message'] }}!
  </div>

@endcomponent
