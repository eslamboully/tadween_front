@component('mail::message')

  <div class="left" id="tours">
    Hello From Tadween :
  </div>

  <div id="news-and-views">
    Tadween registeration code: {{ $data['user']->verification_code }}
  </div>

@endcomponent
