<?php

return [
    'role_structure' => [
        'superadministrator' => [
            'users' => 'c,r,u,d',
            'admins' => 'c,r,u,d',
            'categories' => 'c,r,u,d',
            'sotres' => 'c,r,u,d',
            'products' => 'c,r,u,d',
            'settings' => 'u',
        ],
        'user' => [
          'orders' => 'c,r,u,d',
          'cart' => 'c,r,u,d',
          'products' => 'c,r,u,d',
          'stores' => 'c,r,u,d',
        ],
    ],
    'permission_structure' => [
        'cru_user' => [
            'profile' => 'c,r,u'
        ],
    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
